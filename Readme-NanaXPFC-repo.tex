\documentclass[preprint,preprintnumbers,amsmath,amssymb,floatfix]{paper}

% For inserting eps graphics files
\usepackage[final,dvips]{graphicx}
\usepackage{verbatim}
\usepackage{color}
\usepackage{hyperref}

\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\noid}{\noindent}
\newcommand{\beqa}{\begin{eqnarray}}
\newcommand{\eeqa}{\end{eqnarray}}

\newcommand \rt {\right}
\newcommand \lt {\left}
\newcommand \nline {\nonumber \\}

\newcommand \free {${\cal F}$\ }
\newcommand \freem {{\cal F}}
\newcommand \freed {${\it F}$\ }
\newcommand \freedm {{\it F}}
\newcommand \pxpy[2] {\frac{\partial #1}{\partial #2}}
\newcommand \dxdy[2] {\frac{d #1}{d #2}}
\newcommand \fxfy[2] {\frac{\delta #1}{\delta #2}}
\newcommand \vxvy[2] {\frac{\delta #1}{\delta #2}}

\newcommand \grad {\vec{\nabla}}
\newcommand \fphin {\hat{\phi}^{n+1}}
\newcommand \fphio {\hat{\phi}^{n}}


\begin{document}
\bibliographystyle{unsrt}

\title{README - PFC and XPFC Codes}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\noid{\bf General}: \\
In this folder you will find a small repository of some of the work that I did during my PhD. It mostly deals with the development and testing of various PFC and XPFC models. For the most part the pre-numerical testing, i.e., phase diagrams and energetic calculations, of the models are done through {\it Maple}. So it goes without saying that in order to use these you must have a version of Maple installed or have access to it. Usually after some Maple calculations to determine the equilibrium phase space of the model, a c/c++ code is written to assess the dynamic stability.

I have taken the time to organize the files according to the development of the models by name. Most of which corresponds to either a specific chapter or set of chapters in my thesis or corresponds to a paper that has been published. The names of the folders should be self explanatory as well as the publications and my thesis being added to this repository. The codes are meant as a stepping stone and sort of an introduction. They are by no means the most efficient or robust of algorithms and it is expected that you improve upon them once getting your feet wet. Also it is not for all Maple files I have included that have a corresponding c/c++ to accompany it. But generating one should not be too difficult. If for some reason things aren't self explanatory as I have made it out to be, you can find great resources in asking the more senior students in the group. If that doesn't work, I am sure Nik will have no issues giving you some guidance. If you would rather not bother Nik, at the time of writing this readme file I can be reached at either
\underline{\href{mailto:nana.opoku@northwestern.edu}{nana.opoku@northwestern.edu}} or \underline{\href{mailto:nana.oforiopoku@nist.gov}{nana.oforiopoku@nist.gov}}.
\\
\\
\noid{\bf Maple Files}: \\
As mentioned in the previous paragraph the maple files will go through the calculation of the phase diagram. I will be assuming that you're aware of the general approach in how phase diagrams are calculated from thermodynamic principles and in particular the procedure of how it is done using a PFC-type free energy. If not, Nik has some good notes on the topic. Whether it is PFC or XPFC there is a general flow to the Maple files I have written. This is particularly true for the models where the XPFC paradigm is used. I usually tend to generate a new file for each different project I work on. Clearly there is a more efficient way to go about this. One efficient process could perhaps be to generate a library that has a lot of the information, particularly about crystal structures and such so that that doesn't have to be repeated.

The general flow of a Maple file typically goes like this:
\begin{description}
\item[{\it Density Approximation}] \hfill\\
An approximation for the density profile is assumed, one for each phase of interest or the phases one assumes can be stabilized by the proposed free energy. This could be the single mode approximation familiar in numerous works or the multi-mode approximation which has become the norm in XPFC models. 
\item[{\it Integration Over a Unit Cell}] \hfill\\
Next, usually the density field is inserted into the free energy, expanded and then integrated over a unit cell. This essentially mimics a smoothing operation where the resulting free energy only as thermodynamic variables and the amplitudes of the density oscillations.
  \item[{\it Excess Energy}] \hfill\\
  If the excess energy was not added in the above step, it is added here and also integrated over a unit cell. For the XPFC a little care must be taken if you decide to do the integration in full, since the kernel is a k-sapce term and a convolution is required. You will note that in all the files, the excess energy is just stated and not actually derived. It costs a lot of time and computation to do this, so after doing this for most of the crystal structures of interest, I just started writing the resulting solution. If you do not trust what I have done, I encourage you to do the convolution yourself as proof.
  \item[{\it Correlation Function}] \hfill\\
  Next, specifically for the XPFC, the correlation function and its peaks are defined along with any information of said peaks. For the case of alloys, the interpolation functions and the effective correlation function is also calculated during this step.
  \item[{\it Amplitude Minimization}] \hfill\\
  At this point we make an attempt to minimize out for the amplitudes. For multimode structures this is often accompanied by setting up some sort of ratio relationship between amplitudes of the various modes. This makes a drastic simplification, but it does give us a decent enough idea of the phase space. After the minimized amplitudes are determined, they are substituted back into the free energy which is now only a function of extensive and intensive thermodynamic variables, i.e., temperature, density, concentration etc.
  \item[{\it Free Energy Plot}] \hfill\\
  Once we have the free energy in terms of the thermodynamic variables, i.e., density, concentration, temperature etc, we define the rest of the parameters in the ideal and excess parts of the free energy and plot the energy of each of the phases defined for a given temperature. This gives us an idea of the energy landscape of the system as well as letting us know if we've made any gross errors in the previous steps leading to this.
  \item[{\it Phase Diagram Construction}] \hfill\\
  Assuming that we find the energy landscape suitable and appropriate (for various temperatures), we then proceed to the minimization of the thermodynamic variables, which naturally results in the phase diagram construction, this in the materials science field is referred to as the common tangent construction. If you are a physicist, you are probably more familiar with having it referred to as the Maxwell equal area construction.
\end{description}\hfill
\\
\noid{\bf c/c++ Codes}:\\
The c/c++  algorithms are used for actually testing out the predictions of the phase diagram or used to predict out-of-equilibrium phenomena. One of the simplest ways to test the validity of your code is to test it for equilibrium conditions before using it to study something more complicated. If it passes that test, then at the very least you know that your code works and is benchmarked. 

The codes you will find here have been written in c or c++, are solved using the semi-implicit spectral scheme that is popular in PFC numerics and is parallel using the message passing interface (MPI) algorithm. The parallel and spectral part are courtesy of either the {\bf FFTW2} or {\bf FFTW3} set of libraries which is open source.  Both are used I believe. The manual of this particular method can be easily downloaded from a simple search.  Note that because of the spectral algorithm, these codes only consider periodic boundary conditons. 

The codes themselves are not too extravagant, and have ample room for improvement. I have tried to be as modular as possible in that each part of the calculation required for the solution of the evolution of the system has its own function. These in principle could then be grouped into separate files or libraries. Like with the Maple files I generate a code for each model that is developed or each application. I have no doubt a more sophisticated approach can and should be employed, and perhaps libraries should be included in a single code where any of the various models can just be accessed as a module. 

Following a similar procedure like those in the Maple files, the simulation files are also laid out in a generic fashion across models or applications. The flow, when looking at the {\it main} function in the source file, goes something like:
\begin{description}
\item[{\it Simulation Set Up}] \hfill\\
The parallel, i.e., MPI, algorithm is started, parameters are read in from the input file, positions of peaks and other polynomial coefficients are set, specific simulation domain variables are set and the FFTW algorithm is initiated.
\item[{\it Conditions Set up}] \hfill\\
Arrays are allocated, the specific spectral algorithms are set up knowing the size of the system, initial conditions are created and placed in the domain, and other necessary parameters like information pertaining to the correlation function are also set.
\item[{\it Time Integration}] \hfill\\
Finally the last part of the codes deals with the actual time integration of the equations of motion. From time to time, the code also prints out data files of the fields for visualization and analysis purposes.
\end{description}\hfill\\
There should be a code or script for generating images from the data output floating around somewhere, but this isn't too difficult task to do either incase you don't find one. If any issues or questions should arise, like mentioned before seek help. I will be available for any questions or comments.
\bibliography{bibliomaster}
\end{document}
