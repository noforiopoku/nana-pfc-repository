#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <complex.h>
#include <fftw3-mpi.h>

#define Pi (2.*acos(0.))
#define dxa (0.125)			//grid spacing regular XPFC - for density reconstruction

//domain parameters
double dx;			//grid spacing
double spacing;		//normalized lattice spacing
int atomsx,atomsy;	//number of atoms in x and y
ptrdiff_t Nx,Ny;	//number of grid points in x and y
ptrdiff_t Nx2;		//needed for padding real arrays for transform

//time parameters
int totalTime,printFreq;	//total iteration time and print frequency
int printFreqAmp;			//frequency for outping all amplitudes
double dt;					//time step
int restartFlag;			//set to 1 for restart
int restartTime;			//restart time

//MPI parameters
int argc;
char **argv;
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

/**************** correlation variables ****************/
//k-zero mode
double qC0, w0;			//height of k-zero mode, width of k-zero mode
double PreC0;			//prefactor based on the widths of the k-zero mode

//first and second modes
double qC1,qC2;			//equilibrium peak positions for first and second mode
double w1,w2;			//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double rho1,rho2;		//atomic density of respective planes
double beta1,beta2;		//planar symmetry of respective planes

double DW1,DW2;			//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreC1,PreC2;		//prefactors calculated with the widths of the correlation peaks

/************* initial condition and simulation variables *************/
double iSig;		//initial temperature - related to vibrational amplitude of atom

double navg;		//average density
double ns_sq,ns_tri;//equilibrium average solid sq and tri density for temperature iSig
double nl;			//equilibrium liquid density for temperature iSig

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mno,Ma,Mb;	//mobility constant for density, mobility for amplitudes A and B

double facx,facy,normFac;	//fourier factors for scaling lengths in k-space
/*********************** general Arrays ****************************/
double *dens;			//hold the values for the density reconstruction
double *magA;//[4];			//holds the magnitue of A amplitudes respectively
double *magB;//[2];			//holds the magnitue of B amplitudes respectively

/*********************** average density ****************************/
double *no;		//average density
double *nofNL;	//holds collection of non-linear terms 
double *cubno;	//collection cubic terms (in terms of amplitudes)
double *sumA2;	//holds the sum of the magnitude squared of amplitudes A
double *sumB2;	//holds the sum of the magnitude squared of amplitudes B

fftw_complex *kno;		//complex density after transform
fftw_complex *knofNL;	//complex non-linear density terms after transform

fftw_plan planB_no, planF_no;	//backward and forward plans for density
fftw_plan planF_no_NL;			//non-linear forward plan

/*********************** reciprocal lattice vector quantities ****************************/
double q1x[4], q1y[4];	//indicies of the reciprocal lattice vectors --first mode
double magq1[4];				//magnitude of reciprocal lattice vectors --first mode

double q2x[2], q2y[2];	//indicies of the reciprocal lattice vectors --second mode
double magq2[2];				//magnitude of reciprocal lattice vectors --second mode

/*********************** fftw stuff for Amplitude A ****************************/
fftw_complex *A[4];			//amplitudes first mode
fftw_complex *AfNL[4];		//collection of non-linear terms
fftw_complex *cubA[4];		//cubic terms for non-linear
fftw_complex *quartA[4];	//quartic terms for non-linear terms

double *magA2m[4];		//magnitude of same index
double *sumA2j[4];		//sum of magnitude for indicies not equal to current

fftw_plan planF_A[4];		//forward plans for amplitude A
fftw_plan planB_A[4];		//backward plans for amplitude A
fftw_plan planF_ANL[4];		//non-linear forward plans for amplitude A

/*********************** fftw stuff for Amplitude B ****************************/
fftw_complex *B[2];			//amplitudes first mode
fftw_complex *BfNL[2];		//collection of non-linear terms
fftw_complex *cubB[2];		//cubic terms for non-linear
fftw_complex *quartB[2];	//quartic terms for non-linear terms

double *magB2m[2];		//magnitude of same index
double *sumB2j[2];		//sum of magnitude for indicies not equal to current

fftw_plan planF_B[2];		//forward plans for amplitude B
fftw_plan planB_B[2];		//backward plans for amplitude B
fftw_plan planF_BNL[2];		//non-linear forward plans for amplitude B


void freeMemory()
{
	int ii;
	/*********************** destroy fftw plans **************************/
	//averae density
	fftw_destroy_plan(planB_no);
	fftw_destroy_plan(planF_no);
	fftw_destroy_plan(planF_no_NL);

	//amplitude A
	for(ii=0;ii<4;ii++)
	{
		fftw_destroy_plan(planF_A[ii]);
		fftw_destroy_plan(planB_A[ii]);
		fftw_destroy_plan(planF_ANL[ii]);
	}

	//amplitude B
	for(ii=0;ii<2;ii++)
	{
		fftw_destroy_plan(planF_B[ii]);
		fftw_destroy_plan(planB_B[ii]);
		fftw_destroy_plan(planF_BNL[ii]);
	}

	/*********************** free memory **************************/
	free(dens);	free(magA);		free(magB);
	free(sumA2);	free(sumB2);
	free(no);	free(nofNL);	free(cubno);
		
	fftw_free(kno);	
	fftw_free(knofNL);

	//amplitude A
	for(ii=0;ii<4;ii++)
	{
		free(magA2m[ii]);
		free(sumA2j[ii]);

		fftw_free(cubA[ii]);
		fftw_free(quartA[ii]);
		fftw_free(A[ii]);
		fftw_free(AfNL[ii]);		
	}
	
	//amplitude B
	for(ii=0;ii<2;ii++)
	{
		free(magB2m[ii]);
		free(sumB2j[ii]);

		fftw_free(cubB[ii]);
		fftw_free(quartB[ii]);
		fftw_free(B[ii]);
		fftw_free(BfNL[ii]);		
	}
}
void restartReal(int time,double *Array,char *filepre)
{
	double *tmp;
	tmp = (double*) fftw_malloc(sizeof(double) * Ny*Nx );

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    fp = fopen(filename,"rt");
	
	ptrdiff_t i,j;
	ptrdiff_t jg;	//global index for y direction due to mpi
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			fscanf(fp,"%lf",&tmp[i+Nx*j]);
		}
	}
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+Nx2*jg] = tmp[i+Nx*j];
		}
	}
	
	free(tmp);
	MPI_Barrier(MPI_COMM_WORLD);
}
void outputReal(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);

	ptrdiff_t i,j;
	int kk;
	
	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(j=0;j<local_n0;j++)
			{
				for(i=0;i<Nx;i++)
				{
					fprintf(fp,"%lf\n",Array[i+Nx2*j]);
				}
			}
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void restartComplex(int time,fftw_complex *Array,char *filepre,int ampNum)
{
	double tmpR[Ny*Nx],tmpI[Ny*Nx];
	
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d_%d.dat",filepre,ampNum,time);
    fp = fopen(filename,"r");

    ptrdiff_t i,j;
	ptrdiff_t jg;	//global index for y direction due to mpi
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			fscanf(fp,"%lf %lf",&tmpR[i+Nx*j],&tmpI[i+Nx*j] );
		}
	}
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+Nx*jg] = tmpR[i+Nx*j] + I*tmpI[i+Nx*j];
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
}
void outputComplex(int time,fftw_complex *Array,char *filepre,int ampNum)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d_%d.dat",filepre,ampNum,time);

	ptrdiff_t i,j;
	int kk;
	
	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(j=0;j<local_n0;j++)
			{
				for(i=0;i<Nx;i++)
				{
					fprintf(fp,"%lf  %lf\n",creal( Array[i+Nx*j] ),cimag( Array[i+Nx*j] ) );
				}
			}
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void densReconAmp(double *Array,fftw_complex *ArrayA[4],fftw_complex *ArrayB[2])
{
	ptrdiff_t i,j;
	ptrdiff_t index,index2;
	ptrdiff_t yj;

	int ii;
	double Asum=0.,Bsum=0.;

	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i + Nx*j;
			index2 = i + Nx2*j;
						
			Asum = 0.;
			for(ii=0;ii<4;ii++)
				Asum += 2.*( creal( ArrayA[ii][index] )*cos( q1x[ii]*i*dxa + q1y[ii]*yj*dxa ) 
						-    cimag( ArrayA[ii][index] )*sin( q1x[ii]*i*dxa + q1y[ii]*yj*dxa ) );

			Bsum = 0.;
			for(ii=0;ii<2;ii++)
				Bsum += 2.*( creal( ArrayB[ii][index] )*cos( q2x[ii]*i*dxa + q2y[ii]*yj*dxa ) 
						-    cimag( ArrayB[ii][index] )*sin( q2x[ii]*i*dxa + q2y[ii]*yj*dxa ) );

			dens[index2] = 0.0;
			dens[index2] = Array[index2] + Asum + Bsum;
			
			magA[index2] = cabs( ArrayA[0][index]*conj( ArrayA[0][index] ) )
						 + cabs( ArrayA[1][index]*conj( ArrayA[1][index] ) )
						 + cabs( ArrayA[2][index]*conj( ArrayA[2][index] ) )
						 + cabs( ArrayA[3][index]*conj( ArrayA[3][index] ) );

			magB[index2] = cabs( ArrayB[0][index]*conj( ArrayB[0][index] ) )
						 + cabs( ArrayB[1][index]*conj( ArrayB[1][index] ) );			
		}
	}
	
}
void normalizeRealArrays(double *Array)
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	
	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;
			Array[index] *= normFac;
		}
	}
}
void normalizeComplexArrays(fftw_complex *Array)
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	
	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			Array[index] *= normFac;
		}
	}
}
void StepB()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;
	double prefactorB;
	int ii;
	
	double kx,ky,k2;
	double rk,kC1,kC2;
	double C2;

	/************** Step amplitude B *************/	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<Nx;i++)
		{
			if( i < Nx/2 ) 
				kx = i*facx;
			else 
				kx = (i-Nx)*facx;

			k2 = kx*kx + ky*ky ;

			index = i+Nx*j;

			for(ii=0;ii<2;ii++)
			{
				//rk = sqrt( k2 + 2.*( kx*q2x[ii] + ky*q2y[ii] ) + magq2[ii]*magq2[ii] );
				rk = sqrt( k2 - 2.*( kx*q2x[ii] + ky*q2y[ii] ) + magq2[ii]*magq2[ii] );

				//****************************C2**************************
				kC1 = exp( PreC1 * (rk-qC1)*(rk-qC1) );
				kC2 = exp( PreC2 * (rk-qC2)*(rk-qC2) );

				//creating correlation overlap
				if( DW1*kC1 > DW2*kC2 )
					C2 = DW1*kC1;
				else
					C2 = DW2*kC2;

				prefactorB = 1.0/( 1.0 + dt*Mb*( 1. - C2 ) );
			
				B[ii][index] = prefactorB*( B[ii][index] - dt*Mb*BfNL[ii][index] );
			}
		}
	}
}
void fcalNLB()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index2;
	int ii;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;

			for(ii=0;ii<2;ii++)
			{				
				BfNL[ii][index] = no[index2]*( -eta + chi*no[index2] )*B[ii][index] + ( -eta + 2.*chi*no[index2] )*cubB[ii][index]
								+ chi*B[ii][index]*( magB2m[ii][index2] + 2.*sumB2j[ii][index2] + 2.*sumA2[index2] ) + chi*quartB[ii][index] ;
			}
		}
	}
}
void supplArraysB()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index2;

	int ii,jj;
	double sum=0;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;
			
			for(ii=0;ii<2;ii++)
			{					
				//magnitude for each current amplitude
				magB2m[ii][index2] = cabs( B[ii][index] * conj(B[ii][index]) ) ;

				sum=0;
				for (jj=0;jj<2;jj++)
				{
					if( jj != ii )
						sum += cabs( B[jj][index] * conj(B[jj][index]) );
				}
					
				//sum of magnitudes of amplitude not including current
				sumB2j[ii][index2] = sum;
			}

			//from the product of the cubic terms
			cubB[0][index] = A[0][index]*A[3][index] ;

			cubB[1][index] = A[0][index]*conj( A[3][index] ) ;

			//fourth order terms from the expansion
			quartB[0][index] = 2.*A[3][index]*conj( A[1][index] )*conj( A[2][index] )
							 + A[3][index]*A[3][index]*B[1][index]
							 + A[0][index]*A[0][index]*conj( B[1][index] ) ;
				
			quartB[1][index] = 2.*conj( A[1][index] )*conj( A[2][index] )*conj( A[3][index] )
							 + conj( A[3][index] )*conj( A[3][index] )*B[0][index]
							 + A[0][index]*A[0][index]*conj( B[0][index] ) ;
		}
	}
}
void StepA()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;
	double prefactorA;
	int ii;
	
	double kx,ky,k2;
	double rk,kC1,kC2;
	double C2;

	/************** Step amplitude A *************/	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<Nx;i++)
		{
			if( i < Nx/2 ) 
				kx = i*facx;
			else 
				kx = (i-Nx)*facx;

			k2 = kx*kx + ky*ky ;

			index = i+Nx*j;

			for(ii=0;ii<4;ii++)
			{
				//rk = sqrt( k2 + 2.*( kx*q1x[ii] + ky*q1y[ii] ) + magq1[ii]*magq1[ii] );
				rk = sqrt( k2 - 2.*( kx*q1x[ii] + ky*q1y[ii] ) + magq1[ii]*magq1[ii] );

				//****************************C2**************************
				kC1 = exp( PreC1 * (rk-qC1)*(rk-qC1) );
				kC2 = exp( PreC2 * (rk-qC2)*(rk-qC2) );

				//creating correlation overlap
				if( DW1*kC1 > DW2*kC2 )
					C2 = DW1*kC1;
				else
					C2 = DW2*kC2;

				prefactorA = 1.0/( 1.0 + dt*Ma*( 1. - C2 ) );
			
				A[ii][index] = prefactorA*( A[ii][index] - dt*Ma*AfNL[ii][index] );
			}
		}
	}
}
void fcalNLA()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index2;
	int ii;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;

			for(ii=0;ii<4;ii++)
			{				
				AfNL[ii][index] = no[index2]*( -eta + chi*no[index2] )*A[ii][index] + ( -eta + 2.*chi*no[index2] )*cubA[ii][index]
								+ chi*A[ii][index]*( magA2m[ii][index2] + 2.*sumA2j[ii][index2] + 2.*sumB2[index2] ) + chi*quartA[ii][index] ;
			}
		}
	}
}
void supplArraysA()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index2;

	int ii,jj;
	double sum=0;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;
			
			for(ii=0;ii<4;ii++)
			{					
				//magnitude for each current amplitude
				magA2m[ii][index2] = cabs( A[ii][index] * conj(A[ii][index]) ) ;

				sum=0;
				for (jj=0;jj<4;jj++)
				{
					if( jj != ii )
						sum += cabs( A[jj][index] * conj(A[jj][index]) );
				}
					
				//sum of magnitudes of amplitude not including current
				sumA2j[ii][index2] = sum;
			}

			//from the product of the cubic terms
			cubA[0][index] = conj( A[1][index] )*conj( A[2][index] )
						   + A[3][index]*B[1][index]+conj( A[3][index] )*B[0][index] ;

			cubA[1][index] = conj( A[0][index] )*conj( A[2][index] ) ;

			cubA[2][index] = conj( A[0][index] )*conj( A[1][index] ) ;

			cubA[3][index] = conj( A[0][index] )*B[0][index]
						   + A[0][index]*conj( B[1][index] ) ;

			//fourth order terms from the expansion
			quartA[0][index] = 2.*conj( A[0][index] )*B[0][index]*B[1][index] ;
				
			quartA[1][index] = 2.*( A[3][index]*conj( A[2][index] )*conj( B[0][index] )
							 + conj( A[2][index] )*conj( A[3][index] )*conj( B[1][index] ) );
			
			quartA[2][index] = 2.*( A[3][index]*conj( A[1][index] )*conj( B[0][index] )
							 + conj( A[1][index] )*conj( A[3][index] )*conj( B[1][index] ) );
			
			quartA[3][index] = 2.*( A[1][index]*A[2][index]*B[0][index]
							 + conj( A[1][index] )*conj( A[2][index] )*conj( B[1][index] )
							 + conj( A[3][index] )*B[0][index]*conj( B[1][index] ) );
		}
	}
}
void Stepno()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;
	double prefactorno;
	
	double kx,ky,k2;

	/************** Step average density *************/
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			kx = i*facx;
			k2 = kx*kx + ky*ky ;

			index = i+(Nx/2+1)*j;

			prefactorno = 1.0/( 1.0 + dt*Mno*k2 );
			
			kno[index] = prefactorno*( kno[index] - dt*Mno*k2*knofNL[index] );
		}
	}
}
void fcalNLno()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;

			nofNL[index] = no[index]*no[index]*( -0.5*eta + chi*no[index]/3. )
							+ ( -eta + 2.*chi*no[index] )*( sumA2[index] + sumB2[index] ) + 2.*chi*cubno[index] ;
		}
	}
}
void supplArraysno()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index2;
	
	int ii;
	double sumA=0;
	double sumB=0;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;

			sumA=0;
			for(ii=0;ii<4;ii++)
				sumA += cabs( A[ii][index] * conj(A[ii][index]) ) ;

			//total sum of magnitudes of all amplitudes A
			sumA2[index2] = sumA;

			sumB=0;
			for(ii=0;ii<2;ii++)
				sumB += cabs( B[ii][index] * conj(B[ii][index]) ) ;				

			//total sum of magnitudes of all amplitudes B
			sumB2[index2] = sumB;

			//sum of the product of the amplitudes cubic terms				
			cubno[index2] = conj( A[0][index] )*conj( A[1][index] )*conj( A[2][index] )
						  + A[3][index]*conj( A[0][index] )*B[1][index]
						  + conj( A[0][index] )*conj( A[3][index] )*B[0][index]
						  + A[0][index]*A[3][index]*conj( B[0][index] )
						  + A[0][index]*conj( A[3][index] )*conj( B[1][index] )
						  + A[0][index]*A[1][index]*A[2][index] ;
		}
	}
}
void setDebyeWaller(double Sig)
{
	DW1 = exp(-0.5*Sig*Sig*qC1*qC1/(rho1*beta1) );
	DW2 = exp(-0.5*Sig*Sig*qC2*qC2/(rho2*beta2) );
}
void setCorrPeakPrefactors()
{
	PreC0 = -0.5/(w0*w0);
	PreC1 = -0.5/(w1*w1);
	PreC2 = -0.5/(w2*w2);
}
void initialize()
{	
	ptrdiff_t i,j;
	ptrdiff_t index,index2;
	ptrdiff_t yj;
	int ii;
	
	double asq1 = 0.2;
	double asq2 = .1;
	double atri = .13;
	
	double radius = 20.;
	double hfthick = 20.;
	double len = 300.;
	//double fs = (Pi*radius*radius)/(Ny*Nx);
	//double fs = 2.*hfthick*Nx/(Ny*Nx);
	
	//double fs_sq = (Nx/2*len)/(Ny*Nx);
	//double fs_tri = (Nx/2*len)/(Ny*Nx);

	double angtri = 0.*Pi/180.;
	double angsq = -2.*Pi/180.;
	
	double qrx,qry;

	int k;
	int ng = 100;	//number of grains
	double sepx[ng],sepy[ng],ang[ng];	//array for number of grains
	double fs_tri = ng*(Pi*radius*radius)/(Ny*Nx);

	//***************begin initial conditions******************

	/*//multiple grains triangle
	for(k=0;k<ng;k++)
	{
		//generating a random orientation for the seed.
		ang[k] = (2.*drand48()-1.) * (Pi/6.);
									
		sepx[k] = drand48() * Nx;
		sepy[k] = drand48() * Ny;
		
		//placement of any two grains sufficiently far apart
		for (j=0;j<k;j++)
		{
			if ( sqrt( (sepx[j]-sepx[k])*(sepx[j]-sepx[k]) + (sepy[j]-sepy[k])*(sepy[j]-sepy[k]) ) 
				<= .5*(sqrt((double)(Nx*Ny)/(double)ng)) )
			{
				k--;
				break;
			}
		}			
	}
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;

			//************************** initially mean density and zero amplitudes (liquid) **************************
			no[index2] = navg;

			//first mode amplitude
			for(ii=0;ii<4;ii++)
				A[ii][index] = 0. + I*0.;

			//second mode amplitude
			for(ii=0;ii<2;ii++)
				B[ii][index] = 0. + I*0.;

			for(k=0;k<ng;k++)
			{
				if ( sqrt( (i-sepx[k])*(i-sepx[k]) + (yj-sepy[k])*(yj-sepy[k]) ) < radius ) 
				{
					//****************tirangle******************
					//amplitude A
					for(ii=0;ii<3;ii++)
					{
						//rotation of reciprocal lattice vector
						qrx = q1x[ii]*cos(ang[k]) - q1y[ii]*sin(ang[k]);
						qry = q1x[ii]*sin(ang[k]) + q1y[ii]*cos(ang[k]);

						A[ii][index] = atri*( cos( (qrx-q1x[ii])*i*dxa + (qry-q1y[ii])*yj*dxa ) 
										  + I*sin( (qrx-q1x[ii])*i*dxa + (qry-q1y[ii])*yj*dxa ) );
					}
				
					//average density
					no[index2] = ns_tri;
				}
				else
				{
					//avgerage density in liquid according to the lever rule
					no[index2] = ( navg - ns_tri*fs_tri )/( 1. - fs_tri );
				}
			}
		}
	}*/

	/*//Peritectic fingers 
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;

			//************************** initially mean density and zero amplitudes (liquid) **************************
			no[index2] = navg;

			//first mode amplitude
			for(ii=0;ii<4;ii++)
				A[ii][index] = 0. + I*0.;

			//second mode amplitude
			for(ii=0;ii<2;ii++)
				B[ii][index] = 0. + I*0.;

			if(yj < len )
			{
				if( i <= Nx/4 || ( i > Nx/2 && i <= 3*Nx/4 ) )
				{
					//****************square******************
					//amplitude A
					for(ii=0;ii<4;ii++)
					{
						if( ii%3 == 0 )
						{
							//rotation of reciprocal lattice vector
							qrx = q1x[ii]*cos(angsq) - q1y[ii]*sin(angsq);
							qry = q1x[ii]*sin(angsq) + q1y[ii]*cos(angsq);
						
							A[ii][index] = asq1*( cos( (qrx-q1x[ii])*i*dxa + (qry-q1y[ii])*yj*dxa ) 
											  + I*sin( (qrx-q1x[ii])*i*dxa + (qry-q1y[ii])*yj*dxa ) ) ;
						}
					}

					//amplitude B
					for(ii=0;ii<2;ii++)
					{
						//rotation of reciprocal lattice vector
						qrx = q2x[ii]*cos(angsq) - q2y[ii]*sin(angsq);
						qry = q2x[ii]*sin(angsq) + q2y[ii]*cos(angsq);

						B[ii][index] = asq2*( cos( (qrx-q2x[ii])*i*dxa + (qry-q2y[ii])*yj*dxa ) 
										  + I*sin( (qrx-q2x[ii])*i*dxa + (qry-q2y[ii])*yj*dxa ) );
					}

					//average density
					no[index2] = ns_sq;
				}
				else
				{
					//****************tirangle******************
					//amplitude A
					for(ii=0;ii<3;ii++)
					{
						//rotation of reciprocal lattice vector
						qrx = q1x[ii]*cos(angtri) - q1y[ii]*sin(angtri);
						qry = q1x[ii]*sin(angtri) + q1y[ii]*cos(angtri);

						A[ii][index] = atri*( cos( (qrx-q1x[ii])*i*dxa + (qry-q1y[ii])*yj*dxa ) 
										  + I*sin( (qrx-q1x[ii])*i*dxa + (qry-q1y[ii])*yj*dxa ) );
					}
				
					//average density
					no[index2] = ns_tri;
				}
			}
			else
			{
				//avgerage density in liquid according to the lever rule
				no[index2] = ( navg - (ns_sq*fs_sq + ns_tri*fs_tri) )/( 1. - (fs_sq + fs_tri) );
			}
		}
	}*/
	
	//sphere/slab in middle of domain
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx*j;
			index2 = i+Nx2*j;

			//************************** initially mean density and zero amplitudes (liquid) **************************
			no[index2] = navg;

			//first mode amplitude
			for(ii=0;ii<4;ii++)
				A[ii][index] = 0. + I*0.;

			//second mode amplitude
			for(ii=0;ii<2;ii++)
				B[ii][index] = 0. + I*0.;

			//if( sqrt( (yj-Ny/2.)*(yj-Ny/2.) + (i-Nx/2.)*(i-Nx/2.) ) <= radius )
			if( yj<(Ny/2.+hfthick) && yj>(Ny/2.-hfthick)  )
			{
				//****************square******************
				//amplitude A
				for(ii=0;ii<4;ii++)
					if( ii%3 == 0 )
						A[ii][index] = asq1 + I*0.;

				//amplitude B
				for(ii=0;ii<2;ii++)
					B[ii][index] = asq2 + I*0.;			

				//average density
				//no[index2] = ns_sq;

				//****************tirangle******************
				//amplitude A
				//for(ii=0;ii<3;ii++)
				//	A[ii][index] = atri + I*0.;
				
				//no[index2] = ns_tri;
			}
			else
			{
				//avgerage density in liquid according to the lever rule
				//no[index2] = ( navg - ns_sq*fs )/( 1. - fs );

				//no[index2] = ( navg - ns_tri*fs )/( 1. - fs );
			}
		}
	}
}
void basisVectors()
{
	/************* reciprocal vectors and magnitudes - first mode ******************/
	q1x[0] = 0.;					q1y[0] = 2.*Pi;
	q1x[1] = -2.*Pi*sqrt(3.)/2.;	q1y[1] = -2.*Pi*0.5;
	q1x[2] = 2.*Pi*sqrt(3.)/2.;		q1y[2] = -2.*Pi*0.5;
	q1x[3] = 2.*Pi;					q1y[3] = 0.;

	magq1[0] = sqrt( q1x[0]*q1x[0] + q1y[0]*q1y[0] );
	magq1[1] = sqrt( q1x[1]*q1x[1] + q1y[1]*q1y[1] );
	magq1[2] = sqrt( q1x[2]*q1x[2] + q1y[2]*q1y[2] );
	magq1[3] = sqrt( q1x[3]*q1x[3] + q1y[3]*q1y[3] );

	/************* reciprocal vectors and magnitudes - second mode ******************/
	q2x[0] = 2.*Pi;		q2y[0] = 2.*Pi;
	q2x[1] = -2.*Pi;	q2y[1] = 2.*Pi;

	magq2[0] = sqrt( q2x[0]*q2x[0] + q2y[0]*q2y[0] );
	magq2[1] = sqrt( q2x[1]*q2x[1] + q2y[1]*q2y[1] );
}
void setfftwPlans()
{
	int ii;

	//setting up fftw transforms

	/************** no **************/	
	planF_no = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, no, kno, MPI_COMM_WORLD, FFTW_MEASURE);
	planB_no = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kno, no, MPI_COMM_WORLD, FFTW_MEASURE);

	planF_no_NL = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, nofNL, knofNL, MPI_COMM_WORLD, FFTW_MEASURE);

	/***************** amplitude A ************************/
	for(ii=0;ii<4;ii++)
	{
		planF_A[ii] = fftw_mpi_plan_dft_2d(Ny, Nx, A[ii], A[ii], MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);
		planB_A[ii] = fftw_mpi_plan_dft_2d(Ny, Nx, A[ii], A[ii], MPI_COMM_WORLD, FFTW_BACKWARD, FFTW_MEASURE);
		planF_ANL[ii] = fftw_mpi_plan_dft_2d(Ny, Nx, AfNL[ii], AfNL[ii], MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);		
	}

	/***************** amplitude B ************************/
	for(ii=0;ii<2;ii++)
	{
		planF_B[ii] = fftw_mpi_plan_dft_2d(Ny, Nx, B[ii], B[ii], MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);
		planB_B[ii] = fftw_mpi_plan_dft_2d(Ny, Nx, B[ii], B[ii], MPI_COMM_WORLD, FFTW_BACKWARD, FFTW_MEASURE);
		planF_BNL[ii] = fftw_mpi_plan_dft_2d(Ny, Nx, BfNL[ii], BfNL[ii], MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);		
	}
}
void allocateFft()
{
	int ii;

	//general arrays
	dens = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );
	magA = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );
	magB = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );

	/***************** no ************************/	
	no = (double*) fftw_malloc( sizeof (double*) * 2*alloc_local );
	nofNL = (double*) fftw_malloc( sizeof (double*) * 2*alloc_local );

	kno = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
	knofNL = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );

	sumA2 = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );
	sumB2 = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );
	cubno = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );

	/***************** amplitude A ************************/
	for(ii=0;ii<4;ii++)
	{
		A[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
		AfNL[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );

		cubA[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
	
		magA2m[ii] = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );
		sumA2j[ii] = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );

		quartA[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
	}

	/***************** amplitude B ************************/
	for(ii=0;ii<2;ii++)
	{
		B[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
		BfNL[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );

		cubB[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
	
		magB2m[ii] = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );
		sumB2j[ii] = (double*) fftw_malloc(sizeof(double) * 2*alloc_local );

		quartB[ii] = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
	}
}
void fftwMPIsetup()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);

	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_2d(Ny, Nx, MPI_COMM_WORLD, &local_n0, &local_0_start);
}
void domainParams()
{
	//set up domain parameters
	printf("dx =%f\n",dx);
	printf("Nx = %d\n",Nx);
	printf("Ny = %d\n",Ny);
	
	atomsx = (int)( floor(dxa/spacing*Nx) );
	atomsy = (int)( floor(dxa/spacing*Ny) );

	printf("dxa=%f  numAtomsx=%d numAtomsy=%d\n",dxa, atomsx,atomsy);
	
	Nx2 = 2*(Nx/2+1);

	//set up fourier scaling factors
	facx = 2.*Pi/(Nx*dx);
	facy = 2.*Pi/(Ny*dx);
	normFac = 1.0/(Nx*Ny);
	
	printf("facx=%lf\n",facx);
	printf("facy=%lf\n",facy);
	printf("normFac=%0.10lf\n",normFac);
}
void inputVariables()
{
	char *line = (char *) malloc (BUFSIZ * sizeof(char));
	FILE *in;
	in = fopen("inputSqTriAmpMPI","rt");
   
	printf("reading input file\n");
	if (in == NULL)
	{
		printf("Error opening input file\n");
		printf("Either file does not exit or incorrect file name\n");
		printf("Exiting simulation!\n");
		exit(1);	
	}	

	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &Nx,&Ny);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d", &printFreqAmp);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&qC0,&w0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&w1,&w2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&rho1,&rho2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&beta1,&beta2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf", &iSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&navg,&nl);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&ns_sq,&ns_tri);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&Mno,&Ma,&Mb);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);

	printf("done reading input file\n");
	
	printf("Nx=%d	Ny=%d\n",Nx,Ny);
	printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
	printf("totalTime=%d	printFreq=%d\n",totalTime,printFreq);
	printf("printFreqAmp=%d\n",printFreqAmp);
	printf("qC0=%lf	w0=%lf\n",qC0,w0);
	printf("w1=%lf	w2=%lf\n",w1,w2);
	printf("rho1=%lf	rho2=%lf\n",rho1,rho2);
	printf("beta1=%lf	beta2=%lf\n",beta1,beta2);
	printf("iSig=%lf\n",iSig);
	printf("navg=%lf	nl=%lf\n",navg,nl);
	printf("ns_sq=%lf	ns_tri=%lf\n",ns_sq,ns_tri);
	printf("eta=%lf	chi=%lf\n",eta,chi);
	printf("Mno=%lf	Ma=%lf	Mb=%lf\n",Mno,Ma,Mb);
	printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);

}
void iterateno()
{
	/****************** average density ******************/
	supplArraysno();			//calculate supplementary arrays need for updating average density
	fcalNLno();					//calculate non-linear terms for density

	fftw_execute(planF_no);		//fourier transform average density
	fftw_execute(planF_no_NL);	//foruier transform average density non-linear terms
		
	Stepno();					//update density in k-space
	fftw_execute(planB_no);		//inverse transform average density
	normalizeRealArrays(no);	//normalize average density after transform
}
void iterateA()
{
	int ii;
	/****************** amplitude A ******************/
	supplArraysA();		//calculate supplementary arrays need for updating amplitude A
	fcalNLA();			//calculate non-linear terms for amplitude A

	for(ii=0;ii<4;ii++)
	{
		fftw_execute(planF_A[ii]);		//fourier transform (in place) complex amplitude A
		fftw_execute(planF_ANL[ii]);	//foruier transform non-linear terms (in place)
	}
	
	StepA();			//update amplitude A in k-space
	for(ii=0;ii<4;ii++)
	{
		fftw_execute(planB_A[ii]);		//inverse transform (in place)
		normalizeComplexArrays(A[ii]);	//normailze amplitude after inverse transform
	}
}
void iterateB()
{
	int ii;
	/****************** amplitude B ******************/
	supplArraysB();		//calculate supplementary arrays need for updating amplitude B
	fcalNLB();			//calculate non-linear terms for amplitude B

	for(ii=0;ii<2;ii++)
	{
		fftw_execute(planF_B[ii]);		//fourier transform (in place) complex amplitude B
		fftw_execute(planF_BNL[ii]);	//foruier transform non-linear terms (in place)
	}
	
	StepB();			//update amplitude B in k-space
	for(ii=0;ii<2;ii++)
	{
		fftw_execute(planB_B[ii]);		//inverse transform (in place)
		normalizeComplexArrays(B[ii]);	//normailze amplitude after inverse transform
	}
}
int main()
{
	int iter;
	int ii;
	
	//set up the equil'm position for first and second peaks
	qC1 = 2.*Pi;	//first mode
	qC2 = sqrt(2.)*2.*Pi;			//second mode

	//read from file the input values
	inputVariables();

	//set up domain parameters
	domainParams();
	
	//setup the fftw mpi environment
	fftwMPIsetup();

	//allocating relevant arays
	allocateFft();
	printf("arrays have been allocated\n");

	//setting up fft transform plans
	setfftwPlans();
	printf("fftw plans have been created and set\n");

	//set the reciprocal lattice vectors
	basisVectors();
	printf("reciprocal basis vectors have been set\n");

	//initializa random number generator
	srand48(time(NULL)+myid);

	//initial conditions
	if (restartFlag == 0)
	{
		initialize();

		//output initial conditions
		//density
		printf("output 0\n");
		outputReal(0,no,"dens");

		//complex amplitudes
		for(ii=0;ii<4;ii++)
			outputComplex(0,A[ii],"ampA",ii);

		for(ii=0;ii<2;ii++)
			outputComplex(0,B[ii],"ampB",ii);
		
		//density reconstruction
		densReconAmp(no,A,B);
		outputReal(0,dens,"rcdens");
		outputReal(0,magA,"mAamp");
		outputReal(0,magB,"mBamp");
		
		printf("initializing domain complete\n");
	}
	else
	{
		printf("restart flag has been triggered\n");
		printf("restarting simulation from time step = %d\n",restartTime);

		//read in average density file
		restartReal(restartTime,no,"dens");

		//read in complex amplitude files
		for(ii=0;ii<4;ii++)
			restartComplex(restartTime,A[ii],"ampA",ii);

		for(ii=0;ii<2;ii++)
			restartComplex(restartTime,B[ii],"ampB",ii);
		
		printf("domain has been reinitialized with restart file\n");

		outputReal(0,no,"densR");
		//complex amplitudes
		for(ii=0;ii<4;ii++)
			outputComplex(0,A[ii],"ampAR",ii);

		for(ii=0;ii<2;ii++)
			outputComplex(0,B[ii],"ampBR",ii);
		
		//density reconstruction
		densReconAmp(no,A,B);
		outputReal(0,dens,"rcdensR");
		outputReal(0,magA,"mAampR");
		outputReal(0,magB,"mBampR");
	}

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();
	printf("correlation peak prefactors calculated\n");
	
	//NOTE:: if you wish to do cooling, this function should be called in the iteration loop
	//calculate the debye-waller portion of correlations
	setDebyeWaller(iSig);
	printf("debye-waller factors calculated\n");
	
	/*********************************************************************************************************/
	//begin interations
	printf("begin time iteration\n");	
	for(iter=restartTime+1; iter<totalTime+restartTime+1; iter++)
	{
		iterateno();		//performs function calls for updating average density
		iterateA();			//performs function calls for updating A amplitudes
		iterateB();			//performs function calls for updating B amplitudes		

		//output current data
		if( (iter)%printFreq == 0 )
		{
			printf("time = %d\n",iter);
			
			//output average density
			outputReal(iter,no,"dens");

			//density reconstruction, amplitude magnitude and output
			densReconAmp(no,A,B);
			outputReal(iter,dens,"rcdens");
			outputReal(iter,magA,"mAamp");
			outputReal(iter,magB,"mBamp");
		}

		//output amplitudes for restart
		if( (iter)%printFreqAmp == 0 )
		{
			printf("restart output for amplitudes\n");
			printf("time = %d\n",iter);

			//output complex amplitude
			for(ii=0;ii<4;ii++)
				outputComplex(iter,A[ii],"ampA",ii);

			for(ii=0;ii<2;ii++)
				outputComplex(iter,B[ii],"ampB",ii);

			//density reconstruction, amplitude magnitude and output
			densReconAmp(no,A,B);
			outputReal(iter,dens,"rcdens");
			outputReal(iter,magA,"mAamp");
			outputReal(iter,magB,"mBamp");
		}
	}
	
	MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point	
	freeMemory();  //free memory and destroy fftw plans and arrays

	//exit mpi environment
	MPI_Finalize();

	return 0;
}
