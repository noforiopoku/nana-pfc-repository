#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "fftw.h"
#include "rfftw.h"
#include "fftw_mpi.h"
#include "rfftw_mpi.h"

const double Pi = 3.14159;

double dx;
int Nx,Ny;

int endtime,timefreq;
double dt;
int restartflag;	//set to 1 for restart
int restart_time;	//restart time

double qo;			//equilibrium wave vector for structure
double eps, no;		//temperature and average density
double feps;		//final temperature
int itereps;		//number of iterations needed to traverse temperature change
double nAmp;		//noise amplitude
double M;			//mobility of density

double iRad,gama;	//radius of cascade event, intensity of bombardment
int radFlag,radtime;	//radiation flag
double iRad_d,gama_d;	//temporary variables for radius of cascade event, intensity of bombardment

int int_cond;	//flag for initial condition,//1 - circular seed//2 - multiple orientations//3 - bi-crystal

//mpi variables
int argc;
char **argv;
int myid,numprocs;
int local_nlast, local_last_start, local_nlast2_after_trans,
local_last2_start_after_trans, total_local_size;

//real space arrays and variables
double *n,*nf;			//fields
double *fNL;		//holds collection of non-linear terms
double *gNoisex,*gNoisey;		//gaussian (normal) distributed noise
double *work;			//workspace array...allows for use of MPI_Alltoall primitive for communications

//fft variables and plans
fftw_complex *kn;		//k-space for field
fftw_complex *kfNL;		//holds k-space collection of non-linear terms
fftw_complex *kgNoisex,*kgNoisey;		//k-space noise terms

rfftwnd_mpi_plan plan_B;
rfftwnd_mpi_plan plan_F;


void restart(int time,double *Array,char *filepre)
{
	double tmp[Nx*Ny];
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    fp = fopen(filename,"rt");

    int i,j;
	int ig,index;	//global index

	MPI_Barrier(MPI_COMM_WORLD);

	for(i=0;i<Ny;i++)
		for(j=0;j<Nx;j++)
			fscanf(fp,"%lf",&tmp[j+i*(Nx)]);

    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

    for(i=0;i<Ny;i++)
    {
		for(j=0;j<Nx;j++)
        {
			ig = i - myid*local_nlast;	//rescale coordinates to that of local processor
			if (ig >= 0 && ig <= local_nlast-1)
				Array[j+ig*(Nx)] = tmp[j+i*(Nx)];
        }
    }
}
void output(int time,double *Array,char *filepre)
{
	int i,j;
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    int k;

	MPI_Barrier(MPI_COMM_WORLD);

	for (k=0;k<numprocs;k++)
	{
		if (myid == k)
		{
			if (myid==0)
				fp = fopen(filename,"w");
			else
				fp = fopen(filename,"a");
   
			for(i=0;i<local_nlast;i++)
			{
				for(j=0;j<Nx;j++)
				{
					fprintf(fp,"%f  \n",Array[j+i*(Nx)]);
				}
			}
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void Ln()
{
	int j,index,i;
	int ind2;

	for(i=0;i<local_nlast;i++)
	{
		for(j=0;j<Nx;j++)
		{
			index = j+i*Nx;
			ind2 = j+i*(2*(Nx/2+1));
			
			nf[ind2] = n[index];
		}
	}
	rfftwnd_mpi(plan_F, 1, nf, work, FFTW_NORMAL_ORDER);
}
void fcalNL()
{
	int j,index,i;
	int ind2;

	for(i=0;i<local_nlast;i++)
	{
		for(j=0;j<Nx;j++)
		{
			index = j+i*Nx;
			ind2 = j+i*(2*(Nx/2+1));

			fNL[ind2] = n[index]*n[index]*n[index];
		}
	}
	rfftwnd_mpi(plan_F, 1, fNL, work, FFTW_NORMAL_ORDER);
}
void calcNoise()
{
	int j,index,i;
	double x1,x2,rsqd,r;

	for(i=0;i<local_nlast;i++)
	{
		for(j=0;j<Nx;j++)
		{
			index = j+i*(2*(Nx/2+1));
			do 
			{
				x1 = 2*drand48() - 1.;
				x2 = 2*drand48() - 1.;

				rsqd = x1*x1 + x2*x2;
			} 
			while ( rsqd >= 1.0 );

            r=sqrt(-2.0*log(rsqd)/rsqd);
			
			gNoisex[index] = nAmp*x1*r;
			gNoisey[index] = nAmp*x2*r;
		}
	}

	rfftwnd_mpi(plan_F, 1, gNoisex, work, FFTW_NORMAL_ORDER);
	rfftwnd_mpi(plan_F, 1, gNoisey, work, FFTW_NORMAL_ORDER);
}
void fPsi()
{
	int i,j,kx,index;
	double facx,facy,prefactor_n,k2;
	int ind2;
	int iy;

	double gk;	//radiation kernel

	double kfacx,kfacy,kxf,kyf;

	facx = 4.*Pi*Pi/(Nx*Nx*dx*dx);
	facy = 4.*Pi*Pi/(Ny*Ny*dx*dx);

	kfacx = 2.*Pi/(Nx*dx);
	kfacy = 2.*Pi/(Ny*dx);

	/************** Step *************/
	for(i=0;i<local_nlast;i++)
	{
		iy = i+local_last_start;
		if(iy < Ny/2) 
			kx = iy;
		else 
			kx = (iy-Ny);
		for(j=0;j<(Nx/2+1);j++)
		{
			index = j+i*(Nx/2+1);
			k2 = kx*kx*facy+j*j*facx;

			kyf = kx*kfacy;
			kxf = j*kfacx;

			gk = gama*iRad*iRad/(1.+ iRad*iRad*k2);
						
			prefactor_n = 1.0/( 1.0 + dt*k2*M*( eps + 1.-2.*k2 + k2*k2 + gk ) );
			
			kn[index].re = prefactor_n*( kn[index].re - dt*k2*M*kfNL[index].re 
				- dt*kxf*kgNoisex[index].im - dt*kyf*kgNoisey[index].im );

			kn[index].im = prefactor_n*( kn[index].im - dt*k2*M*kfNL[index].im 
				- dt*kxf*kgNoisex[index].re - dt*kyf*kgNoisey[index].re );
		}
	}

	rfftwnd_mpi(plan_B, 1, nf, work, FFTW_NORMAL_ORDER);

	for(i=0;i<local_nlast;i++)
	{
		for(j=0;j<Nx;j++)
		{
			index = j+i*Nx;
			ind2 = j+i*(2*(Nx/2+1));

			n[index] = nf[ind2]/(Nx*Ny);
		}
	}
}

void initialize()
{
	//flag for either bar or circle initial condition
	//1 - 3 sided embedded grain						
	
	double Radius = 10.;	//radius of circular seed
	double hfwidth = 10.;	//half thickness of bar in domain
	int i,j,index;
	int iy;

	int x,y;

	//multiple grain variables
	int k;

	double atri = 0.23;	
	double Angle=Pi;		//angles for seeing bi-crystal

	double ang1=20*Pi/180.;
	double ang2=-20*Pi/180.;
	double ang3=50*Pi/180.;
	double ang4=0*Pi/180.;

	//begin possible initial initial conditions
	if ( int_cond == 1 )
	{
		for(i=0;i<local_nlast;i++)
		{
			iy = i + local_last_start;
			for(j=0;j<Nx;j++)
			{
				index = j+i*Nx;
				//initially mean density
				n[index] = no;
							
				if( iy > 3*Ny/4. && j < Nx/2. ) //seed 1 if( sqrt( (iy-3*Ny/4.)*(iy-3*Ny/4.) + (j-Nx/4.)*(j-Nx/4.) ) <= Radius+20 ) //seed 1
				{
					x=j*cos(ang1)+iy*sin(ang1);
					y=iy*cos(ang1)-j*sin(ang1);
					//triangle
					n[index] = no + atri * ( cos(qo*dx*x)*cos(qo/sqrt(3.)*dx*y) - 0.5*cos(dx*y) );	
				}
				if( iy > 3*Ny/4. && j >= Nx/2. ) //seed 2 else if( sqrt( (iy-3*Ny/4.)*(iy-3*Ny/4.) + (j-3*Nx/4.)*(j-3*Nx/4.) ) <= Radius+20 ) //seed 2
				{
					x=j*cos(ang2)+iy*sin(ang2);
					y=iy*cos(ang2)-j*sin(ang2);
					//triangle
					n[index] = no + atri * ( cos(qo*dx*x)*cos(qo/sqrt(3.)*dx*y) - 0.5*cos(dx*y) );	
				}
				else if( iy < 20. ) //seed 3  else if( sqrt( (iy-Ny)*(iy-Ny) + (j-Nx/2.)*(j-Nx/2.) ) <= 500 ) //seed 3 
				{
					x=j*cos(ang3)+iy*sin(ang3);
					y=iy*cos(ang3)-j*sin(ang3);
					//triangle
					n[index] = no + atri * ( cos(qo*dx*x)*cos(qo/sqrt(3.)*dx*y) - 0.5*cos(dx*y) );	
				}
				else if( sqrt( (iy-Ny/2.)*(iy-Ny/2.) + (j-Nx/2.)*(j-Nx/2.) ) <= (Radius) ) //seed 4
				{
					x=j*cos(ang4)+iy*sin(ang4);
					y=iy*cos(ang4)-j*sin(ang4);
					//triangle
					n[index] = no + atri * ( cos(qo*dx*x)*cos(qo/sqrt(3.)*dx*y) - 0.5*cos(dx*y) );	
				}
			}
		}
	}
}
void allocateFft()
{
	/***************** n ************************/	
	n = (double*)malloc ( sizeof ( double ) * total_local_size );
	nf = (double*)malloc ( sizeof ( double ) * total_local_size );
	fNL = (double*)malloc ( sizeof ( double ) * total_local_size );

	kn = (fftw_complex*) nf;
	kfNL = (fftw_complex*) fNL;

	/*********************noise*****************/
	gNoisex = (double*)malloc ( sizeof ( double ) * total_local_size );
	gNoisey = (double*)malloc ( sizeof ( double ) * total_local_size );

	kgNoisex = (fftw_complex*) gNoisex;
	kgNoisey = (fftw_complex*) gNoisey;

	/***************workspace array*************/
	work = (double*)malloc ( sizeof ( double ) * total_local_size );
}
void mpi()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);

	MPI_Barrier(MPI_COMM_WORLD);
}
void fftwMpi()
{
	//setting up mpi fftw transforms
	plan_F = rfftw2d_mpi_create_plan(MPI_COMM_WORLD,Ny, Nx,FFTW_REAL_TO_COMPLEX, FFTW_MEASURE);
	plan_B = rfftw2d_mpi_create_plan(MPI_COMM_WORLD,Ny, Nx,FFTW_COMPLEX_TO_REAL, FFTW_MEASURE);
	
	rfftwnd_mpi_local_sizes(plan_F, &local_nlast, &local_last_start,
							&local_nlast2_after_trans,&local_last2_start_after_trans,&total_local_size);
}	
void inputVariables()
{
	char *line = malloc(BUFSIZ);
	FILE *in;
	in = fopen("inputPureRadPaper","rt");	
   
	printf("reading input file\n");
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &Nx,&Ny);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&dx,&dt);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&endtime,&timefreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&no,&qo);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %d",&eps,&feps,&itereps);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&nAmp,&M);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d",&int_cond);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %d %d",&iRad_d,&gama_d,&radFlag,&radtime);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartflag,&restart_time);
	
	fclose(in);

	printf("done reading input file\n");
}
void removeDens()
{	
	double RadiusRemove = 220./2.;	//radius of circular portion we are removing
	int i,j,iy,index;

	for(i=0;i<local_nlast;i++)
	{
		iy = i + local_last_start;
		for(j=0;j<Nx;j++)
		{
			index = j+i*Nx;
			if( sqrt( (iy-Ny/2.)*(iy-Ny/2.) + (j-Nx/2.)*(j-Nx/2.) ) <= RadiusRemove ) //seed 4
			{
				//triangle
				n[index] = no;
			}			
		}
	}
}
void setRadVar()
{
	iRad = iRad_d;
	gama = gama_d;
}
int main()
{
	int iter;
	double deps;
		
	//read from file the input values
	inputVariables();

	deps = (eps - feps)/itereps;
	
	printf("number of grid points in x %d\n",Nx);
	printf("number of grid points in y %d\n",Ny);

	mpi();

	MPI_Barrier(MPI_COMM_WORLD);

	fftwMpi();

	MPI_Barrier(MPI_COMM_WORLD);

	allocateFft();

	MPI_Barrier(MPI_COMM_WORLD);

	srand48(time(NULL)+myid);	//seed for random number

	if (restartflag == 0)
	{
		initialize();

		printf("%s","output 1\n");
		output(0,n,"dens");
	}
	else
	{
		printf("%s","restart flag has been triggered\n");
		restart(restart_time,n,"dens");
	}
		
	printf("%s","begin time iteration\n");

	MPI_Barrier(MPI_COMM_WORLD);

	for(iter=restart_time+1;iter<endtime+restart_time;iter++)
	{
		if ( iter == radtime && radFlag == 0 )
			removeDens();
		else if ( iter == radtime && radFlag == 1 )
			setRadVar();

		Ln();
		MPI_Barrier(MPI_COMM_WORLD);
		fcalNL();
		MPI_Barrier(MPI_COMM_WORLD);
		calcNoise();
		MPI_Barrier(MPI_COMM_WORLD);
		fPsi();
		
		MPI_Barrier(MPI_COMM_WORLD);
		if((iter)%timefreq == 0)
		{
			if (myid==0)
				printf("%d\n",iter);
			output(iter,n,"dens");
		}

		//change temperature
		if (iter <= itereps)
			eps = eps - deps;

		MPI_Barrier(MPI_COMM_WORLD);
			
	}

	//destroy fftw plans
	rfftwnd_mpi_destroy_plan(plan_F);
	rfftwnd_mpi_destroy_plan(plan_B);

	//free memory
	free(n);
	free(nf);
	free(fNL);	
	free(gNoisex);
	free(gNoisey);

	//exit mpi environment
	MPI_Finalize();

	return 0;
}
