#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <complex.h>
#include <fftw3-mpi.h>

#define Pi (2.*acos(0.))

//domain parameters
char run[3];			//label of current run
double dx,dy;			//grid spacing
double spacing;			//normalized lattice spacing

double atomsx,atomsy;	//number of lattice units in x,y and z
ptrdiff_t Nx,Ny;		//number of grid points in x,y and z
ptrdiff_t Nx2,Nxn2;		//needed for padding real arrays for transform

//time parameters
int totalTime,printFreq;	//total iteration time and print frequency
int eneFreq;				//frequency for energy calculation
double dt;					//time step
int restartFlag;			//set to 1 for restart
int restartTime;			//restart time

//MPI parameters
int argc;
char **argv;
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

/**********************correlation variables****************************/
//k-zero mode
double H0, w0;
double PreC0;	//prefactors based on the widths of the correlation peaks

//first and second modes
double qC1,qC2;		//k-space peak positions for first and second mode for 
double w1,w2;		//widths of the first and second mode peaks )sets elasticity, anisotropy, interface widths etc)
double DW1,DW2;		//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreC1,PreC2;	//prefactors based on the widths of the correlation peaks

double rho1,rho2;	//planar atomic density
double beta1,beta2;	//planar symmetries

//*************initial condition and simulation variables
double iSig;			//initial temperature (note, can be expanded with an iteration count to do cooling)
double navg,nl,ns;		//average density and equilibrium liquid and solid density at iSig

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mn;			//mobility constant for density

double facx,facy,normFac;	//fourier factors for scaling lengths in k-space

//*******************Arrays for fields
//real space arrays
double *n;				//density
double *nfNL;			//non-linear terms for density
double *karr;			//holds k^2 array in k-space
double *kCorr;			//correlation fucntion

//complex arrays
fftw_complex *kn;			//k-space density
fftw_complex *knfNL;		//k-space non-linear terms

//fftw plans
fftw_plan planF, planB;			//forward and backward plans

void freeMemory()
{
	free(gPot);
	fftw_free(kgPot);

	free(kCorr);free(karr);

	free(n);free(nfNL);
	fftw_free(kn);fftw_free(knfNL);

	fftw_destroy_plan(planF);
	fftw_destroy_plan(planB);	
}
void restart(int time,double *Array,char *filepre,char *runtype)
{
	double *tmp;
	tmp = (double*) fftw_malloc(sizeof(double) * Ny*Nx );

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%s%d.dat",runtype,filepre,time);
    fp = fopen(filename,"rt");
	
	ptrdiff_t i,j;
	ptrdiff_t jg;	//global index for y direction due to mpi
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			fscanf(fp,"%lf",&tmp[i+Nx*j]);
		}
	}
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+Nx2*jg] = tmp[i+Nx*j];
		}
	}	
	free(tmp);
	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre,char *runtype)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%s%d.dat",runtype,filepre,time);

	ptrdiff_t i,j;
	int kk;
	
	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(j=0;j<local_n0;j++)
			{
				for(i=0;i<Nx;i++)
				{
					fprintf(fp,"%lf\n",Array[i+Nx2*j]);
				}
			}			
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void normalize(double *Array)
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;

	//normalize the transform
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;
			Array[index] *= normFac;
		}
	}
}
void energy(int time,char *runtype)
{
	ptrdiff_t i,j,k;
	ptrdiff_t index,index1;

	double ene,mu,avgdens,pr;
	double ened,mud,avgdensd,prd;
	int isource;
	MPI_Status status;

	char filename[BUFSIZ];
    FILE *fpene;
    
	if (myid==0)
    {
    	sprintf(filename,"%s.ene",runtype);
		fpene = fopen(filename,"a");
    }

	fftw_mpi_execute_dft_r2c(planF,n,kn);	//fourier transform dens
	MPI_Barrier(MPI_COMM_WORLD);

	//Correlation part of free energy
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nxn2;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1; 
			kn[index] = kCorr[index]*kn[index];
		
		}
	}
	fftw_mpi_execute_dft_c2r(planB,kn,nfNL);	//inverse transform correlative part
	normalize(nfNL);							//normalize 
	MPI_Barrier(MPI_COMM_WORLD);

	// Add bulk terms to correlation part of energy
    ene=0.;
    mu=0.;
    avgdens=0.;
    pr=0.;
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;
            ene = ene + .5*n[index]*n[index]*( 1. + n[index]*(-eta/3. + chi*n[index]/6.) ) - .5*n[index]*nfNL[index];
         
			mu = mu + n[index]*( 1. + n[index]*( -.5*eta + chi*n[index]/3. ) ) - nfNL[index];
            
			avgdens = avgdens + n[index];

			pr = pr + gPot[index];
		}
	}
            

	// Tabulate and write avg energy, chemical potential, and density
    if (myid == 0)
	{
		for (isource=1; isource<numprocs; isource++)
		{
            MPI_Recv(&ened,1,MPI_DOUBLE,isource,1,MPI_COMM_WORLD,&status);
            ene = ene+ened;
            MPI_Recv(&mud,1,MPI_DOUBLE,isource,2,MPI_COMM_WORLD,&status);
            mu = mu+mud;
            MPI_Recv(&avgdensd,1,MPI_DOUBLE,isource,3,MPI_COMM_WORLD,&status);
            avgdens = avgdens+avgdensd;
            MPI_Recv(&prd,1,MPI_DOUBLE,isource,4,MPI_COMM_WORLD,&status);
            pr = pr+prd;
		}
		ene = ene*normFac;
        mu  = mu*normFac;
        avgdens = avgdens*normFac;
        pr = pr*normFac;
		
		fprintf(fpene,"%9f %.17g %.17g %.17g %.17g \n",time*dt,ene,mu,avgdens,pr);
	    fclose(fpene);
	}
    else
	{
        MPI_Send(&ene,1,MPI_DOUBLE,0,1,MPI_COMM_WORLD);
        MPI_Send(&mu,1,MPI_DOUBLE,0,2,MPI_COMM_WORLD);
        MPI_Send(&avgdens,1,MPI_DOUBLE,0,3,MPI_COMM_WORLD);
        MPI_Send(&pr,1,MPI_DOUBLE,0,4,MPI_COMM_WORLD);
	}
	MPI_Barrier(MPI_COMM_WORLD);
}
void timeStep() 
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	ptrdiff_t yj;

	double prefactorn;

	double k2;
	
	/************** Step *************/	
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nxn2;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;

			k2 = karr[index];

			prefactorn = 1./( 1. + dt*Mn*k2*( 1. - kCorr[index] ) );

			kn[index] = prefactorn * ( kn[index] - dt*Mn*k2*knfNL[index] );
		}
	}
}
void calcNL()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;

	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;

			nfNL[index] = n[index]*n[index]*( -.5*eta + chi*n[index]/3. );			
		}
	}
}
void calcCorr()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	ptrdiff_t yj;

	//temporary variable for correlation peak heights
	double kC0,kC1,kC2;
	double C2;

	double rk,k2;

	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nxn2;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;

			k2 = karr[index];

			rk = sqrt(k2);

			//****************************C2**************************//
			kC0 = H0*exp( PreC0 * k2 );
			kC1 = DW1*exp( PreC1 * (rk-qC1)*(rk-qC1) );
			kC2 = DW2*exp( PreC2 * (rk-qC2)*(rk-qC2) );

			//creating the correlation overlap
			if ( kC0 > kC1 )
				C2 = -kC0;
			else if ( kC1 > kC2 )
				C2 = kC1;
			else
				C2 = kC2;

			kCorr[index] = C2;
		}
	}
}
void k2array()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	ptrdiff_t yj;
		
	double kx,ky;
	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky =yj*facy;
		else
			ky = (yj-Ny)*facy;

		index1 = j*Nxn2;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;

			kx = i*facx;
	
			karr[index] = ( 3. - cos(kx) - cos(ky) - cos(kx)*cos(ky) )/dx/dx;
		}
	}				
}
void setDebyeWaller(double Sig)
{
	DW1 = exp(-.5*Sig*Sig*qC1*qC1/(rho1*beta1) );
	DW2 = exp(-.5*Sig*Sig*qC2*qC2/(rho2*beta2) );
}
void setCorrPeakPrefactors()
{
	PreC0 = -.5/(w0*w0);
	PreC1 = -.5/(w1*w1);
	PreC2 = -.5/(w2*w2);
}
void initialize()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	ptrdiff_t yj;
	
	double ko=2.*Pi;
	
	double atri=.23;

	double asq1,asq2;
	asq1 = 0.25;
	asq2 = 0.13;	


	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;

			// *********set avg dens everywhere***********
			n[index] = navg;

			if ( yj < Ny/2+100 && yj > Ny/2-100 )
			{
				//triangle
				n[index] = ns + atri*2.*( cos(ko*yj*dx) + cos(ko*(-0.5*yj+0.5*sqrt(3.)*i)*dx) + cos(ko*(-0.5*yj-0.5*sqrt(3.)*i)*dx) );
			}
		}			
	}
}
void initialConditions()
{
	//determine if this is a restart or initialization
	if (restartFlag == 0)
	{
		initialize();
		//adjust_density();

		//output initial conditions
		//density
		if(myid==0) printf("output 0\n");
		output(0,n,"n",run);
		
		if(myid==0) printf("initializing domain complete\n");
	}
	else
	{
		if(myid==0) printf("restart flag has been triggered\n");
		if(myid==0) printf("restarting simulation from time step = %d\n",restartTime);
		
		restart(restartTime,n,"n",run);

		if(myid==0) printf("domain has been reinitialized with restart file\n");

	}
}
void setfftwPlans()
{
	/************** n **************/
	planF = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, n, kn, MPI_COMM_WORLD, FFTW_MEASURE);
	planB = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kn, n, MPI_COMM_WORLD, FFTW_MEASURE);
}
void allocateArrays()
{
	/****************** n ******************/
	n = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	nfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	knfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** C2 ******************/
	kCorr = (double *) fftw_malloc( sizeof (double)*alloc_local );

	karr = (double*) fftw_malloc( sizeof (double*) * alloc_local );
}
void fftwMPIsetup()
{
	//start fftw mpi routines
	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_2d(Ny, Nx/2+1, MPI_COMM_WORLD,&local_n0, &local_0_start);

	if(myid==0) printf(" %d local_n0=%zu local_0_start=%zu\n",myid,local_n0,local_0_start);
}
void domainParams()
{
	//from the lattice spacing and grid spacing, compute the number of grid points
	if(myid==0) printf("dx=%f  numAtomsx=%lf numAtomsy=%lf\n",dx, atomsx,atomsy);
	
	Nx = (int)(floor(spacing/dx*atomsx));
	Ny = (int)(floor(spacing/dy*atomsy));

	if(myid==0) printf("number of grid points in x %zu\n",Nx);
	if(myid==0) printf("number of grid points in y %zu\n",Ny);
	
	Nxn2 = (Nx/2+1);
	Nx2 = 2*Nxn2;

	facx = 2.*Pi/(Nx);
	facy = 2.*Pi/(Ny);
	normFac = 1.0/(Nx*Ny);
	
	if(myid==0) printf("facx=%lf\n",facx);
	if(myid==0) printf("facy=%lf\n",facy);
	if(myid==0) printf("normFac=%0.10lf\n",normFac);
}
void inputVariables()
{
	char *line = (char *) malloc (BUFSIZ * sizeof(char));
	FILE *in;
	in = fopen("inputSqTriPureMPI","rt");
   
	if(myid==0) printf("reading input file\n");
	if (in == NULL)
	{
		printf("Error opening input file\n");
		printf("Either file does not exit or incorrect file name\n");
		printf("Exiting simulation!\n");
		exit(1);	
	}	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%s", run);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf", &atomsx,&atomsy);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d", &eneFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&H0,&w0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&w1,&w2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&rho1,&rho2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&beta1,&beta2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf", &iSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&navg);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&nl,&ns);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&Mn);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);

	dy = dx;

	if(myid==0) 
	{
		printf("done reading input file\n");
		
		printf("run=%s\n",run);
		printf("atomsx=%lf	atomsy=%lf\n",atomsx,atomsy);
		printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
		printf("totalTime=%d printFreq=%d\n",totalTime,printFreq);
		printf("eneFreq=%d\n",eneFreq);
		printf("H0=%lf	w0=%lf\n",H0,w0);
		printf("w1=%lf	w2=%lf\n",w1,w2);
		printf("rho1=%lf	rho2=%lf\n",rho1,rho2);
		printf("beta1=%lf	beta2=%lf\n",beta1,beta2);
		printf("iSig=%lf\n",iSig);
		printf("navg=%lf\n",navg);
		printf("nl=%lf	ns=%lf\n",nl,ns);
		printf("eta=%lf	chi=%lf\n",eta,chi);
		printf("Mn=%lf\n",Mn);
		printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);
	}
}
void setPeakPositions()
{
	qC1 = 2.*Pi;
	qC2 = sqrt(2.)*qC1;
}
void start_mpi()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);
}
int main(int argc, char* argv[])
{
	int iter;	

	//initiate mpi daemons
	start_mpi();

	//set the corresponding peak positions
	setPeakPositions();

	MPI_Barrier(MPI_COMM_WORLD);

	//read in the parameter file
	inputVariables();

	MPI_Barrier(MPI_COMM_WORLD);

	//set up domain parameters
	domainParams();

	//setup the fftw mpi environment
	fftwMPIsetup();

	//allocate real and k-space arrays
	allocateArrays();
	if(myid==0) printf("arrays have been allocated\n");

	//set up fftw plans
	setfftwPlans();
	if(myid==0) printf("fftw plans have been created and set\n");

	//initialize random number generator
	srand48(time(NULL)+myid);
	
	MPI_Barrier(MPI_COMM_WORLD);

	//initial conditions
	initialConditions();

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();
	if(myid==0) printf("correlation peak prefactors calculated\n");
	
	//calculate the debye-waller portion of correlations
	setDebyeWaller(iSig);
	if(myid==0) printf("debye-waller factors calculated\n");

	//calculate the k2, i.e., laplacian, array in k-space
	k2array();
	if(myid==0) printf("k2 array calculated\n");

	//calculate the initial correlation kernerl
	calcCorr();
	if(myid==0) printf("initial C2 kernel calculated\n");
	

	fftw_mpi_execute_dft_r2c(planF,n,kn);		//fourier transform density
	if(myid==0) printf("first transform complete\n");
	calcPressure();

	//********************************************************************************************************************
	if(myid==0) printf("beginning iterations\n");
	for(iter=restartTime+1;iter<totalTime;iter++)
	{
		/****** non-linear *******/
		calcNL();						//caluclate non-linear density terms

		fftw_mpi_execute_dft_r2c(planF,n,kn);		//fourier transform density
		fftw_mpi_execute_dft_r2c(planF,nfNL,knfNL);	//fourier transform non-linear
	
		/****** time step fields *******/
		timeStep();

		/****** inverse transform and normalize field arrays *******/
		fftw_mpi_execute_dft_c2r(planB,kn,n);		//inverse transform density
		normalize(n);								//normalize 
				
		MPI_Barrier(MPI_COMM_WORLD);

		if( iter%printFreq == 0 )
		{
			if(myid==0) printf("time = %d\n",iter);
			output(iter,n,"n",run);
		}

		MPI_Barrier(MPI_COMM_WORLD);		
		
		if( (iter)%eneFreq == 0 )
			energy(iter,run);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	
	//free memory and destroy fftw plans
	freeMemory();

	//exit mpi
	MPI_Finalize();

	return 0;
}