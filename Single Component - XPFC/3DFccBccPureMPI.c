#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <complex.h>
#include <fftw3-mpi.h>

#define Pi (2.*acos(0.))

//domain parameters
double dx,dy,dz;			//grid spacing
double spacing;				//normalized lattice spacing

double atomsx,atomsy,atomsz;	//number of atoms in x,y and z
ptrdiff_t Nx,Ny,Nz;				//number of grid points in x,y and z
ptrdiff_t Nx2,Nxn2;					//needed for padding real arrays for transform

//time parameters
int totalTime,printFreq;	//total iteration time and print frequency
int eneFreq,peakFreq;		//frequency for energy and peak
double dt;					//time step
int restartFlag;			//set to 1 for restart
int restartTime;			//restart time

//MPI parameters
int argc;
char **argv;
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

/**************** correlation variables ****************/
//k-zero mode
double qC0, w0;			//height of k-zero mode, width of k-zero mode
double PreC0;			//prefactor based on the widths of the k-zero mode

//first and second modes
double qC1,qC2;			//equilibrium peak positions for first and second mode
double w1,w2;			//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double rho1,rho2;		//atomic density of respective planes
double beta1,beta2;		//planar symmetry of respective planes

double DW1,DW2;			//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreC1,PreC2;		//prefactors calculated with the widths of the correlation peaks

/************* initial condition and simulation variables *************/
double iSig;		//initial temperature - related to vibrational amplitude of atom
double navg;		//average density

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mn;	//mobility constant for density

double facx,facy,facz,normFac;	//fourier factors for scaling lengths in k-space

/*********************** density ****************************/
double *n;		//density
double *nfNL;	//holds collection of non-linear terms 

fftw_complex *kn;		//complex density after transform
fftw_complex *knfNL;	//complex non-linear density terms after transform

/*********************** correlation ****************************/
double *kCAA;			//holds C2 array in k-space
double *karr;			//holds k2 array in k-sapce

fftw_plan planB, planF;	//backward and forward plans


void freeMemory()
{
	/*********************** destroy fftw plans **************************/
	fftw_destroy_plan(planB);
	fftw_destroy_plan(planF);

	/*********************** free memory **************************/
	free(n);	free(nfNL);
		
	fftw_free(kn);	fftw_free(knfNL);	
}
void restart(int time,double *Array,char *filepre)
{
	double *tmp;
	tmp = (double*) fftw_malloc(sizeof(double) * Nz*Ny*Nx );

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    fp = fopen(filename,"rt");
	
	ptrdiff_t i,j,k;
	ptrdiff_t kg;	//global index for z direction due to mpi
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	for(k=0;k<Nz;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				fscanf(fp,"%lf",&tmp[i+Nx*(j+k*Ny)]);
			}
		}
	}
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(k=0;k<Nz;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				kg = k - myid*local_n0;	//rescale coordinates to that of local processor
				if (kg >= 0 && kg <= local_n0-1)
					Array[i+Nx2*(j+kg*Ny)] = tmp[i+Nx*(j+k*Ny)];
			}
		}
	}
	
	free(tmp);
	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);

	ptrdiff_t i,j,k;
	int kk;
	
	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(k=0;k<local_n0;k++)
			{
				for(j=0;j<Ny;j++)
				{
					for(i=0;i<Nx;i++)
					{
						fprintf(fp,"%lf\n",Array[i+Nx2*(j+k*Ny)]);
					}
				}
			}
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void outputSlice(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);

	ptrdiff_t i,j,k;
	int kk;

	MPI_Barrier(MPI_COMM_WORLD);

	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to open file for appending\n");
					exit(1);
				}
			}
   
			i=Nx/2;
			for(k=0;k<local_n0;k++)
				for(j=0;j<Ny;j++)
					fprintf(fp,"%f  \n",Array[i+Nx2*(j+k*Ny)]);
					
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void normalize(double *Array)
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	
	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i+Nx2*(j+k*Ny);
				Array[index] *= normFac;
			}
		}
	}
}
void timeStep()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;

	double prefactorn;
	
	double k2;

	/************** Step *************/	
	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nxn2;i++)
			{
				index = i+Nxn2*(j+k*Ny);
				
				k2 = karr[index];
				
				prefactorn = 1.0/( 1.0 + dt*Mn*k2*( 1. - kCAA[index] ) );
			
				kn[index] = prefactorn*( kn[index] - dt*Mn*k2*knfNL[index] );
			}
		}
	}
}
void calcNL()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;

	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i+Nx2*(j+k*Ny);

				nfNL[index] = n[index]*n[index]*( -0.5*eta + chi*n[index]/3. );
			}
		}
	}
}
void calcCorr()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;	

	//temporary variables for correlation peak heights
	double kCA1,kCA2;
	double C2A;

	double rk,k2;
	
	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nxn2;i++)
			{
				index = i+Nxn2*(j+k*Ny);
				
				k2 = karr[index];

				rk = sqrt(k2);

				//**************************** C2A **************************
				kCA1 = DW1*exp( PreC1 * (rk-qC1)*(rk-qC1) );
				kCA2 = DW2*exp( PreC2 * (rk-qC2)*(rk-qC2) );

				//creating correlation overlap
				if( kCA1 > kCA2 )
					C2A = kCA1;
				else
					C2A = kCA2;

				kCAA[index] = C2A;
			}
		}
	}
}
void k2array()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;
		
	double kx,ky,kz;
	
	for(k=0;k<local_n0;k++)
	{
		zk = k + local_0_start;

		if ( zk < Nz/2 )
			kz = zk*facz;
		else
			kz = (zk-Nz)*facz;

		for(j=0;j<Ny;j++)
		{
			if ( j < Ny/2 )
				ky = j*facy;
			else
				ky = (j-Ny)*facy;

			for(i=0;i<Nxn2;i++)
			{
				index = i+Nxn2*(j+k*Ny);

				kx = i*facx;

				karr[index] = kx*kx + ky*ky + kz*kz;
			}
		}
	}				
}
void setDebyeWaller(double Sig)
{
	DW1 = exp(-0.5*Sig*Sig*qC1*qC1/(rho1*beta1) );
	DW2 = exp(-0.5*Sig*Sig*qC2*qC2/(rho2*beta2) );
}
void setCorrPeakPrefactors()
{
	PreC0 = -0.5/(w0*w0);
	PreC1 = -0.5/(w1*w1);
	PreC2 = -0.5/(w2*w2);
}
void initialize()
{	
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;
		
	double ko = 2.*Pi;

	double afcc1 = .14;
	double afcc2 = .1;
	
	double fs = 20*Nx*Ny*normFac;

	//***************begin initial conditions******************
	for(k=0;k<local_n0;k++)
	{
		zk = k + local_0_start;

		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i+Nx2*(j+k*Ny);

				//initially mean density
				n[index] = -.172975;			

				if( zk < Nz/2+10 && zk > Nz/2-10 )
				{
					//density
					n[index] = -.02226 + afcc1*(2.*cos(ko*(-i+j+zk)*dx) + 2.*cos(ko*(i-j+zk)*dx) + 2.*cos(ko*(i+j-zk)*dx) + 2.*cos(ko*(-i-j-zk)*dx) ) //first mode
							  + afcc2*( 2.*cos(2.*ko*i*dx) + 2.*cos(2.*ko*j*dx) + 2.*cos(2.*ko*zk*dx) );
				}
				else
				{
					//avgerage density in liquid according to the lever rule
					//n[index] = ( navg - (-.02226*fs) )/( 1. - fs );
				}
			}
		}		
	}
}
void initialConditions()
{
	if (restartFlag == 0)
	{
		initialize();

		//output initial conditions
		if(myid==0) printf("output 0\n");
		output(0,n,"dens");

		outputSlice(0,n,"nSlice");
		
		if(myid==0) printf("initializing domain complete\n");
	}
	else
	{
		if(myid==0) printf("restart flag has been triggered\n");
		if(myid==0) printf("restarting simulation from time step = %d\n",restartTime);

		//read in restart file
		restart(restartTime,n,"dens");		
		if(myid==0) printf("domain has been reinitialized with restart file\n");
	}
}
void setfftwPlans()
{
	//setting up fftw transforms	
	planF = fftw_mpi_plan_dft_r2c_3d(Nz, Ny, Nx, n, kn, MPI_COMM_WORLD, FFTW_MEASURE);
	planB = fftw_mpi_plan_dft_c2r_3d(Nz, Ny, Nx, kn, n, MPI_COMM_WORLD, FFTW_MEASURE);
}
void allocateArrays()
{
	/***************** n ************************/
	n = (double*) fftw_malloc( sizeof (double*) * 2*alloc_local );
	nfNL = (double*) fftw_malloc( sizeof (double*) * 2*alloc_local );

	kn = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );
	knfNL = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * alloc_local );

	/***************** C2 ************************/
	kCAA = (double*) fftw_malloc( sizeof (double*) * alloc_local );
	karr = (double*) fftw_malloc( sizeof (double*) * alloc_local );
}
void fftwMPIsetup()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	if(myid == 0) printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);

	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_3d(Nz, Ny, Nx/2+1, MPI_COMM_WORLD, &local_n0, &local_0_start);
	
	if(myid == 0) printf(" %d local_n0=%zu local_0_start=%zu\n",myid,local_n0,local_0_start);
}
void domainParams()
{
	//set up domain parameters
	if(myid == 0) printf("dx=%f  numAtomsx=%lf numAtomsy=%lf numAtomsz=%lf\n",dx, atomsx,atomsy,atomsz);

	Nx = (int)( floor(atomsx*spacing/dx) );
	Ny = (int)( floor(atomsy*spacing/dy) );
	Nz = (int)( floor(atomsz*spacing/dz) );

	if(myid==0) printf("Nx = %zu\n",Nx);
	if(myid==0) printf("Ny = %zu\n",Ny);
	if(myid==0) printf("Nz = %zu\n",Nz);
	
	Nxn2 = (Nx/2+1);
	Nx2 = 2*Nxn2;

	//set up fourier scaling factors
	facx = 2.*Pi/(Nx*dx);
	facy = 2.*Pi/(Ny*dy);
	facz = 2.*Pi/(Nz*dz);
	normFac = 1.0/(Nx*Ny*Nz);
	
	if(myid==0) printf("facx=%lf\n",facx);
	if(myid==0) printf("facy=%lf\n",facy);
	if(myid==0) printf("facz=%lf\n",facz);
	if(myid==0) printf("normFac=%0.10lf\n",normFac);
}
void inputVariables()
{
	char *line = (char *) malloc (BUFSIZ * sizeof(char));
	FILE *in;
	in = fopen("input3DPureMPI.in","rt");
   
	if(myid == 0) printf("reading input file\n");
	if (in == NULL)
	{
		if(myid == 0) printf("Error opening input file\n");
		if(myid == 0) printf("Either file does not exit or incorrect file name\n");
		if(myid == 0) printf("Exiting simulation!\n");
		exit(1);	
	}	

	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf", &atomsx,&atomsy,&atomsz);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &eneFreq,&peakFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&qC0,&w0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&w1,&w2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&rho1,&rho2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&beta1,&beta2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf", &iSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&navg);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&Mn);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);

	dy = dx;
	dz = dx;

	if(myid==0) 
	{
		printf("done reading input file\n");
	
		printf("atomsx=%lf	atomsy=%lf	atomsz=%lf\n",atomsx,atomsy,atomsz);
		printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
		printf("totalTime=%d	printFreq=%d\n",totalTime,printFreq);
		printf("eneFreq=%d	peakFreq=%d\n",eneFreq,peakFreq);
		printf("qC0=%lf	w0=%lf\n",qC0,w0);
		printf("w1=%lf	w2=%lf\n",w1,w2);
		printf("rho1=%lf	rho2=%lf\n",rho1,rho2);
		printf("beta1=%lf	beta2=%lf\n",beta1,beta2);
		printf("iSig=%lf\n",iSig);
		printf("navg=%lf\n",navg);
		printf("eta=%lf	chi=%lf\n",eta,chi);
		printf("Mn=%lf\n",Mn);
		printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);
	}

}
void setPeakPosition()
{
	qC1 = sqrt(3.)*2.*Pi;	//first mode
	qC2 = 4.*Pi;			//second mode
}
int main()
{
	int iter;
	
	//set up the equil'm position for first and second peaks
	setPeakPosition();

	//read from file the input values
	inputVariables();

	//set up domain parameters
	domainParams();
	
	//setup the fftw mpi environment
	fftwMPIsetup();

	//allocating relevant arays
	allocateArrays();
	if(myid==0) printf("arrays have been allocated\n");

	//setting up fft transform plans
	setfftwPlans();
	if(myid==0) printf("fftw plans have been created and set\n");

	//initialize random number generator
	srand48(time(NULL));

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();
	if(myid==0) printf("correlation peak prefactors calculated\n");
	
	//calculate the debye-waller portion of correlations
	setDebyeWaller(iSig);
	if(myid==0) printf("debye-waller factors calculated\n");

	//calculate the k2, i.e., laplacian array, in k-space
	k2array();

	//calculate the initial correlation kernerl
	calcCorr();

	//initial conditions
	initialConditions();	
	
	/*********************************************************************************************************/
	//begin interations
	if(myid==0) printf("begin time iteration\n");	
	for(iter=restartTime+1; iter<totalTime+restartTime+1; iter++)
	{
		/****************** density ******************/
		calcNL();					//calculate non-linear terms for density

		fftw_mpi_execute_dft_r2c(planF,n,kn);		//fourier transform density
		fftw_mpi_execute_dft_r2c(planF,nfNL,knfNL);	//foruier transform average density non-linear terms
		
		timeStep();									//update density in k-space
		fftw_mpi_execute_dft_c2r(planB,kn,n);		//inverse transform density
		normalize(n);								//normalize average density after	transform

		//output current data
		if( (iter)%printFreq == 0 )
		{
			if(myid==0) printf("time = %d\n",iter);
			
			//output average density
			//output(iter,n,"dens");
			outputSlice(iter,n,"nSlice");
		}

		if( (iter)%peakFreq == 0 )
		{
			//output average density
			output(iter,n,"dens");
		}
	}
	
	//free meory and destroy fftw plans
	freeMemory();

	//exit mpi
	MPI_Finalize();

	return 0;
}
