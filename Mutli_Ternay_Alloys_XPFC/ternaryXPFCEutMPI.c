#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <complex.h>
#include <fftw3-mpi.h>

#define Pi (2.*acos(0.))

//domain parameters
int atomsx,atomsy;			//number of atoms in x and y
ptrdiff_t Nx,Ny;			//number of grid points in x and y
ptrdiff_t Nx2;
double spacing;				//lattice spacing normalized to 1
double dx;					//numerical grid spacing

//time parameters
int totalTime, printFreq;		//simulation time and print frequency
double dt;						//numerical time step
int restartFlag,restartTime;	//flag for restarting (set to 1) and time

//MPI parameters
int argc;
char **argv;
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

/**********************correlation variables for A****************************/
//k-zero mode
double HA0;	//height/depth of k-zero mode A
double wA0;	//width of k-zero mode A
double PreCA0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kA1,kA2;	//k-space peak positions for first and second mode for A
double wA1,wA2;	//widths of the first and second mode peaks )sets elasticity, anisotropy, interface widths etc)
double DWA1,DWA2;	//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCA1,PreCA2;	//prefactors based on the widths of the correlation peaks

double sigMA1,sigMA2;	//sets the effective transition temperatures for the corresponding mode of the correlation

/**********************correlation variables for B***************************/
//k-zero mode
double HB0;	//height/depth of k-zero mode B
double wB0;	//width of k-zero mode B
double PreCB0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kB1,kB2;		//k-space peak positions for first and second mode for B
double wB1,wB2;		//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double DWB1,DWB2;		//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCB1,PreCB2;	//prefactors based on the widths of the correlation peaks

double sigMB1,sigMB2;	//sets the effective melting points for the corresponding mode of the correlation

/**********************correlation variables for C***************************/
//k-zero mode
double HC0;	//height/depth of k-zero mode C
double wC0;	//width of k-zero mode C
double PreCC0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kC1,kC2;		//k-space peak positions for first and second mode for C
double wC1,wC2;		//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double DWC1,DWC2;		//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCC1,PreCC2;	//prefactors based on the widths of the correlation peaks

double sigMC1,sigMC2;	//sets the effective melting points for the corresponding mode of the correlation

//*************initial condition and simulation variables
double iSig;			//initial temperature (note, can be expanded with an iteration count to do cooling)
double cavgA,cavgB;		//average concentration of A and B respectively

double csA_sqA, csA_sqB, csA_sqC;	//equilibrium compositions of A in each of the structures
double csB_sqA, csB_sqB, csB_sqC;	//equilibrium compositions of B in each of the structures

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mn;			//mobility constant for density

double coA,coB;			//reference liquid compositions
double Omega;			//entropy of mixing coefficient
double alphaA,alphaB;	//gradient energy coefficients 
double McA,McB;			//mobilities for concentration

double facx,facy;	//fourier factors for scaling lengths in k-space

//*******************Arrays for fields
//real space arrays
double *n,*cA,*cB;				//density and concentrations
double *nfNL,*cAfNL,*cBfNL;		//non-linear terms for density and concentrations
double *CAn,*CBn,*CCn;			//real space arrays for correlations convoluted with n
double *Ceffn,*CeffcA,*CeffcB;	//effective correlations after interpolation with concentration

//complex arrays
fftw_complex *kn,*kcA,*kcB;				//k-space density and concentrations
fftw_complex *knfNL,*kcAfNL,*kcBfNL;	//k-space non-linear terms
fftw_complex *kCAn,*kCBn,*kCCn;			//k-space convolution of correlation and density
fftw_complex *kCeffn,*kCeffcA,*kCeffcB;	//k-space effective correlations

//fftw plans
fftw_plan planF_n, planB_n;			//forward and backward plan for density
fftw_plan planF_cA, planB_cA;		//forward and backward plan for concentration A
fftw_plan planF_cB, planB_cB;		//forward and backward plan for concentration B
fftw_plan planF_NL_n;				//non-linear forward transform density
fftw_plan planF_NL_cA,planF_NL_cB;	//non-linear forward transforms concentration
fftw_plan planB_CAn,planB_CBn,planB_CCn;	//backward transforms for convoluted correlation C2n
fftw_plan planF_Ceffn;					//forward transforms for effective correlation density
fftw_plan planF_CeffcA,planF_CeffcB;	//forward transforms for effective correlations concentration

void freeMemory()
{	
	free(n);free(cA);free(cB);
	free(nfNL);free(cAfNL);free(cBfNL);
	free(CAn);free(CBn);free(CCn);
	free(Ceffn);free(CeffcA);free(CeffcB);

	fftw_free(kn);fftw_free(kcA);fftw_free(kcB);
	fftw_free(knfNL);fftw_free(kcAfNL);fftw_free(kcBfNL);
	fftw_free(kCAn);fftw_free(kCBn);fftw_free(kCCn);
	fftw_free(kCeffn);fftw_free(kCeffcA);fftw_free(kCeffcB);

	fftw_destroy_plan(planF_n);fftw_destroy_plan(planB_n);
	fftw_destroy_plan(planF_cA);fftw_destroy_plan(planB_cA);
	fftw_destroy_plan(planF_cB);fftw_destroy_plan(planB_cB);

	fftw_destroy_plan(planF_NL_n);fftw_destroy_plan(planF_NL_cA);fftw_destroy_plan(planF_NL_cB);
	fftw_destroy_plan(planB_CAn);fftw_destroy_plan(planB_CBn);fftw_destroy_plan(planB_CCn);
	fftw_destroy_plan(planF_Ceffn);fftw_destroy_plan(planF_CeffcA);fftw_destroy_plan(planF_CeffcB);
}
void restart(int time,double *Array,char *filepre)
{
	double *tmp;
	tmp = (double*) fftw_malloc(sizeof(double) * Ny*Nx );

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    fp = fopen(filename,"rt");
	
	ptrdiff_t i,j;
	ptrdiff_t jg;	//global index for y direction due to mpi
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			fscanf(fp,"%lf",&tmp[i+Nx*j]);
		}
	}
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+Nx2*jg] = tmp[i+Nx*j];
		}
	}	
	free(tmp);
	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);

	ptrdiff_t i,j;
	int kk;
	
	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(j=0;j<local_n0;j++)
			{
				for(i=0;i<Nx;i++)
				{
					fprintf(fp,"%lf\n",Array[i+Nx2*j]);
				}
			}			
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void normalize(double *Array)
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	//normalize the transform
	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;
			Array[index] /= (Nx*Ny);
		}
	}
}
void timeStepcB()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;

	double kx,ky,k2;
	double prefactor_cB;

	//************************time step for concentration B************************************//
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
			kx = i*facx;
			k2 = kx*kx + ky*ky ;
				
			prefactor_cB = 1./( 1. + dt*McB*k2*k2*alphaB );

			kcB[index] = prefactor_cB * ( kcB[index] - dt*McB*k2*( kcBfNL[index] - kCeffcB[index] ) );
		}
	}
}
void calcCeffcB()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	double c2A;
	double c2B;
	double cAB;

	double mix;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;

			c2A = cA[index]*cA[index];
			c2B = cB[index]*cB[index];
			cAB = cA[index]*cB[index];

			if( cB[index] > 1E-8 &&  (1.-cA[index]-cB[index]) > 1E-8 )
			{
				mix = Omega*( n[index] + 1. ) * ( log(cB[index]/coB) - log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else if( cB[index] > 1E-8 &&  (1.-cA[index]-cB[index]) < 1E-8 )
			{
				mix = Omega*( n[index] + 1. ) * ( log(cB[index]/coB) );
			}
			else if( cB[index] < 1E-8 &&  (1.-cA[index]-cB[index]) > 1E-8 )
			{
				mix = Omega*( n[index] + 1. ) * ( - log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else 
			{
				mix = 0.;
			}
			
			cBfNL[index] = mix;
			//cBfNL[index] = Omega*( n[index] + 1. ) * ( log(cB[index]/coB) - log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
				
			CeffcB[index] = 0.5*n[index] * ( 2.*cA[index] - 2.*c2A - 4.*cAB ) * CAn[index]
						  + 0.5*n[index] * ( 2.*cA[index] + 6.*cB[index] - 2.*c2A - 4.*cAB - 6.*c2B ) * CBn[index]
						  + 0.5*n[index] * ( -6.*cB[index] + 6.*c2B - 4.*cA[index] + 4.*c2A + 8.*cAB ) * CCn[index] ; 
					
		}
	}	
}
void timeStepcA()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;

	double kx,ky,k2;
	double prefactor_cA;

	//************************time step for concentration A************************************//
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
			kx = i*facx;
			k2 = kx*kx + ky*ky ;
				
			prefactor_cA = 1./( 1. + dt*McA*k2*k2*alphaA );

			kcA[index] = prefactor_cA * ( kcA[index] - dt*McA*k2*( kcAfNL[index] - kCeffcA[index] ) );
		}
	}
}
void calcCeffcA()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	double c2A;
	double c2B;
	double cAB;

	double mix;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;

			c2A = cA[index]*cA[index];
			c2B = cB[index]*cB[index];
			cAB = cA[index]*cB[index];

			if( cA[index] > 1E-8 && (1.-cA[index]-cB[index]) > 1E-8  )
			{
				mix = Omega*( n[index] + 1. ) * ( log(cA[index]/coA) - log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else if( cA[index] > 1E-8 && (1.-cA[index]-cB[index]) < 1E-8  )
			{
				mix = Omega*( n[index] + 1. ) * ( log(cA[index]/coA) );
			}
			else if( cA[index] < 1E-8 && (1.-cA[index]-cB[index]) > 1E-8  )
			{
				mix = Omega*( n[index] + 1. ) * ( - log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else 
			{
				mix = 0.;
			}

			cAfNL[index] = mix;
			//cAfNL[index] = Omega*( n[index] + 1. ) * ( log(cA[index]/coA) - log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
				
			CeffcA[index] = 0.5*n[index] * ( 6.*cA[index] + 2.*cB[index] - 6.*c2A - 4.*cAB - 2.*c2B ) * CAn[index]
						  + 0.5*n[index] * ( 2.*cB[index] - 4.*cAB - 2.*c2B ) * CBn[index]
						  + 0.5*n[index] * ( -6.*cA[index] + 6.*c2A - 4.*cB[index] + 8.*cAB + 4.*c2B ) * CCn[index] ; 
					
		}
	}	
}
void timeStepn()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;

	double kx,ky,k2;
	double prefactor_n;

	/*****************************time step density**************************************/
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
			kx = i*facx;
			k2 = kx*kx + ky*ky ;

			prefactor_n = 1./( 1. + dt*Mn*k2 );

			kn[index] = prefactor_n * ( kn[index] - dt*Mn*k2*( knfNL[index] - kCeffn[index] ) );
		}
	}
}
void calcCeffn()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	double c2A,c3A;
	double c2B,c3B;
	double cAB;
	double mix;

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;

			c2A = cA[index]*cA[index];			
			c3A = cA[index]*c2A;
			c2B = cB[index]*cB[index];
			c3B = cB[index]*c2B;
			cAB = cA[index]*cB[index];

			if( cA[index] > 1E-8 && cB[index] > 1E-8 &&  (1.-cA[index]-cB[index]) > 1E-8 )
			{
				mix = Omega* ( cA[index] * log( cA[index] / coA ) + cB[index] * log( cB[index] / coB ) 
									+ (1.-cA[index]-cB[index]) * log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else if( cA[index] > 1E-8 && cB[index] > 1E-8 &&  (1.-cA[index]-cB[index]) < 1E-8 )
			{
				mix = Omega* ( cA[index] * log( cA[index] / coA ) + cB[index] * log( cB[index] / coB ) );
			}
			else if( cA[index] > 1E-8 && cB[index] < 1E-8 &&  (1.-cA[index]-cB[index]) > 1E-8 )
			{
				mix = Omega* ( cA[index] * log( cA[index] / coA ) + (1.-cA[index]-cB[index]) * log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else if( cA[index] < 1E-8 && cB[index] > 1E-8 &&  (1.-cA[index]-cB[index]) > 1E-8 )
			{
				mix = Omega* ( cB[index] * log( cB[index] / coB ) + (1.-cA[index]-cB[index]) * log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else if( cA[index] > 1E-8 && cB[index] < 1E-8 &&  (1.-cA[index]-cB[index]) < 1E-8 )
			{
				mix = Omega* ( cA[index] * log( cA[index] / coA ) );
			}
			else if( cA[index] < 1E-8 && cB[index] > 1E-8 &&  (1.-cA[index]-cB[index]) < 1E-8 )
			{
				mix = Omega* ( cB[index] * log( cB[index] / coB ) );
			}
			else if( cA[index] < 1E-8 && cB[index] < 1E-8 &&  (1.-cA[index]-cB[index]) > 1E-8 )
			{
				mix = Omega* ( (1.-cA[index]-cB[index]) * log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );
			}
			else
			{
				mix = 0.;
			}

			nfNL[index] = n[index]*n[index]*( -0.5*eta + chi*n[index]/3. ) + mix;

			/*nfNL[index] = n[index]*n[index]*( -0.5*eta + chi*n[index]/3. ) 
				        + Omega* ( cA[index] * log( cA[index] / coA ) + cB[index] * log( cB[index] / coB ) 
									+ (1.-cA[index]-cB[index]) * log( (1.-cA[index]-cB[index]) / (1.-coA-coB) ) );*/
				
			Ceffn[index] = ( 3.*c2A + 2.*cAB - 2.*c3A - 2.*c2A*cB[index] - 2.*cA[index]*c2B ) * CAn[index]
						 + ( 2.*cAB + 3.*c2B - 2.*c2A*cB[index] - 2.*cA[index]*c2B - 2.*c3B ) * CBn[index]
						 + ( 1. - 3.*c2A + 2.*c3A - 3.*c2B + 2.*c3B - 4.*cAB + 4.*c2A*cB[index] + 4.*cA[index]*c2B ) * CCn[index] ;
					
		}
	}
}
void calcCorrelations()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;

	double kx,ky,k2,rk;

	//temporary variable for the peak positions
	double kCA0,kCB0,kCC0;
	double kCA1,kCB1,kCC1;
	double kCA2,kCB2,kCC2;
	double kCA,kCB,kCC;

	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
				
			kx = i*facx;
			k2 = kx*kx + ky*ky ;
			rk = sqrt(k2);

			//****************************CA**************************//
			kCA0 = exp(PreCA0*k2);
			kCA1 = exp(PreCA1* (rk-kA1)*(rk-kA1) );
			kCA2 = exp(PreCA2* (rk-kA2)*(rk-kA2) );				

			//creating the correlation overlap
			if (HA0*kCA0 > DWA1*kCA1)
				kCA = -HA0*kCA0;
			else if (DWA1*kCA1 > DWA2*kCA2)
				kCA = DWA1*kCA1;
			else
				kCA = DWA2*kCA2;

			kCAn[index] = kn[index]*kCA;

			//****************************CB**************************//
			kCB0 = exp(PreCB0*k2);
			kCB1 = exp(PreCB1* (rk-kB1)*(rk-kB1) );
			kCB2 = exp(PreCB2* (rk-kB2)*(rk-kB2) );

			//creating the correlation overlap
			if (HB0*kCB0 > DWB1*kCB1)
				kCB = -HB0*kCB0;
			else if (DWB1*kCB1 > DWB2*kCB2)
				kCB = DWB1*kCB1;
			else
				kCB = DWB2*kCB2;

			kCBn[index] = kn[index]*kCB;

			//****************************CC**************************//
			kCC0 = exp(PreCC0*k2);
			kCC1 = exp(PreCC1* (rk-kC1)*(rk-kC1) );
			kCC2 = exp(PreCC2* (rk-kC2)*(rk-kC2) );

			//creating the correlation overlap
			if (HC0*kCC0 > DWC1*kCC1)
				kCC = -HC0*kCC0;
			else if (DWC1*kCC1 > DWC2*kCC2)
				kCC = DWC1*kCC1;
			else
				kCC = DWC2*kCC2;

			kCCn[index] = kn[index]*kCC;
		}
	}	
}
void setDebyeWaller(double Sig)
{
	DWA1 = exp(-Sig*Sig/(sigMA1*sigMA1) );
	DWA2 = exp(-Sig*Sig/(sigMA2*sigMA2) );
	
	DWB1 = exp(-Sig*Sig/(sigMB1*sigMB1) );
	DWB2 = exp(-Sig*Sig/(sigMB2*sigMB2) );

	DWC1 = exp(-Sig*Sig/(sigMC1*sigMC1) );
	DWC2 = exp(-Sig*Sig/(sigMC2*sigMC2) );
}
void setCorrPeakPrefactors()
{
	PreCA0 = -0.5/(wA0*wA0);
	PreCA1 = -0.5/(wA1*wA1);
	PreCA2 = -0.5/(wA2*wA2);

	PreCB0 = -0.5/(wB0*wB0);
	PreCB1 = -0.5/(wB1*wB1);
	PreCB2 = -0.5/(wB2*wB2);

	PreCC0 = -0.5/(wC0*wC0);
	PreCC1 = -0.5/(wC1*wC1);
	PreCC2 = -0.5/(wC2*wC2);
}
void initialize()
{
	ptrdiff_t i,j;
	ptrdiff_t index;
	ptrdiff_t yj;
	
	double asq1,asq2;
	asq1 = 0.22;
	asq2 = 0.11;

	double fs_sqA = (400*Ny/2.) / (Nx*Ny);
	//double fs_sqB = (400*Ny/3.) / (Nx*Ny);
	double fs_sqC = (Pi*40*40)/(Nx*Ny); //(400*Ny/2.) / (Nx*Ny);

	double x,y;
	double Angle_sqA = 0.*Pi/180.;
	double Angle_sqB = 0.*Pi/180.;
	double Angle_sqC = 0.*Pi/180.;
	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;

			//initially liquid with average concentration
			n[index] = 0;
			cA[index] = cavgA;
			cB[index] = cavgB;

			if ( sqrt( (i-Nx/2)*(i-Nx/2) + (yj-Ny/2)*(yj-Ny/2) ) < 40. )
			{
				x = i*cos(Angle_sqC) - yj*sin(Angle_sqC);
				y = i*sin(Angle_sqC) + yj*cos(Angle_sqC);

				//square C -  gamma phase
				n[index] = asq1*( 2.*cos(kC1*y*dx) + 2.*cos(kC1*x*dx) )	//first mode
							+ asq2*( cos(kC1*(x+y)*dx) + cos(kC1*(x-y)*dx) + cos(kC1*(-x+y)*dx) + cos(kC1*(-x-y)*dx) );	//second mode
					
				cA[index] = csA_sqC;
				cB[index] = csB_sqC;
			}
			else
			{
				cA[index] = ( cavgA - ( csA_sqC*fs_sqC ) ) / ( 1. - ( fs_sqC ) );
				cB[index] = ( cavgB - ( csB_sqC*fs_sqC ) ) / ( 1. - ( fs_sqC ) );
			}
		}
	}

	/*//testing planar growth
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;

			//initially liquid with average concentration
			n[index] = 0;
			cA[index] = cavgA;
			cB[index] = cavgB;

			if ( yj < Ny/2+40 && yj > Ny/2-40 )
			{
				x = i*cos(0.) - yj*sin(0.);
				y = i*sin(0.) + yj*cos(0.);

				//square A - alpha phase
				n[index] = asq1*( 2.*cos(kA1*y*dx) + 2.*cos(kA1*x*dx) )	//first mode
						 + asq2*( cos(kA1*(x+y)*dx) + cos(kA1*(x-y)*dx) + cos(kA1*(-x+y)*dx) + cos(kA1*(-x-y)*dx) );	//second mode
				
				x = i*cos(0.) - yj*sin(0.);
				y = i*sin(0.) + yj*cos(0.);

				//square C -  gamma phase
				n[index] = asq1*( 2.*cos(kC1*y*dx) + 2.*cos(kC1*x*dx) )	//first mode
							+ asq2*( cos(kC1*(x+y)*dx) + cos(kC1*(x-y)*dx) + cos(kC1*(-x+y)*dx) + cos(kC1*(-x-y)*dx) );	//second mode
			}
		}
	}*/
	
	/*//two phase eutecitc
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;

			//initially liquid with average concentration
			n[index] = 0;
			cA[index] = cavgA;
			cB[index] = cavgB;

			if ( i < 400 )
			{
				if( yj <= Nx/6 || ( yj > 2.*Ny/6 && yj <= 3.*Ny/6  ) || ( yj > 4.*Ny/6 && yj <= 5.*Ny/6  ) )
				{
					x = i*cos(Angle_sqA) - yj*sin(Angle_sqA);
					y = i*sin(Angle_sqA) + yj*cos(Angle_sqA);

					//square A - alpha phase
					n[index] = asq1*( 2.*cos(kA1*y*dx) + 2.*cos(kA1*x*dx) )	//first mode
							 + asq2*( cos(kA1*(x+y)*dx) + cos(kA1*(x-y)*dx) + cos(kA1*(-x+y)*dx) + cos(kA1*(-x-y)*dx) );	//second mode

					cA[index] = csA_sqA;
					cB[index] = csB_sqA;
				}
				else
				{
					x = i*cos(Angle_sqC) - yj*sin(Angle_sqC);
					y = i*sin(Angle_sqC) + yj*cos(Angle_sqC);

					//square C -  gamma phase
					n[index] = asq1*( 2.*cos(kC1*y*dx) + 2.*cos(kC1*x*dx) )	//first mode
							 + asq2*( cos(kC1*(x+y)*dx) + cos(kC1*(x-y)*dx) + cos(kC1*(-x+y)*dx) + cos(kC1*(-x-y)*dx) );	//second mode
					
					cA[index] = csA_sqC;
					cB[index] = csB_sqC;
				}
			}
			else
			{
				cA[index] = ( cavgA - ( csA_sqA*fs_sqA + csA_sqC*fs_sqC ) ) / ( 1. - ( fs_sqA + fs_sqC ) );
				cB[index] = ( cavgB - ( csB_sqA*fs_sqA + csB_sqC*fs_sqC ) ) / ( 1. - ( fs_sqA + fs_sqC ) );
			}
		}
	}*/

	/*//three phase eutectic
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;

			//initially liquid with average concentration
			n[index] = 0;
			cA[index] = cavgA;
			cB[index] = cavgB;

			if ( yj < 600 )
			{
				if( ( i > Nx/6. && i <= 2.*Nx/6 ) || ( i > 4.*Nx/6 && i <= 5.*Nx/6) )
				{
					x = i*cos(Angle_sqA) - yj*sin(Angle_sqA);
					y = i*sin(Angle_sqA) + yj*cos(Angle_sqA);

					//square A - alpha phase
					n[index] = asq1*( 2.*cos(kA1*y*dx) + 2.*cos(kA1*x*dx) )	//first mode
							 + asq2*( cos(kA1*(x+y)*dx) + cos(kA1*(x-y)*dx) + cos(kA1*(-x+y)*dx) + cos(kA1*(-x-y)*dx) );	//second mode

					cA[index] = csA_sqA;
					cB[index] = csB_sqA;
				}
				else if( i <= Nx/6 || ( i > 3.*Nx/6 && i <= 4.*Nx/6  ) )
				{
					x = i*cos(Angle_sqB) - yj*sin(Angle_sqB);
					y = i*sin(Angle_sqB) + yj*cos(Angle_sqB);

					//square B -  beta phase
					n[index] = asq1*( 2.*cos(kB1*y*dx) + 2.*cos(kB1*x*dx) )	//first mode
							 + asq2*( cos(kB1*(x+y)*dx) + cos(kB1*(x-y)*dx) + cos(kB1*(-x+y)*dx) + cos(kB1*(-x-y)*dx) );	//second mode
					
					cA[index] = csA_sqB;
					cB[index] = csB_sqB;
				}
				else
				{
					x = i*cos(Angle_sqC) - yj*sin(Angle_sqC);
					y = i*sin(Angle_sqC) + yj*cos(Angle_sqC);

					//square C -  gamma phase
					n[index] = asq1*( 2.*cos(kC1*y*dx) + 2.*cos(kC1*x*dx) )	//first mode
							 + asq2*( cos(kC1*(x+y)*dx) + cos(kC1*(x-y)*dx) + cos(kC1*(-x+y)*dx) + cos(kC1*(-x-y)*dx) );	//second mode
					
					cA[index] = csA_sqC;
					cB[index] = csB_sqC;
				}
			}
			else
			{
				cA[index] = ( cavgA - ( csA_sqA*fs_sqA + csA_sqB*fs_sqB + csA_sqC*fs_sqC ) ) / ( 1. - ( fs_sqA + fs_sqB + fs_sqC ) );
				cB[index] = ( cavgB - ( csB_sqA*fs_sqA + csB_sqB*fs_sqB + csB_sqC*fs_sqC ) ) / ( 1. - ( fs_sqA + fs_sqB + fs_sqC ) );
			}
		}
	}*/
}
void setfftwPlans()
{
	/************** n **************/
	planF_n = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, n, kn, MPI_COMM_WORLD, FFTW_MEASURE);
	planB_n = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kn, n, MPI_COMM_WORLD, FFTW_MEASURE);
	planF_NL_n = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, nfNL, knfNL, MPI_COMM_WORLD, FFTW_MEASURE);

	/************** cA **************/
	planF_cA = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cA, kcA, MPI_COMM_WORLD, FFTW_MEASURE);
	planB_cA = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kcA, cA, MPI_COMM_WORLD, FFTW_MEASURE);
	planF_NL_cA = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cAfNL, kcAfNL, MPI_COMM_WORLD, FFTW_MEASURE);

	/************** cB **************/
	planF_cB = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cB, kcB, MPI_COMM_WORLD, FFTW_MEASURE);
	planB_cB = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kcB, cB, MPI_COMM_WORLD, FFTW_MEASURE);
	planF_NL_cB = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cBfNL, kcBfNL, MPI_COMM_WORLD, FFTW_MEASURE);

	/************** C2 **************/
	planB_CAn = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kCAn, CAn, MPI_COMM_WORLD, FFTW_MEASURE);
	planB_CBn = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kCBn, CBn, MPI_COMM_WORLD, FFTW_MEASURE);
	planB_CCn = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kCCn, CCn, MPI_COMM_WORLD, FFTW_MEASURE);

	planF_Ceffn = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, Ceffn, kCeffn, MPI_COMM_WORLD, FFTW_MEASURE);
	planF_CeffcA = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, CeffcA, kCeffcA, MPI_COMM_WORLD, FFTW_MEASURE);
	planF_CeffcB = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, CeffcB, kCeffcB, MPI_COMM_WORLD, FFTW_MEASURE);
}
void allocateArrays()
{
	/****************** n ******************/
	n = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	nfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	knfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** cA ******************/
	cA = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	cAfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kcA = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kcAfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** cB ******************/
	cB = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	cBfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kcB = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kcBfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** C2 ******************/
	CAn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CBn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CCn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	
	Ceffn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CeffcA = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CeffcB = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kCAn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCBn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCCn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	kCeffn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCeffcA = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCeffcB = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
}
void fftwMPIsetup()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);

	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_2d(Ny, Nx/2+1, MPI_COMM_WORLD,&local_n0, &local_0_start);
}
void domainParams()
{
	//from the lattice spacing and grid spacing, compute the number of grid points
	printf("dx=%f  numAtomsx=%d numAtomsy=%d\n",dx, atomsx,atomsy);
	
	Nx = (int)(floor(spacing/dx*atomsx));
	Ny = (int)(floor(spacing/dx*atomsy));

	printf("number of grid points in x %d\n",Nx);
	printf("number of grid points in y %d\n",Ny);
	
	Nx2 = 2*(Nx/2+1);

	//set up fourier scaling factors
	facx = 2.*Pi/(Nx*dx);
	facy = 2.*Pi/(Ny*dx);
}
void inputVariables()
{
	char *line = malloc(BUFSIZ);
	FILE *in;
	in = fopen("inputTernEutMPI","rt");	
   
	printf("reading input file\n");
	if (in == NULL)
	{
		printf("Either wrong file name or file does not exist\n");
		printf("Exiting simulation!\n");
	}
	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &atomsx,&atomsy);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HA0,&wA0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wA1,&wA2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMA1,&sigMA2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HB0,&wB0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wB1,&wB2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMB1,&sigMB2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HC0,&wC0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wC1,&wC2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMC1,&sigMC2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&iSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&cavgA,&cavgB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&csA_sqA,&csA_sqB,&csA_sqC);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&csB_sqA,&csB_sqB,&csB_sqC);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&Omega,&coA,&coB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&alphaA,&alphaB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&Mn,&McA,&McB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);
	fclose(in);

	printf("done reading input file\n");
	
	printf("numAtomsx=%d	numAtomsy=%d\n",atomsx,atomsy);
	printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
	printf("totalTime=%d	printFreq=%d\n",totalTime,printFreq);
	printf("HA0=%lf	wA0=%lf\n",HA0,wA0);
	printf("wA1=%lf	wA2=%lf\n",wA1,wA2);
	printf("sigMA1=%lf	sigMA2=%lf\n",sigMA1,sigMA2);
	printf("HB0=%lf	wB0=%lf\n",HB0,wB0);
	printf("wB1=%lf	wB2=%lf\n",wB1,wB2);
	printf("sigMB1=%lf	sigMB2=%lf\n",sigMB1,sigMB2);
	printf("HC0=%lf	wC0=%lf\n",HC0,wC0);
	printf("wC1=%lf	wC2=%lf\n",wC1,wC2);
	printf("sigMC1=%lf	sigMC2=%lf\n",sigMC1,sigMC2);
	printf("iSig=%lf\n",iSig);
	printf("cavgA=%lf	cavgB=%lf\n",cavgA,cavgB);
	printf("csA_sqA=%lf	csA_sqB=%lf csA_sqC=%lf\n",csA_sqA,csA_sqB,csA_sqC);
	printf("csB_sqA=%lf	csB_sqB=%lf csB_sqC=%lf\n",csB_sqA,csB_sqB,csB_sqC);
	printf("eta=%lf	chi=%lf\n",eta,chi);
	printf("coA=%lf coB=%lf	Omega=%lf\n",coA,coB,Omega);
	printf("alphaA=%lf alphaB=%lf\n",alphaA,alphaB);
	printf("Mn=%lf	McA=%lf McB=%lf\n",Mn,McA,McB);
	printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);	
}
void setPeakPositions()
{
	/*kA1 = 2.0822*Pi;
	kA2 = sqrt(2.)*kA1;

	kB1 = 1.8765*Pi;
	kB2 = sqrt(2.)*kB1;

	kC1 = 2.*Pi;
	kC2 = sqrt(2.)*kC1;*/

	kA1 = (81./38.)*Pi;
	kA2 = sqrt(2.)*kA1;

	kB1 = (54./29.)*Pi;
	kB2 = sqrt(2.)*kB1;

	kC1 = 2.*Pi;
	kC2 = sqrt(2.)*kC1;
}
int main(int argc, char* argv[])
{
	int t;	

	//set the corresponding peak positions
	setPeakPositions();

	//read in the parameter file
	inputVariables();

	//set up domain parameters
	domainParams();

	//setup the fftw mpi environment
	fftwMPIsetup();

	//allocate real and k-space arrays
	allocateArrays();

	MPI_Barrier(MPI_COMM_WORLD);

	//set up fftw plans
	setfftwPlans();

	MPI_Barrier(MPI_COMM_WORLD);

	srand48(time(NULL)+myid);
	//determine if this is a restart or initialization
	if (restartFlag == 0)
	{
		initialize();

		printf("output 0\n");
		output(0,n,"dens");
		output(0,cA,"concA");
		output(0,cB,"concB");
	}
	else
	{
		printf("restart flag has been triggered\n");
		printf("restart from time iteration %d\n",restartTime);
		restart(restartTime,n,"dens");
		restart(restartTime,cA,"concA");
		restart(restartTime,cB,"concB");
	}

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();

	MPI_Barrier(MPI_COMM_WORLD);

	//setting up the initial debye waller prefactors based on the initial temperature
	setDebyeWaller(iSig);	
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	//********************************************************************************************************************
	printf("beginning iterations\n");
	for(t=restartTime+1;t<totalTime;t++)
	{
		if (t>1000)
			dt=10;
		fftw_execute(planF_n);			//forward transform density
		fftw_execute(planF_cA);			//forward transform concentration A
		fftw_execute(planF_cB);			//forward transform concentration B

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point	

		/****** inverse transform correlations and normalize *******/
		calcCorrelations();				//caluclate the convolution of the correlation and density in kspace
		fftw_execute(planB_CAn);		//back transform A correlation after convolution with density
		normalize(CAn);					//normalize the transform
		fftw_execute(planB_CBn);		//back transform B correlation after convolution with density
		normalize(CBn);					//normalize the transform
		fftw_execute(planB_CCn);		//back transform C correlation after convolution with density
		normalize(CCn);					//normalize the transform

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		/****** non-linear and effective correlations *******/
		calcCeffn();					//calculate non-linear terms and effective correlation (variation with density)
		fftw_execute(planF_NL_n);		//forward transform non-linear density terms
		fftw_execute(planF_Ceffn);		//forward transform effective correlation density
		
		calcCeffcA();					//calculate non-linear terms and effective correlation (variation with cA)
		fftw_execute(planF_NL_cA);		//forward transform non-linear concentration terms
		fftw_execute(planF_CeffcA);		//forward transform effective correlation concentration

		calcCeffcB();					//calculate non-linear terms and effective correlation (variation with cB)
		fftw_execute(planF_NL_cB);		//forward transform non-linear concentration terms
		fftw_execute(planF_CeffcB);		//forward transform effective correlation concentration
		
		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point
		
		/****Note****/ //some improved stability maybe gain by solving for density first, and then concentration
		
		/****** time step fields *******/
		timeStepn();
		timeStepcA();
		timeStepcB();

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		/****** inverse transform and normalize field arrays *******/
		fftw_execute(planB_n);		//inverse transform density
		normalize(n);				//normalize density after inverse transform		
		fftw_execute(planB_cA);		//inverse transform concentration A
		normalize(cA);				//normalize concentration A after inverse transform
		fftw_execute(planB_cB);		//inverse transform concentration B
		normalize(cB);				//normalize concentration B after inverse transform
		
		
		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		if( t%printFreq == 0 )
		{
			printf("time = %d\n",t);
			output(t,n,"dens");
			output(t,cA,"concA");
			output(t,cB,"concB");
		}

	}

	MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point	
	freeMemory();  //free memory and destroy fftw plans and arrays

	//exit mpi environment
	MPI_Finalize();

	return 0;
}