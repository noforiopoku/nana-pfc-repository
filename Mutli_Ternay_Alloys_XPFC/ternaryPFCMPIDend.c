#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <fftw3-mpi.h>

#define Pi (2.*acos(0.))

//domain parameters
int atomsx,atomsy;	//number of atoms in x and y
ptrdiff_t Nx,Ny;	//number of grid points in x and y
ptrdiff_t Nx2;		//size factor for local cpu		
double spacing;		//lattice spacing normalized to 1
double dx;			//numerical grid spacing

//time parameters
int totalTime, printFreq;		//simulation time and print frequency
double dt;						//numerical time step
int restartFlag,restartTime;	//flag for restarting (set to 1) and time

//MPI parameters
int argc;
char **argv;
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

//********************** correlation variables for A **********************//
//k-zero mode
double HA0;	//height/depth of k-zero mode A
double wA0;	//width of k-zero mode A
double PreCA0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kA1,kA2;	//k-space peak positions for first and second mode for A
double wA1,wA2;	//widths of the first and second mode peaks )sets elasticity, anisotropy, interface widths etc)
double DWA1,DWA2;	//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCA1,PreCA2;	//prefactors based on the widths of the correlation peaks

double sigMA1,sigMA2;	//sets the effective melting points for the corresponding mode of the correlation


//********************** correlation variables for B **********************/
//k-zero mode
double HB0;	//height/depth of k-zero mode B
double wB0;	//width of k-zero mode B
double PreCB0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kB1,kB2;		//k-space peak positions for first and second mode for B
double wB1,wB2;		//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double DWB1,DWB2;		//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCB1,PreCB2;	//prefactors based on the widths of the correlation peaks

double sigMB1,sigMB2;	//sets the effective melting points for the corresponding mode of the correlation

//********************** correlation variables for C **********************/
//k-zero mode
double HC0;	//height/depth of k-zero mode C
double wC0;	//width of k-zero mode C
double PreCC0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kC1,kC2;		//k-space peak positions for first and second mode for C
double wC1,wC2;		//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double DWC1,DWC2;		//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCC1,PreCC2;	//prefactors based on the widths of the correlation peaks

double sigMC1,sigMC2;	//sets the effective melting points for the corresponding mode of the correlation

//*************initial condition and simulation variables
double iSig;		//initial temperature (note, can be expanded with an iteration count to do cooling)

double cavgA;		//average concentration in A
double csA,clA;		//equilibrium solidus and liquidus concentration for temperature iSig

double cavgB;		//average concentration in B
double csB,clB;		//equilibrium solidus and liquidus concentration for temperature iSig

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mn;			//mobility constant for density

double cAo,cBo;			//reference liquid compositions for A and B
double Omega;			//entropy of mixing coefficient
double alphaA,alphaB;	//gradient energy coefficients for A and B
double McA,McB;			//mobility for solute concentrations A and B

double facx,facy;	//fourier factors for scaling lengths in k-space


//*******************Arrays for fields
//real space arrays
double *n,*cA,*cB;				//density and concentration
double *nfNL,*cAfNL,*cBfNL;		//non-linear terms for density and concentration
double *CAn,*CBn,*CCn;			//real space arrays for correlation convoluted with n
double *Ceffn,*CeffcA,*CeffcB;	//effective correlations after interpolation with concentration

//complex arrays
fftw_complex *kn,*kcA,*kcB;				//k-space density and concentration
fftw_complex *knfNL,*kcAfNL,*kcBfNL;	//k-space non-linear terms
fftw_complex *kCAn,*kCBn,*kCCn;			//k-space convolution of correlation and density
fftw_complex *kCeffn,*kCeffcA,*kCeffcB;	//k-space effective correlations

//fftw plans
fftw_plan planF_n, planB_n;			//forward and backward plan for density
fftw_plan planF_NL_n;				//non-linear forward transform for density
fftw_plan planF_Ceffn;				//forward transforms for effective correlations in density
fftw_plan planB_CAn,planB_CBn,planB_CCn;	//backward transforms for convoluted correlations with density

fftw_plan planF_cA, planB_cA;		//forward and backward plan for concentration A
fftw_plan planF_cB, planB_cB;		//forward and backward plan for concentration B
fftw_plan planF_NL_cA,planF_NL_cB;	//non-linear forward transforms for solute concentrations A and B
fftw_plan planF_CeffcA,planF_CeffcB;	//forward transforms for effective correlations fo solute concentrations A and B

void freeMemory()
{
	free(n);free(nfNL);free(Ceffn);
	free(cA);free(cB);free(cAfNL);free(cBfNL);
	free(CeffcA);free(CeffcB);
	free(CAn);free(CBn);free(CCn);	
	
	fftw_free(kn);fftw_free(knfNL);fftw_free(kCeffn);
	fftw_free(kcA);fftw_free(kcB);fftw_free(kcAfNL);fftw_free(kcBfNL);
	fftw_free(kCeffcA);fftw_free(kCeffcB);
	fftw_free(kCAn);fftw_free(kCBn);fftw_free(kCCn);	

	fftw_destroy_plan(planF_n);fftw_destroy_plan(planB_n);
	fftw_destroy_plan(planF_cA);fftw_destroy_plan(planB_cA);
	fftw_destroy_plan(planF_cB);fftw_destroy_plan(planB_cB);
	fftw_destroy_plan(planF_NL_n);
	fftw_destroy_plan(planF_NL_cA);fftw_destroy_plan(planF_NL_cB);
	fftw_destroy_plan(planB_CAn);fftw_destroy_plan(planB_CBn);fftw_destroy_plan(planB_CCn);
	fftw_destroy_plan(planF_Ceffn);
	fftw_destroy_plan(planF_CeffcA);fftw_destroy_plan(planF_CeffcB);
}
void restart(int time,double *Array,char *filepre)
{
	double tmp[Nx*Ny];	//temporary array to read in and then dessiminate field
	ptrdiff_t jg;	//global index for y direction

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    fp = fopen(filename,"rt");
	
	ptrdiff_t i,j; 
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	//read in file to temporary array
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			fscanf(fp,"%lf",&tmp[i+j*Nx]);
		}
	}
    fclose(fp);
	
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+jg*Nx2] = tmp[i+j*Nx];
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
	fp = fopen(filename,"w");

	ptrdiff_t i,j;

	int kk;

	MPI_Barrier(MPI_COMM_WORLD);

	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to open file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(j=0;j<local_n0;j++)
			{
				for(i=0;i<Nx;i++)
				{
					fprintf(fp,"%f  \n",Array[i+j*Nx2]);
				}
			}
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void normalize(double *Array)
{
	ptrdiff_t i,j;
	
	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			Array[i + Nx2*j] /= (Nx*Ny);
		}
	}
}
void timeStepcB()
{
	ptrdiff_t i,j;
	ptrdiff_t yj;
	ptrdiff_t index;
	double kx,ky,k2;
	double prefactorcB;

	/************** Step concentration B*************/	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
			kx = i*facx;
			k2 = kx*kx + ky*ky;
				
			prefactorcB = 1./( 1. + dt*McB*k2*k2*alphaB );

			kcB[index][0] = prefactorcB * ( kcB[index][0] - dt*McB*k2*( kcBfNL[index][0] - kCeffcB[index][0] ) );
			kcB[index][1] = prefactorcB * ( kcB[index][1] - dt*McB*k2*( kcBfNL[index][1] - kCeffcB[index][1] ) );
		}
	}
}
void timeStepcA()
{
	ptrdiff_t i,j;
	ptrdiff_t yj;
	ptrdiff_t index;
	double kx,ky,k2;
	double prefactorcA;

	/************** Step concentration A*************/	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
			kx = i*facx;
			k2 = kx*kx + ky*ky;
				
			prefactorcA = 1./( 1. + dt*McA*k2*k2*alphaA );

			kcA[index][0] = prefactorcA * ( kcA[index][0] - dt*McA*k2*( kcAfNL[index][0] - kCeffcA[index][0] ) );
			kcA[index][1] = prefactorcA * ( kcA[index][1] - dt*McA*k2*( kcAfNL[index][1] - kCeffcA[index][1] ) );
		}
	}
}
void timeStepn()
{
	ptrdiff_t i,j;
	ptrdiff_t yj;
	ptrdiff_t index;
	double kx,ky,k2;
	double prefactorn;

	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
			kx = i*facx;
			k2 = kx*kx + ky*ky;

			prefactorn = 1./( 1. + dt*Mn*k2 );

			kn[index][0] = prefactorn * ( kn[index][0] - dt*Mn*k2*( knfNL[index][0] - kCeffn[index][0] ) );
			kn[index][1] = prefactorn * ( kn[index][1] - dt*Mn*k2*( knfNL[index][1] - kCeffn[index][1] ) );
		}
	}
}
void convolutionCeffcB()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	double c2A;	//hold the quadratic term for composition A
	double c2B;	//hold the quadratic term for composition B
	double cAB;		//hold the multiplication cA*cB

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;

			c2A = cA[index]*cA[index];	//cA squared
			c2B = cB[index]*cB[index];	//cB squared
			cAB = cA[index]*cB[index];	//cA*cB

			cBfNL[index] = Omega*( n[index] + 1. ) * ( log(cB[index]/cBo) - log( (1.-cA[index]-cB[index]) / (1.-cAo-cBo) ) );
				
			CeffcB[index] = 0.5*n[index] * ( 2.*cA[index] - 2.*c2A - 4.*cAB ) * CAn[index]
						  + 0.5*n[index] * ( 2.*cA[index] + 6.*cB[index] - 2.*c2A - 4.*cAB - 6.*c2B ) * CBn[index] 
						  + 0.5*n[index] * ( -6.*cB[index] + 6.*c2B - 4.*cA[index] + 4.*c2A + 8.*cAB ) * CCn[index] ; 
					
		}
	}
}
void convolutionCeffcA()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	double c2A;	//hold the quadratic term for composition A
	double c2B;	//hold the quadratic term for composition B
	double cAB;		//hold the multiplication cA*cB

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;

			c2A = cA[index]*cA[index];	//cA squared
			c2B = cB[index]*cB[index];	//cB squared
			cAB = cA[index]*cB[index];	//cA*cB

			cAfNL[index] = Omega*( n[index] + 1. ) * ( log(cA[index]/cAo) - log( (1.-cA[index]-cB[index]) / (1.-cAo-cBo) ) );
				
			CeffcA[index] = 0.5*n[index] * ( 6.*cA[index] + 2.*cB[index] - 6.*c2A - 4.*cAB - 2.*c2B ) * CAn[index]
						  + 0.5*n[index] * ( 2.*cB[index] - 4.*cAB - 2.*c2B ) * CBn[index] 
						  + 0.5*n[index] * ( -6.*cA[index] + 6.*c2A - 4.*cB[index] + 8.*cAB + 4.*c2B ) * CCn[index] ; 
					
		}
	}
}
void convolutionCeffn()
{
	ptrdiff_t i,j;
	ptrdiff_t index;

	double c2A,c3A;	//hold the quadratic and cubic terms for composition A
	double c2B,c3B;	//hold the quadratic and cubic terms for composition B
	double cAB;		//hold the multiplication cA*cB

	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;

			c2A = cA[index]*cA[index];
			c3A = c2A*cA[index];

			c2B = cB[index]*cB[index];
			c3B = c2B*cB[index];

			cAB = cA[index]*cB[index];

			nfNL[index] = n[index]*n[index]*( -0.5*eta + chi*n[index]/3. ) 
						+ Omega* ( cA[index] * log( cA[index] / cAo ) + cB[index] * log( cB[index] / cBo ) 
						+ (1.-cA[index]-cB[index]) * log( (1.-cA[index]-cB[index]) / (1.-cAo-cBo) ) );
				
			Ceffn[index] = ( 3.*c2A + 2.*cAB - 2.*c3A - 2.*c2A*cB[index] - 2.*cA[index]*c2B ) * CAn[index]
						 + ( 2.*cAB + 3.*c2B - 2.*c2A*cB[index] - 2.*cA[index]*c2B - 2.*c3B ) * CBn[index]
						 + ( 1. - 3.*c2A + 2.*c3A - 3.*c2B + 2.*c3B - 4.*cAB + 4.*c2A*cB[index] + 4.*cA[index]*c2B ) * CCn[index];
		}
	}
}
void calcCorrelations()
{
	ptrdiff_t i,j;
	ptrdiff_t yj;
	ptrdiff_t index;
	double kx,ky,k2,rk;

	//temporary variables for the peaks
	double kCA0,kCB0,kCC0;
	double kCA1,kCB1,kCC1;
	double kCA2,kCB2,kCC2;
	double kCA,kCB,kCC;

	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		for(i=0;i<(Nx/2+1);i++)
		{
			index = i + (Nx/2+1)*j;
				
			kx = i*facx;
			k2 = kx*kx + ky*ky;
			rk = sqrt(k2);

			//****************************CA**************************//
			kCA0 = exp(PreCA0*k2);
			kCA1 = exp(PreCA1* (rk-kA1)*(rk-kA1) );
			kCA2 = exp(PreCA2* (rk-kA2)*(rk-kA2) );				

			//creating the correlation overlap
			if (HA0*kCA0 > DWA1*kCA1)
				kCA = -HA0*kCA0;
			else if (DWA1*kCA1 > DWA2*kCA2)
				kCA = DWA1*kCA1;
			else
				kCA = DWA2*kCA2;

			kCAn[index][0] = kn[index][0]*kCA;
			kCAn[index][1] = kn[index][1]*kCA;


			//****************************CB**************************//
			kCB0 = exp(PreCB0*k2);
			kCB1 = exp(PreCB1* (rk-kB1)*(rk-kB1) );
			kCB2 = exp(PreCB2* (rk-kB2)*(rk-kB2) );

			//creating the correlation overlap
			if (HB0*kCB0 > DWB1*kCB1)
				kCB = -HB0*kCB0;
			else if (DWB1*kCB1 > DWB2*kCB2)
				kCB = DWB1*kCB1;
			else
				kCB = DWB2*kCB2;

			kCBn[index][0] = kn[index][0]*kCB;
			kCBn[index][1] = kn[index][1]*kCB;

			//****************************CC**************************//
			kCC0 = exp(PreCC0*k2);
			kCC1 = exp(PreCC1* (rk-kC1)*(rk-kC1) );
			kCC2 = exp(PreCC2* (rk-kC2)*(rk-kC2) );

			//creating the correlation overlap
			if (HC0*kCC0 > DWC1*kCC1)
				kCC = -HC0*kCC0;
			else if (DWC1*kCC1 > DWC2*kCC2)
				kCC = DWC1*kCC1;
			else
				kCC = DWC2*kCC2;

			kCCn[index][0] = kn[index][0]*kCC;
			kCCn[index][1] = kn[index][1]*kCC;
		}
	}

	fftw_execute(planB_CAn);
	fftw_execute(planB_CBn);
	fftw_execute(planB_CCn);

	//normalize the transform
	for(j=0;j<local_n0;j++)
	{
		for(i=0;i<Nx;i++)
		{
			index = i + Nx2*j;
			CAn[index] /= (Nx*Ny);
			CBn[index] /= (Nx*Ny);
			CCn[index] /= (Nx*Ny);
		}
	}
}
void setdebyeWaller(double Sig)
{
	DWA1 = exp(-Sig*Sig/(sigMA1*sigMA1) );
	DWA2 = exp(-Sig*Sig/(sigMA2*sigMA2) );
	
	DWB1 = exp(-Sig*Sig/(sigMB1*sigMB1) );
	DWB2 = exp(-Sig*Sig/(sigMB2*sigMB2) );

	DWC1 = exp(-Sig*Sig/(sigMC1*sigMC1) );
	DWC2 = exp(-Sig*Sig/(sigMC2*sigMC2) );
}
void setCorrPeakPrefactors()
{
	PreCA0 = -0.5/(wA0*wA0);
	PreCA1 = -0.5/(wA1*wA1);
	PreCA2 = -0.5/(wA2*wA2);

	PreCB0 = -0.5/(wB0*wB0);
	PreCB1 = -0.5/(wB1*wB1);
	PreCB2 = -0.5/(wB2*wB2);

	PreCC0 = -0.5/(wC0*wC0);
	PreCC1 = -0.5/(wC1*wC1);
	PreCC2 = -0.5/(wC2*wC2);
}
void initialize()
{
	ptrdiff_t i,j;
	ptrdiff_t yj;
	ptrdiff_t index;

	double asq1=0.23;
	double asq2=0.14;

	double R=16.;
	double fs= (Pi*R*R)/(Nx*Ny);

	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		for(i=0;i<Nx;i++)
		{
			index = i+Nx2*j;

			n[index] = 0;
			cA[index] = cavgA;
			cB[index] = cavgB;

			if( sqrt( (yj-Ny/2)*(yj-Ny/2) + (i-Nx/2)*(i-Nx/2) ) <= R ) //( i < (Nx/2+10) && i> (Nx/2-10) )
			{
				//square C - gamma phase
				n[index] = asq1*( 2.*cos(kC1*yj*dx) + 2.*cos(kC1*i*dx) )	//first mode
						 + asq2*2.*( cos(kC1*(i+yj)*dx) + cos(kC1*(i-yj)*dx) + cos(kC1*(-i+yj)*dx) + cos(kC1*(-i-yj)*dx) );	//second mode

				//cA[index] = csA;
				//cB[index] = csB;
			}
			else
			{
				//cA[index] = ( cavgA-csA*fs ) / ( 1.-fs );
				//cB[index] = ( cavgB-csB*fs ) / ( 1.-fs );
			}

		}
	}


	
}
void setfftwPlans()
{
	/************** n **************/
	planF_n = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, n, kn, MPI_COMM_WORLD , FFTW_MEASURE);
	planB_n = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kn, n, MPI_COMM_WORLD , FFTW_MEASURE);
	planF_NL_n = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, nfNL, knfNL, MPI_COMM_WORLD , FFTW_MEASURE);

	/************** cA **************/
	planF_cA = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cA, kcA, MPI_COMM_WORLD , FFTW_MEASURE);
	planB_cA = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kcA, cA, MPI_COMM_WORLD , FFTW_MEASURE);
	planF_NL_cA = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cAfNL, kcAfNL, MPI_COMM_WORLD , FFTW_MEASURE);

	/************** cB **************/
	planF_cB = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cB, kcB, MPI_COMM_WORLD , FFTW_MEASURE);
	planB_cB = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kcB, cB, MPI_COMM_WORLD , FFTW_MEASURE);
	planF_NL_cB = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, cBfNL, kcBfNL, MPI_COMM_WORLD , FFTW_MEASURE);

	/************** C2 **************/
	planB_CAn = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kCAn, CAn, MPI_COMM_WORLD , FFTW_MEASURE);
	planB_CBn = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kCBn, CBn, MPI_COMM_WORLD , FFTW_MEASURE);
	planB_CCn = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, kCCn, CCn, MPI_COMM_WORLD , FFTW_MEASURE);

	planF_Ceffn = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, Ceffn, kCeffn, MPI_COMM_WORLD , FFTW_MEASURE);
	planF_CeffcA = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, CeffcA, kCeffcA, MPI_COMM_WORLD , FFTW_MEASURE);
	planF_CeffcB = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, CeffcB, kCeffcB, MPI_COMM_WORLD , FFTW_MEASURE);
}
void allocateArrays()
{
	/****************** n ******************/
	n = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	nfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	knfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** cA ******************/
	cA = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	cAfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kcA = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kcAfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** cB ******************/
	cB = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	cBfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kcB = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kcBfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** C2 ******************/
	CAn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CBn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CCn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	
	Ceffn = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CeffcA = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CeffcB = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kCAn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCBn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCCn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	kCeffn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCeffcA = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCeffcB = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
}
void fftwMPIsetup()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);

	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_2d(Ny, Nx, MPI_COMM_WORLD,&local_n0, &local_0_start);
}
void domainParams()
{
	//from the lattice spacing and grid spacing, compute the number of grid points
	printf("dx=%f  numAtomsx=%d numAtomsy=%d\n",dx, atomsx,atomsy);
	
	Nx = (int)(floor(spacing/dx*atomsx));
	Ny = (int)(floor(spacing/dx*atomsy));

	printf("number of grid points in x %d\n",Nx);
	printf("number of grid points in y %d\n",Ny);
	
	//size factor for local cpu
	Nx2 = 2*(Nx/2+1);

	//set up fourier scaling factors
	facx = 2.*Pi/(Nx*dx);
	facy = 2.*Pi/(Ny*dx);
}
void inputVariables()
{
	char *line = malloc(BUFSIZ);
	FILE *in;
	in = fopen("inputTernayMPIDend","rt");	
   
	printf("reading input file\n");
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &atomsx,&atomsy);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HA0,&wA0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wA1,&wA2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMA1,&sigMA2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HB0,&wB0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wB1,&wB2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMB1,&sigMB2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HC0,&wC0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wC1,&wC2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMC1,&sigMC2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&iSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&cavgA,&cAo);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&csA,&clA);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&cavgB,&cBo);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&csB,&clB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&Omega);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&alphaA,&alphaB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&Mn,&McA,&McB);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);
	fclose(in);

	printf("done reading input file\n");
	
	printf("numAtomsx=%d	numAtomsy=%d\n",atomsx,atomsy);
	printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
	printf("totalTime=%d	printFreq=%d\n",totalTime,printFreq);
	printf("HA0=%lf	wA0=%lf\n",HA0,wA0);
	printf("wA1=%lf	wA2=%lf\n",wA1,wA2);
	printf("sigMA1=%lf	sigMA2=%lf\n",sigMA1,sigMA2);
	printf("HB0=%lf	wB0=%lf\n",HB0,wB0);
	printf("wB1=%lf	wB2=%lf\n",wB1,wB2);
	printf("sigMB1=%lf	sigMB2=%lf\n",sigMB1,sigMB2);
	printf("HC0=%lf	wC0=%lf\n",HC0,wC0);
	printf("wC1=%lf	wC2=%lf\n",wC1,wC2);
	printf("sigMC1=%lf	sigMC2=%lf\n",sigMC1,sigMC2);
	printf("iSig=%lf\n",iSig);
	printf("cavgA=%lf	cAo=%lf\n",cavgA,cAo);
	printf("csA=%lf	clA=%lf\n",csA,clA);
	printf("cavgB=%lf	cBo=%lf\n",cavgB,cBo);
	printf("csB=%lf	clB=%lf\n",csB,clB);
	printf("eta=%lf	chi=%lf\n",eta,chi);
	printf("alphaA=%lf	alphaB=%lf	Omega=%lf\n",alphaA,alphaB,Omega);
	printf("Mn=%lf	McA=%lf	McB=%lf\n",Mn,McA,McB);
	printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);
	
}
int main(int argc, char* argv[])
{
	int t;
	//set the corresponding peak positions
	kA1 = (81./38.)*Pi;
	kA2 = sqrt(2.)*kA1;

	kB1 = (54./29.)*Pi;
	kB2 = sqrt(2.)*kB1;

	kC1 = 2.*Pi;
	kC2 = sqrt(2.)*kC1;

	//read in the parameter file
	inputVariables();

	//set up domain parameters
	domainParams();

	//setup the fftw mpi environment
	fftwMPIsetup();

	MPI_Barrier(MPI_COMM_WORLD);

	//allocate real and k-space arrays
	allocateArrays();

	//set up fftw plans
	setfftwPlans();

	MPI_Barrier(MPI_COMM_WORLD);

	srand48(time(NULL)+myid);	// initialize the random number seed for each cpu
	//determine if this is a restart or initialization
	if (restartFlag == 0)
	{
		initialize();

		//output initial conditions
		//density
		printf("output 0\n");
		output(0,n,"dens");

		//solute concentrations
		output(0,cA,"concA");
		output(0,cB,"concB");
	}
	else
	{
		printf("restart flag has been triggered\n");
		printf("restart from time iteration %d\n",restartTime);
		
		//read in files for initial condition or continuing
		//density
		restart(restartTime,n,"dens");

		//solute concentrations
		restart(restartTime,cA,"concA");
		restart(restartTime,cB,"concB");
	}

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();

	//setting up the initial debye waller prefactors based on the initial temperature
	setdebyeWaller(iSig);	
	
	MPI_Barrier(MPI_COMM_WORLD);
	/*********************************************************************************************************/
	//begin interations
	printf("%s","begin time iteration\n");	
	for(t=restartTime+1;t<totalTime+restartTime+1;t++)
	{
		if(t == 10000)
			dt = 10;
		else if (t == 100000)
			dt = 20;		

		fftw_execute(planF_n);	//forward transform the density
		fftw_execute(planF_cA);	//forward transform solute concentration A
		fftw_execute(planF_cB);	//forward transform solute concentration B

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		calcCorrelations();			//calculate (in k-space) correlations-density convolution, inverse transform and normalize

		convolutionCeffn();			//effective correlation calcuation using concentration interpolation and density mutliplication and non-linear terms
		fftw_execute(planF_NL_n);	//forward transform non-linear terms
		fftw_execute(planF_Ceffn);	//forward transform effective correlation for density dynamics
		timeStepn();				//update density (k-space)
		fftw_execute(planB_n);		//inverse transform
		normalize(n);				//normalize density after inverse transform

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		convolutionCeffcA();		//effective correlation calcuation using concentration interpolation and density mutliplication and non-linear terms
		fftw_execute(planF_NL_cA);	//forward transform non-linear terms
		fftw_execute(planF_CeffcA);	//forward transform effective correlation for concentration A dynamics
		timeStepcA();				//update concentration A (k-space)
		fftw_execute(planB_cA);		//inverse transform
		normalize(cA);				//normalize concentration A after inverse transform

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		convolutionCeffcB();
		fftw_execute(planF_NL_cB);	//forward transform non-linear terms
		fftw_execute(planF_CeffcB);	//forward transform effective correlation for concentration B dynamics
		timeStepcB();				//update concentration B (k-space)
		fftw_execute(planB_cB);		//inverse transform
		normalize(cB);				//normalize concentration B after inverse transform

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point
		//output current data
		if( t%printFreq == 0 )
		{
			printf("time = %d\n",t);
			//output density
			output(t,n,"dens");

			//output solute concentrations
			output(t,cA,"concA");
			output(t,cB,"concB");			
		}

	}

	MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point
	freeMemory(); //free memory and destroy fftw plans and arrays

	//exit mpi environment
	MPI_Finalize();

	return 0;
}
