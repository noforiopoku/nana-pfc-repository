#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <complex>
#include <fftw3-mpi.h>

using namespace std;

#define Pi (2.*acos(0.))


//domain parameters
char run[3];			//label of current run
double dx,dy;				//grid spacing
double spacing;			//normalized lattice spacing

double atomsx,atomsy;	//number of lattice units in x,y and z
ptrdiff_t Nx,Ny;		//number of grid points in x,y and z
ptrdiff_t Nx2,Nxn2;		//needed for padding real arrays for transform

//time parameters
int totalTime,printFreq;	//total iteration time and print frequency
int eneFreq,peakFreq;		//frequency for energy and peak
double dt;					//time step
int restartFlag;			//set to 1 for restart
int restartTime;			//restart time

//MPI parameters
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

//element A ******************************************
//k-zero mode
double HA0, wA0;			//height of k-zero mode, width of k-zero mode
double PreCA0;				//prefactor based on the widths of the k-zero mode

//first and second modes
double qA1,qA2;				//equilibrium peak positions for first and second mode
double wA1,wA2;				//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double sigMA1,sigMA2;		//effective transition temperatures

double DWA1,DWA2;			//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCA1,PreCA2;		//prefactors calculated with the widths of the correlation peaks

//element B ******************************************
//k-zero mode
double HB0, wB0;			//height of k-zero mode, width of k-zero mode
double PreCB0;				//prefactor based on the widths of the k-zero mode

//first mode
double qB1;					//equilibrium peak positions for first and second mode
double wB1;					//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double sigMB1;				//effective transition temperatures

double DWB1;				//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCB1;				//prefactors calculated with the widths of the correlation peaks

/************* initial condition and simulation variables *************/
double iSig,dSig;	//initial temperature, temperature change
double cavg,cs;		//average concentration, eqlm solid conc

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mn;			//mobility constant for density

double omega,kappa;	//entropy of mixing coeff, gradient energy coeff
double co,Mc;		//reference composition, concentration mobility

double amp;			//noise amplitude

double facx,facy,normFac;	//fourier factors for scaling lengths in k-space

//coeffs for effective correlation interpolation functions
double a0,a1,a2,a3;		//CAA
double b0,b1,b2,b3;		//CBB

/*********************** density ****************************/
double *n;		//density
double *nfNL;	//holds collection of non-linear terms 

complex<double> *kn;		//complex density after transform
complex<double> *knfNL;	//complex non-linear density terms after transform

/*********************** concentration ****************************/
double *c;		//concentration
double *cfNL;	//holds collection of non-linear terms 

complex<double> *kc;		//complex conc after transform
complex<double> *kcfNL;	//complex non-linear density terms after transform

/*********************** correlation ****************************/
double *CAAn, *CBBn;			//correlation convolured with density C2n	
complex<double> *kCAAn, *kCBBn;	//k-space correlation convolured with density C2n
double *karr;					//holds k2 array in k-sapce

fftw_plan planB, planF;	//backward and forward plans

void freeMemory()
{
	/*********************** destroy fftw plans **************************/
	fftw_destroy_plan(planB);
	fftw_destroy_plan(planF);

	/*********************** fftw_free memory **************************/
	fftw_free(n);	fftw_free(nfNL);		
	delete[] kn;	delete[] knfNL;	

	fftw_free(c);	fftw_free(cfNL);
	delete[] kc;	delete[] kcfNL;

	fftw_free(CAAn);	fftw_free(CBBn);
	delete[] kCAAn;	delete[] kCBBn;

	fftw_free(karr);
}
void restart(int time,double *Array,char *filepre,char *runtype,int bin)
{
	double *tmp;
	tmp = fftw_alloc_real(Nx*Ny);

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%sB%s%d.dat",runtype,filepre,time);
    fp = fopen(filename,"rb");

	ptrdiff_t i,j;
	ptrdiff_t jg;	//global index for y direction

	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	fread(tmp,sizeof(double),Ny*Nx,fp);
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
	    {
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+jg*Nx2] = tmp[i+j*Nx];
	    }
	}
	fftw_free(tmp);
	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre,char *runtype,int bin)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%sB%s%d.dat",runtype,filepre,time);

	ptrdiff_t i,j;
	int kk;

	MPI_Barrier(MPI_COMM_WORLD);

	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"wb");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"ab");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
   
			fwrite(Array,sizeof(float),local_n0*Nx,fp);
					
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void restart(int time,double *Array,char *filepre,char *runtype)
{
	double *tmp;
	tmp = fftw_alloc_real(Nx*Ny);

	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%s%d.dat",runtype,filepre,time);
    fp = fopen(filename,"rt");
	
	ptrdiff_t i,j;
	ptrdiff_t jg;	//global index for z direction due to mpi
	
	if( fp == NULL )
	{
		printf("Unable to open file for reading\n");
		printf("Exiting simulation!\n");
		exit(1);
	}
	
	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			fscanf(fp,"%lf",&tmp[i+Nx*j]);
		}
	}
    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(j=0;j<Ny;j++)
	{
		for(i=0;i<Nx;i++)
		{
			jg = j - myid*local_n0;	//rescale coordinates to that of local processor
			if (jg >= 0 && jg <= local_n0-1)
				Array[i+Nx2*jg] = tmp[i+Nx*j];
		}
	}
	
	fftw_free(tmp);
	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre,char *runtype)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%s%d.dat",runtype,filepre,time);

	ptrdiff_t i,j;
	ptrdiff_t index1;
	int kk;
	
	MPI_Barrier(MPI_COMM_WORLD);

	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to reopen file for appending\n");
					printf("Exiting simulation!\n");
					exit(1);
				}
			}

			for(j=0;j<local_n0;j++)
			{
				index1 = j*Nx2;
				for(i=0;i<Nx;i++)
				{
					fprintf(fp,"%lf\n",Array[i + index1]);
				}
			}
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void normalize(double *Array)
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;
			Array[index] *= normFac;
		}
	}
}
void energy(int time,char *runtype)
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;

	double ene,mun,muc,avgdens,avgconc;
	double ened,mund,mucd,avgdensd,avgconcd;
	int isource;
	MPI_Status status;

	char filename[BUFSIZ];
    FILE *fpene;
    
	if (myid==0)
    {
    	sprintf(filename,"%s.ene",runtype);
		fpene = fopen(filename,"a");
    }

	//Cahn-Hilliard gradient term
	for(j=0;j<local_n0;j++)
	{
		index1 = Nxn2*j;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;
			kcfNL[index] = -karr[index]*kc[index];
		}
	}
	fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kcfNL),cfNL);	//inverse transform conc
	normalize(cfNL);							//normalize 
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Add bulk terms to correlation part of energy
    ene=0.;
    mun=0.;
	muc=0.;
    avgdens=0.;
	avgconc=0.;
	for(j=0;j<local_n0;j++)
	{
		index1 = Nx2*j;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;
            ene = ene + .5*n[index]*n[index]*( 1. + n[index]*(-eta/3. + chi*n[index]/6.) )
					  + omega * ( n[index] + 1. ) * ( c[index]*log( c[index] / co ) + ( 1. - c[index] )*log( ( 1. - c[index] ) / ( 1. - co ) ) )
					  - .5*n[index] * ( a0 + ( a1 + ( a2 + a3*c[index] )*c[index] )*c[index] ) * CAAn[index]
					  - .5*n[index] * ( b0 + ( b1 + ( b2 + b3*c[index] )*c[index] )*c[index] ) * CBBn[index]
					  - .5*c[index]*kappa*cfNL[index];	//cahn-hilliard gradient term energy
         
			mun = mun + n[index] + n[index]*n[index]*( -.5*eta + chi*n[index]/3. )
					  + omega * ( c[index]*log( c[index] / co ) + ( 1. - c[index] )*log( ( 1. - c[index] ) / ( 1. - co ) ) )
					  - ( a0 + ( a1 + ( a2 + a3*c[index] )*c[index] )*c[index] ) * CAAn[index]
					  - ( b0 + ( b1 + ( b2 + b3*c[index] )*c[index] )*c[index] ) * CBBn[index];
			
			muc = muc + omega * ( n[index] + 1. ) * ( log( c[index] / co ) - log( ( 1. - c[index] ) / ( 1. - co ) ) )
					  - .5*n[index] * ( a1 + ( 2.*a2 + 3.*a3*c[index] )*c[index] ) * CAAn[index]
					  - .5*n[index] * ( b1 + ( 2.*b2 + 3.*b3*c[index] )*c[index] ) * CBBn[index]
					  - kappa*cfNL[index];//cahn-hilliard gradient term for energy
            
			avgdens = avgdens + n[index];

			avgconc = avgconc + c[index];
		}
	}
            

	// Tabulate and write avg energy, chemical potential, and density
    if (myid == 0)
	{
		for (isource=1; isource<numprocs; isource++)
		{
            MPI_Recv(&ened,1,MPI_DOUBLE,isource,1,MPI_COMM_WORLD,&status);
            ene = ene+ened;
            MPI_Recv(&mund,1,MPI_DOUBLE,isource,2,MPI_COMM_WORLD,&status);
            mun = mun+mund;
			MPI_Recv(&mucd,1,MPI_DOUBLE,isource,3,MPI_COMM_WORLD,&status);
            muc = muc+mucd;
            MPI_Recv(&avgdensd,1,MPI_DOUBLE,isource,4,MPI_COMM_WORLD,&status);
            avgdens = avgdens+avgdensd;
			MPI_Recv(&avgconcd,1,MPI_DOUBLE,isource,5,MPI_COMM_WORLD,&status);
            avgconc = avgconc+avgconcd;			
		}
		ene = ene*normFac;
        mun  = mun*normFac;
		muc  = muc*normFac;
        avgdens = avgdens*normFac;
		avgconc = avgconc*normFac;
		 
		fprintf(fpene,"%9f %.17g %.17g %.17g %.17g %.17g \n",time*dt,ene,mun,muc,avgdens,avgconc);
	    
	    fclose(fpene);		
	}
    else
	{
        MPI_Send(&ene,1,MPI_DOUBLE,0,1,MPI_COMM_WORLD);
        MPI_Send(&mun,1,MPI_DOUBLE,0,2,MPI_COMM_WORLD);
		MPI_Send(&muc,1,MPI_DOUBLE,0,3,MPI_COMM_WORLD);
        MPI_Send(&avgdens,1,MPI_DOUBLE,0,4,MPI_COMM_WORLD);
		MPI_Send(&avgconc,1,MPI_DOUBLE,0,5,MPI_COMM_WORLD);
	}
	MPI_Barrier(MPI_COMM_WORLD);
}
void timeStep()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;

	double prefactorn;
	double prefactorc;
	
	double k2;

	/************** Step *************/	
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nxn2;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;
			
			k2 = karr[index];
			
			//density
			prefactorn = 1.0/( 1.0 + dt*Mn*k2 );
		
			kn[index] = prefactorn*( kn[index] - dt*Mn*k2*knfNL[index] );

			//concentration
			prefactorc = 1.0/( 1.0 + dt*Mc*k2*k2*kappa );
		
			kc[index] = prefactorc*( kc[index] - dt*Mc*k2*kcfNL[index] );
		}
	}
}
void calcNL()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;

	double x1,x2,rsqd,r;

	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;

			index = i+Nx*j;
			
			do 
			{
				x1 = 2*drand48() - 1.;
				x2 = 2*drand48() - 1.;

				rsqd = x1*x1 + x2*x2;
			} 
			while ( rsqd >= 1.0 );

            r = amp*(sqrt(dt)/dx/dx)*sqrt(-2.0*log(rsqd)/rsqd);


			nfNL[index] = n[index]*n[index]*( -.5*eta + chi*n[index]/3. )
						+ omega * ( c[index]*log( c[index] / co ) + ( 1. - c[index] )*log( ( 1. - c[index] ) / ( 1. - co ) ) )
						- ( a0 + ( a1 + ( a2 + a3*c[index] )*c[index] )*c[index] ) * CAAn[index]
						- ( b0 + ( b1 + ( b2 + b3*c[index] )*c[index] )*c[index] ) * CBBn[index] + r*x1;

			cfNL[index] = omega * ( n[index] + 1. ) * ( log( c[index] / co ) - log( ( 1. - c[index] ) / ( 1. - co ) ) )
						- .5*n[index] * ( a1 + ( 2.*a2 + 3.*a3*c[index] )*c[index] ) * CAAn[index]
						- .5*n[index] * ( b1 + ( 2.*b2 + 3.*b3*c[index] )*c[index] ) * CBBn[index] + r*x2;
		}
	}
}
void calcCorr()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;

	//temporary variables for correlation peak heights
	double kCA0;
	double kCA1,kCA2;
	double C2A;

	double kCB0;
	double kCB1;
	double C2B;

	double rk,k2;
	
	for(j=0;j<local_n0;j++)
	{
		index1 = j*Nxn2;
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;
			
			k2 = karr[index];

			rk = sqrt(k2);

			//**************************** C2A **************************
			kCA0 = HA0*exp( PreCA0 * k2 );
			kCA1 = DWA1*exp( PreCA1 * (rk-qA1)*(rk-qA1) );
			kCA2 = DWA2*exp( PreCA2 * (rk-qA2)*(rk-qA2) );

			//creating correlation overlap
			if( kCA0 > kCA1 )
				C2A = -kCA0;
			else if( kCA1 > kCA2 )
				C2A = kCA1;
			else
				C2A = kCA2;

			kCAAn[index] = C2A*kn[index];

			//**************************** C2B **************************
			kCB0 = HB0*exp( PreCB0 * k2 );
			kCB1 = DWB1*exp( PreCB1 * (rk-qB1)*(rk-qB1) );

			if( kCB0 > kCB1 )
				C2B = -kCB0;
			else
				C2B = kCB1;

			kCBBn[index] = C2B*kn[index];
		}
	}
}
void k2array()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	ptrdiff_t yj;
		
	double kx,ky;
	double gxx,gyy;
	
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		if ( yj < Ny/2 )
			ky = yj*facy;
		else
			ky = (yj-Ny)*facy;

		index1 = j*Nxn2;
		
		for(i=0;i<Nxn2;i++)
		{
			index = i + index1;

			kx = i*facx;

			gxx = .5*( 3. - cos(kx)*cos(ky)  + cos(ky) - 3.*cos(kx) )/dx/dx;
			gyy = .5*( 3. - cos(kx)*cos(ky)  + cos(kx) - 3.*cos(ky) )/dy/dy;

			karr[index] = gxx + gyy ;
		}
	}				
}
void setDebyeWaller(double Sig)
{
	DWA1 = exp(-Sig*Sig / (sigMA1*sigMA1) );
	DWA2 = exp(-Sig*Sig / (sigMA2*sigMA2) );

	DWB1 = exp(-Sig*Sig / (sigMB1*sigMB1) );
}
void setCorrPeakPrefactors()
{
	PreCA0 = -.5/(wA0*wA0);
	PreCA1 = -.5/(wA1*wA1);
	PreCA2 = -.5/(wA2*wA2);

	PreCB0 = -.5/(wB0*wB0);
	PreCB1 = -.5/(wB1*wB1);
}
void initialize()
{
	ptrdiff_t i,j;
	ptrdiff_t index,index1;
	ptrdiff_t yj;
	
	double asq1,asq2;
	double atri;
	double ko = 2.*Pi;

	double fs= 4.*(Pi*64.*64.)/(Nx*Ny); //(100.*Nx)/(Nx*Ny);

	asq1 = 0.23;
	asq2 = 0.14;
	atri = 0.13;

	int ng=4;
	double sepx[ng],sepy[ng],ang[ng];
	double x,y;
	sepx[0]=Nx/4.;		sepy[0]=Ny/4.;
	sepx[1]=Nx/4.;		sepy[1]=3.*Ny/4.;
	sepx[2]=3.*Nx/4.;	sepy[2]=Ny/4.;
	sepx[3]=3.*Nx/4.;	sepy[3]=3.*Ny/4.;
	for(int k=0;k<ng;k++)
	{		
		//generating a random orientation for the seed.
		ang[k] = (2.*(double)rand()/(double)RAND_MAX - 1) * .5*Pi;
									
		//sepx[k] = (double)rand()/(double)RAND_MAX * Nx;
		//sepy[k] = (double)rand()/(double)RAND_MAX * Ny;
	
		//placement of any two grains sufficiently far apart
		/*for (j=0;j<k;j++)
		{
			if ( sqrt( (sepx[j]-sepx[k])*(sepx[j]-sepx[k]) + (sepy[j]-sepy[k])*(sepy[j]-sepy[k]) ) 
				<= 2.*(sqrt( (Nx*Ny)/(ng*Pi) )) )
			{
				k--;
				break;
			}
		}*/			
	}

	//seeding initial crystals
	for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;

			n[index]=0;

			c[index] = cavg;
			//c[index] = ( cavg-cs*fs ) / ( 1.-fs );

			for (int k=0;k<ng;k++)
			{
				if( sqrt( (i-sepx[k])*(i-sepx[k]) + (yj-sepy[k])*(yj-sepy[k]) ) <= 64. )
				{
					x = i*cos(ang[k]) - yj*sin(ang[k]);
					y = i*sin(ang[k]) + yj*cos(ang[k]);

					n[index] = asq1*2.*( cos(ko*y*dx) + cos(ko*x*dx) )	//first mode
							 + asq2*2.*( cos(ko*(x+y)*dx) + cos(ko*(x-y)*dx) );	//second mode

					//c[index] = cs;		
				}
			}
		}
	}
	//slab testing
	/*for(j=0;j<local_n0;j++)
	{
		yj = j + local_0_start;

		index1 = j*Nx2;
		for(i=0;i<Nx;i++)
		{
			index = i + index1;

			n[index]=0;

			c[index] = cavg;

			if( yj > (Ny/2-50) && yj < (Ny/2+50) ) // if( sqrt( (yj-Ny/2)*(yj-Ny/2) + (i-Nx/2)*(i-Nx/2) ) <= 40. )
			{
				//n[index] = .5*(2.*drand48()-1.);
				n[index] = asq1*2.*( cos(ko*yj*dx) + cos(ko*i*dx) )	//first mode
						 + asq2*2.*( cos(ko*(i+yj)*dx) + cos(ko*(i-yj)*dx) + cos(ko*(-i+yj)*dx) + cos(ko*(-i-yj)*dx) );	//second mode

				//triangle
				n[index] = atri*( 2.*cos(kTri*yj*dx) + cos(kTri*(-0.5*yj+0.5*sqrt(3.)*i)*dx) + cos(kTri*(-0.5*yj-0.5*sqrt(3.)*i)*dx) 
							cos(kTri*(0.5*yj+0.5*sqrt(3.)*i)*dx) + cos(kTri*(0.5*yj-0.5*sqrt(3.)*i)*dx) ); //first mode
				

				c[index] = cs;		
			}
			else
			{
				c[index] = ( cavg-cs*fs ) / ( 1.-fs );
			}

		}
	}*/
}
void initialConditions()
{
	if (restartFlag == 0)
	{
		initialize();

		//output initial conditions
		if(myid==0) printf("output 0\n");
		output(0,n,"n",run);
		output(0,c,"c",run);
		output(0,n,"n",run,1);
		output(0,c,"c",run,1);
		
		if(myid==0) printf("initializing domain complete\n");
	}
	else
	{
		if(myid==0) printf("restart flag has been triggered\n");
		if(myid==0) printf("restarting simulation from time step = %d\n",restartTime);

		//read in restart files
		restart(restartTime,n,"n",run);
		restart(restartTime,c,"c",run);
		restart(restartTime,n,"n",run,1);
		restart(restartTime,c,"c",run,1);
		if(myid==0) printf("domain has been reinitialized with restart file\n");
	}
}
void setfftwPlans()
{
	//setting up fftw transforms	
	planF = fftw_mpi_plan_dft_r2c_2d(Ny, Nx, n, reinterpret_cast<fftw_complex*>(kn), MPI_COMM_WORLD, FFTW_PATIENT);
	planB = fftw_mpi_plan_dft_c2r_2d(Ny, Nx, reinterpret_cast<fftw_complex*>(kn), n, MPI_COMM_WORLD, FFTW_PATIENT);
}
void allocateArrays()
{
	/***************** n ************************/
	n = fftw_alloc_real( 2*alloc_local );
	nfNL = fftw_alloc_real( 2*alloc_local );

	kn = new complex<double>[ alloc_local ];
	knfNL = new complex<double>[ alloc_local ];

	/***************** conc ************************/
	c = fftw_alloc_real( 2*alloc_local );
	cfNL = fftw_alloc_real( 2*alloc_local );

	kc = new complex<double>[ alloc_local ];
	kcfNL = new complex<double>[ alloc_local ];

	/***************** C2 ************************/
	CAAn = fftw_alloc_real( 2*alloc_local );
	CBBn = fftw_alloc_real( 2*alloc_local );

	kCAAn = new complex<double>[ alloc_local ];
	kCBBn = new complex<double>[ alloc_local ];

	karr = fftw_alloc_real( alloc_local );
}
void fftwMPIsetup()
{
	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_2d(Ny, Nx/2+1, MPI_COMM_WORLD, &local_n0, &local_0_start);
	
	printf(" %d local_n0=%zu local_0_start=%zu\n",myid,local_n0,local_0_start);
}
void domainParams()
{
	//set up domain parameters
	if(myid==0) printf("dx=%f  numAtomsx=%lf numAtomsy=%lf\n",dx, atomsx,atomsy);

	Nx = (int)( floor(atomsx*spacing/dx) );
	Ny = (int)( floor(atomsy*spacing/dx) );

	Nxn2 = (Nx/2+1);
	Nx2 = 2*Nxn2;

	if(myid==0) printf("Nx = %zu\n",Nx);
	if(myid==0) printf("Ny = %zu\n",Ny);
	
	//set up fourier scaling factors
	facx = 2.*Pi/(Nx);//*dx);
	facy = 2.*Pi/(Ny);//*dx);
	normFac = 1.0/(Nx*Ny);
	
	if(myid==0) printf("facx=%lf\n",facx);
	if(myid==0) printf("facy=%lf\n",facy);
	if(myid==0) printf("normFac=%0.10lf\n",normFac);
}
void inputVariables()
{
	char *line = (char *) malloc (BUFSIZ * sizeof(char));
	FILE *in;
	in = fopen("xpfc2DAlloy.in","rt");
   
	if(myid==0) printf("reading input file\n");
	if (in == NULL)
	{
		if(myid==0) printf("Error opening input file\n");
		if(myid==0) printf("Either file does not exit or incorrect file name\n");
		if(myid==0) printf("Exiting simulation!\n");
		exit(1);	
	}	

	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%s", run);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf", &atomsx,&atomsy);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d", &totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d", &eneFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HA0,&wA0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wA1,&wA2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMA1,&sigMA2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HB0,&wB0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&wB1);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&sigMB1);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf", &iSig,&dSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&cavg,&cs);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&omega,&kappa,&co);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&Mn,&Mc);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&amp);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);

	dy=dx;

	if(myid==0) 
	{
		printf("done reading input file\n");
	
		printf("run=%s\n",run);
		printf("atomsx=%lf	atomsy=%lf\n",atomsx,atomsy);
		printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
		printf("totalTime=%d	printFreq=%d\n",totalTime,printFreq);
		printf("eneFreq=%d\n",eneFreq);
		printf("HA0=%lf	wA0=%lf\n",HA0,wA0);
		printf("wA1=%lf	wA2=%lf\n",wA1,wA2);
		printf("sigMA1=%lf	sigMA2=%lf\n",sigMA1,sigMA2);
		printf("HB0=%lf	wB0=%lf\n",HB0,wB0);
		printf("wB1=%lf\n",wB1);
		printf("sigMB1=%lf\n",sigMB1);
		printf("iSig=%lf	dSig=%lf\n",iSig,dSig);
		printf("cavg=%lf	cs=%lf\n",cavg,cs);
		printf("eta=%lf	chi=%lf\n",eta,chi);
		printf("omega=%lf	kappa=%lf	co=%lf\n",omega,kappa,co);
		printf("Mn=%lf	Mc=%lf\n",Mn,Mc);
 		printf("amp=%lf\n",amp);
		printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);
	}
}
void setPolynomialCoeffs()
{
	//initialize the coefficients for the concentration interpolation functions
	//CAA				//CBB
	a0 = 1.;			b0 = 0;
	a1 = 0;				b1 = 0;
	a2 = -3.;			b2 = 3.;
	a3 = 2.;			b3 = -2.;
}
void setPeakPosition()
{
	//*********element A*************
	qA1 = 2.*Pi;			//first mode
	qA2 = sqrt(2.)*qA1;		//second mode

	//*********element B*************
	qB1 = (2./sqrt(3.))*2.*Pi;	//first mode
}
void start_mpi(int argc, char *argv[])
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);
}
int main(int argc, char *argv[])
{
	int iter;
	
	//initiate mpi daemons
	start_mpi(argc,argv);

	//set up the equil'm position for first and second peaks
	setPeakPosition();

	//set the coefficients for the correlation interpolation functions
	setPolynomialCoeffs();

	//read from file the input values
	inputVariables();

	//set up domain parameters
	domainParams();
	
	//setup the fftw mpi environment
	fftwMPIsetup();

	//allocating relevant arrays
	allocateArrays();
	if(myid==0) printf("arrays have been allocated\n");

	//setting up fftw transform plans
	setfftwPlans();
	if(myid==0) printf("fftw plans have been created and set\n");

	//initialize random number generator
	srand48(time(NULL) + myid);

	//initial conditions
	initialConditions();

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();
	if(myid==0) printf("correlation peak prefactors calculated\n");
	
	//calculate the debye-waller portion of correlations
	setDebyeWaller(iSig);
	if(myid==0) printf("debye-waller factors calculated\n");

	//calculate the k2, i.e., laplacian array, in k-space
	k2array();
	if(myid==0) printf("k2 array calculated\n");

	
	fftw_mpi_execute_dft_r2c(planF,n,reinterpret_cast<fftw_complex*>(kn));		//fourier transform density
	fftw_mpi_execute_dft_r2c(planF,c,reinterpret_cast<fftw_complex*>(kc));		//fourier transform conc

	//calculate the initial correlation kernerl
	calcCorr();
	fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kCAAn),CAAn);		//inverse transform C2n
	normalize(CAAn);								//normalize

	fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kCBBn),CBBn);		//inverse transform C2n
	normalize(CBBn);

	if(myid==0) printf("initial C2 kernel calculated\n");								//normalize		
	
	if( restartFlag == 0 )
		energy(0,run);

	/*********************************************************************************************************/
	//begin interations
	if(myid==0) printf("begin time iteration\n");
	MPI_Barrier(MPI_COMM_WORLD);
	for(iter=restartTime+1; iter<totalTime+restartTime+1; iter++)
	{
		fftw_mpi_execute_dft_r2c(planF,n,reinterpret_cast<fftw_complex*>(kn));		//fourier transform density
		fftw_mpi_execute_dft_r2c(planF,c,reinterpret_cast<fftw_complex*>(kc));		//fourier transform conc

		///non-linear terms
		calcNL();					//calculate non-linear terms for density
		
		fftw_mpi_execute_dft_r2c(planF,nfNL,reinterpret_cast<fftw_complex*>(knfNL));	//foruier transform density non-linear terms		
		fftw_mpi_execute_dft_r2c(planF,cfNL,reinterpret_cast<fftw_complex*>(kcfNL));	//foruier transform conc non-linear terms

		//time stepping
		timeStep();									//update fields in k-space		

		//calculate correlation kernerl
		iSig = iSig - dSig;
		setDebyeWaller(iSig);
		calcCorr();									//calculate correlations
		fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kCAAn),CAAn);	//inverse transform C2n
		normalize(CAAn);							//normalize

		fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kCBBn),CBBn);	//inverse transform C2n
		normalize(CBBn);							//normalize
	
		fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kn),n);		//inverse transform density
		normalize(n);								//normalize 
		fftw_mpi_execute_dft_c2r(planB,reinterpret_cast<fftw_complex*>(kc),c);		//inverse transform conc
		normalize(c);								//normalize 		

		//output current data
		if( (iter)%(printFreq) == 0 )
		{
			if(myid==0) printf("time = %d\n",iter);
			
			//output fields
			output(iter,n,"n",run,1);
			output(iter,c,"c",run,1);
		}

		if( (iter)%eneFreq == 0 )
			energy(iter,run);
	}
	
	//free meory and destroy fftw plans
	freeMemory();

	//exit mpi
	MPI_Finalize();

	return 0;
}