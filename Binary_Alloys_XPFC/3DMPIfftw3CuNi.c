#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <complex.h>
#include <fftw3-mpi.h>

#define Pi (2.*acos(0.))

//domain parameters
int atomsx,atomsy,atomsz;	//number of atoms in x,y and z
ptrdiff_t Nx,Ny,Nz;			//number of grid points in x,y and z
ptrdiff_t Nx2;
double spacing;				//lattice spacing normalized to 1
double dx;					//numerical grid spacing
int maxnum;					//maximum number of peaks per processor

//time parameters
int totalTime, printFreq;		//simulation time and print frequency
int peakFreq;		//how often to calculate density peaks
double dt;						//numerical time step
int restartFlag,restartTime;	//flag for restarting (set to 1) and time

//MPI parameters
int argc;
char **argv;
int myid,numprocs;
ptrdiff_t alloc_local, local_n0, local_0_start;

/**********************correlation variables for Cu****************************/
//k-zero mode
double HCu0;	//height/depth of k-zero mode Cu
double wCu0;	//width of k-zero mode Cu
double PreCCu0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kCu1,kCu2;	//k-space peak positions for first and second mode for Cu
double wCu1,wCu2;	//widths of the first and second mode peaks )sets elasticity, anisotropy, interface widths etc)
double DWCu1,DWCu2;	//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCCu1,PreCCu2;	//prefactors based on the widths of the correlation peaks

double sigMCu1,sigMCu2;	//sets the effective transition temperatures for the corresponding mode of the correlation

/**********************correlation variables for Ni***************************/
//k-zero mode
double HNi0;	//height/depth of k-zero mode Ni
double wNi0;	//width of k-zero mode Ni
double PreCNi0;	// prefactor based on the widths of the k-zero mode

//first and second modes
double kNi1,kNi2;		//k-space peak positions for first and second mode for Ni
double wNi1,wNi2;		//widths of the first and second mode peaks (sets elasticity, anisotropy, interface widths etc)
double DWNi1,DWNi2;		//debye-waller factor to modulate peaks as a function of temperature (sigma)
double PreCNi1,PreCNi2;	//prefactors based on the widths of the correlation peaks

double sigMNi1,sigMNi2;	//sets the effective melting points for the corresponding mode of the correlation

//*************initial condition and simulation variables
double iSig;		//initial temperature (note, can be expanded with an iteration count to do cooling)
double cavg;		//average concentration
double cs,cl;		//equilibrium solidus and liquidus concentration for temperature iSig

double eta,chi;		//coefficients for cubic and quartic term in ideal energy
double Mn;			//mobility constant for density

double co;			//reference liquid composition
double Omega,alpha;	//entropy of mixing coefficient and gradient energy coefficient concentration
double Mc;			//mobility for concentration

//note these are hard coded into the simulation
double a0,a1,a2,a3;	//polynomial coefficients for concentration -- correlation CCu
double b0,b1,b2,b3;	//polynomial coefficients for concentration -- correlation CNi

double facx,facy,facz,normFac;	//fourier factors for scaling lengths in k-space

//*******************Arrays for fields
//real space arrays
double *n,*c;			//density and concentration
double *nfNL,*cfNL;		//non-linear terms for density and concentration
double *CCun,*CNin;		//real space arrays for correlation convoluted with n

//complex arrays
fftw_complex *kn,*kc;			//k-space density and concentration
fftw_complex *knfNL,*kcfNL;		//k-space non-linear terms
fftw_complex *kCCun,*kCNin;		//k-space convolution of correlation and density

//fftw plans
fftw_plan planF, planB;			//forward and backward plans

void freeMemory()
{	
	free(n);
	free(c);
	free(nfNL);
	free(cfNL);
	free(CCun);
	free(CNin);

	fftw_free(kn);
	fftw_free(kc);
	fftw_free(knfNL);
	fftw_free(kcfNL);
	fftw_free(kCCun);
	fftw_free(kCNin);

	fftw_destroy_plan(planF);
	fftw_destroy_plan(planB);	
}
void restart(int time,double *Array,char *filepre)
{
	double tmp[Nx*Ny*Nz];
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
    fp = fopen(filename,"rt");

    ptrdiff_t i,j,k;	
	ptrdiff_t kg;	//global index for y direction

	MPI_Barrier(MPI_COMM_WORLD);
	
	for(k=0;k<Nz;k++)
		for(j=0;j<Ny;j++)
			for(i=0;i<Nx;i++)
				fscanf(fp,"%lf",&tmp[i+j*Nx+k*Nx*Ny]);

    fclose(fp);

	MPI_Barrier(MPI_COMM_WORLD);

	for(k=0;k<Nz;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				kg = k - myid*local_n0;	//rescale coordinates to that of local processor
				if (kg >= 0 && kg <= local_n0-1)
					Array[i+Nx2*(j+kg*Ny)] = tmp[i+Nx*(j+k*Ny)];
			}
		}
	}	
	MPI_Barrier(MPI_COMM_WORLD);
}
void output(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);

	ptrdiff_t i,j,k;
	int kk;

	MPI_Barrier(MPI_COMM_WORLD);

	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to open file for appending\n");
					exit(1);
				}
			}
   
			for(k=0;k<local_n0;k++)
				for(j=0;j<Ny;j++)
					for(i=0;i<Nx;i++)
						fprintf(fp,"%f  \n",Array[i+Nx2*(j+k*Ny)]);
					
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void outputSlice(int time,double *Array,char *filepre)
{
	char filename[BUFSIZ];
    FILE *fp;
    sprintf(filename,"%s%d.dat",filepre,time);
	fp = fopen(filename,"w");

	ptrdiff_t i,j,k;
	int kk;

	MPI_Barrier(MPI_COMM_WORLD);

	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					exit(1);
				}
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to open file for appending\n");
					exit(1);
				}
			}
   
			i=Nx/2;
			for(k=0;k<local_n0;k++)
				for(j=0;j<Ny;j++)
						fprintf(fp,"%f  \n",Array[i+Nx2*(j+k*Ny)]);
					
			fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}
void normalize(double *Array)
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;

	//normalize the transform
	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i + Nx2*(j+k*Ny);
				Array[index] *= normFac;
			}
		}
	}
}
void timeStepc()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;
	double kx,ky,kz,k2;
	double prefactor_c;

	for(k=0;k<local_n0;k++)
	{
		zk = k + local_0_start;

		if ( zk < Nz/2 )
			kz = zk*facz;
		else
			kz = (zk-Nz)*facz;

		for(j=0;j<Ny;j++)
		{
			if ( j < Ny/2 )
				ky = j*facy;
			else
				ky = (j-Ny)*facy;

			for(i=0;i<(Nx/2+1);i++)
			{
				index = i + (Nx/2+1)*(j+k*Ny);
				kx = i*facx;
				k2 = kx*kx + ky*ky + kz*kz;
				
				prefactor_c = 1./( 1. + dt*Mc*k2*k2*alpha );

				kc[index] = prefactor_c * ( kc[index] - dt*Mc*k2*kcfNL[index] );
			}
		}
	}
}
void timeStepn()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;
	double kx,ky,kz,k2;
	double prefactor_n;

	for(k=0;k<local_n0;k++)
	{
		zk = k + local_0_start;

		if ( zk < Nz/2 )
			kz = zk*facz;
		else
			kz = (zk-Nz)*facz;

		for(j=0;j<Ny;j++)
		{
			if ( j < Ny/2 )
				ky = j*facy;
			else
				ky = (j-Ny)*facy;

			for(i=0;i<(Nx/2+1);i++)
			{
				index = i + (Nx/2+1)*(j+k*Ny);
				kx = i*facx;
				k2 = kx*kx + ky*ky + kz*kz;

				prefactor_n = 1./( 1. + dt*Mn*k2 );

				kn[index] = prefactor_n * ( kn[index] - dt*Mn*k2*knfNL[index] );
			}
		}
	}
}
void calcCeffc()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;

	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i + Nx2*(j+k*Ny);

				cfNL[index] = Omega*( n[index] + 1. ) * ( log(c[index]/co) - log( (1.-c[index]) / (1.-co) ) );
							- 0.5*n[index] * ( a1 + ( 2.*a2 + 3.*a3*c[index] ) * c[index] ) * CCun[index]
							- 0.5*n[index] * ( b1 + ( 2.*b2 + 3.*b3*c[index] ) * c[index] ) * CNin[index] ; 
					
			}
		}
	}

	fftw_mpi_execute_dft_r2c(planF,cfNL,kcfNL);
}
void calcCeffn()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;

	for(k=0;k<local_n0;k++)
	{
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i + Nx2*(j+k*Ny);

				nfNL[index] = n[index]*n[index]*( -0.5*eta + chi*n[index]/3. ) 
							+ Omega* ( c[index] * log( c[index] / co ) + (1.-c[index]) * log( (1.-c[index]) / (1.-co) ) );
							- ( a0 + ( a1 + ( a2 + a3*c[index] ) * c[index] ) * c[index] ) * CCun[index]
							- ( b0 + ( b1 + ( b2 + b3*c[index] ) * c[index] ) * c[index] ) * CNin[index];
					
			}
		}
	}

	fftw_mpi_execute_dft_r2c(planF,nfNL,knfNL);
}
void calcCorrelations()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;
	double kx,ky,kz,k2,rk;

	//temporary variables for the peaks
	double kCCu0,kCNi0;
	double kCCu1,kCNi1;
	double kCCu2,kCNi2;
	double kCCu,kCNi;

	for(k=0;k<local_n0;k++)
	{
		zk = k + local_0_start;

		if ( zk < Nz/2 )
			kz = zk*facz;
		else
			kz = (zk-Nz)*facz;

		for(j=0;j<Ny;j++)
		{
			if ( j < Ny/2 )
				ky = j*facy;
			else
				ky = (j-Ny)*facy;

			for(i=0;i<(Nx/2+1);i++)
			{
				index = i + (Nx/2+1)*(j+k*Ny);
				
				kx = i*facx;
				k2 = kx*kx + ky*ky + kz*kz;
				rk = sqrt(k2);

				//****************************CCu**************************//
				kCCu0 = exp(PreCCu0*k2);
				kCCu1 = exp(PreCCu1* (rk-kCu1)*(rk-kCu1) );
				kCCu2 = exp(PreCCu2* (rk-kCu2)*(rk-kCu2) );				

				//creating the correlation overlap
				if (HCu0*kCCu0 > DWCu1*kCCu1)
					kCCu = -HCu0*kCCu0;
				else if (DWCu1*kCCu1 > DWCu2*kCCu2)
					kCCu = DWCu1*kCCu1;
				else
					kCCu = DWCu2*kCCu2;

				kCCun[index] = kn[index]*kCCu;


				//****************************CNi**************************//
				kCNi0 = exp(PreCNi0*k2);
				kCNi1 = exp(PreCNi1* (rk-kNi1)*(rk-kNi1) );
				kCNi2 = exp(PreCNi2* (rk-kNi2)*(rk-kNi2) );

				//creating the correlation overlap
				if (HNi0*kCNi0 > DWNi1*kCNi1)
					kCNi = -HNi0*kCNi0;
				else if (DWNi1*kCNi1 > DWNi2*kCNi2)
					kCNi = DWNi1*kCNi1;
				else
					kCNi = DWNi2*kCNi2;

				kCNin[index] = kn[index]*kCNi;
			}
		}
	}	
}
void setDebyeWaller(double Sig)
{
	DWCu1 = exp(-Sig*Sig/(sigMCu1*sigMCu1) );
	DWCu2 = exp(-Sig*Sig/(sigMCu2*sigMCu2) );
	
	DWNi1 = exp(-Sig*Sig/(sigMNi1*sigMNi1) );
	DWNi2 = exp(-Sig*Sig/(sigMNi2*sigMNi2) );
}
void setCorrPeakPrefactors()
{
	PreCCu0 = -0.5/(wCu0*wCu0);
	PreCCu1 = -0.5/(wCu1*wCu1);
	PreCCu2 = -0.5/(wCu2*wCu2);

	PreCNi0 = -0.5/(wNi0*wNi0);
	PreCNi1 = -0.5/(wNi1*wNi1);
	PreCNi2 = -0.5/(wNi2*wNi2);
}
void initialize()
{
	ptrdiff_t i,j,k;
	ptrdiff_t index;
	ptrdiff_t zk;
	
	double ampCu1,ampCu2;	
	double ko = 2.*Pi;

	double fs= Pi*20.*20.*20/(Nx*Ny*Nz);

	ampCu1 = 0.1;
	ampCu2 = 0.1;

	for(k=0;k<local_n0;k++)
	{
		zk = k + local_0_start;
		for(j=0;j<Ny;j++)
		{
			for(i=0;i<Nx;i++)
			{
				index = i+Nx2*(j+k*Ny);

				n[index]=0;

				if( sqrt( (zk-Nz/2)*(zk-Nz/2) + (j-Ny/2)*(j-Ny/2) + (i-Nx/2)*(i-Nx/2) ) <= 20. )
				{
					//n[index] = .5*(2.*drand48()-1.);
					n[index] = ampCu1*( 2*cos(ko*(i+j+zk)*dx) + 2*cos(ko*(i-j+zk)*dx) + 2*cos(ko*(i+j-zk)*dx) + 2*cos(ko*(i-j-zk)*dx) + 
							2*cos(ko*(-i+j+zk)*dx) + 2*cos(ko*(-i-j+zk)*dx) + 2*cos(ko*(-i+j-zk)*dx) + 2*cos(ko*(-i-j-zk)*dx) )	//first mode
							+ ampCu2*( 2*cos(2*ko*i*dx) + 2*cos(2*ko*j*dx) + 2*cos(2*ko*zk*dx) ); //second mode

					c[index] = cs;
					
				}
				else
				{
					c[index] = ( cavg-cs*fs ) / ( 1.-fs );
				}

			}
		}
	}
}
void setfftwPlans()
{
	/************** n **************/
	planF = fftw_mpi_plan_dft_r2c_3d(Nz, Ny, Nx, n, kn, MPI_COMM_WORLD, FFTW_MEASURE);
	planB = fftw_mpi_plan_dft_c2r_3d(Nz, Ny, Nx, kn, n, MPI_COMM_WORLD, FFTW_MEASURE);
}
void allocateArrays()
{
	/****************** n ******************/
	n = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	nfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kn = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	knfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** c ******************/
	c = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	cfNL = (double *) fftw_malloc( sizeof (double)*2*alloc_local );

	kc = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kcfNL = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );

	/****************** C2 ******************/
	CCun = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	CNin = (double *) fftw_malloc( sizeof (double)*2*alloc_local );
	
	kCCun = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
	kCNin = (fftw_complex *) fftw_malloc( sizeof (fftw_complex)*alloc_local );
}
void fftwMPIsetup()
{
	//starting mpi daemons
	MPI_Init(&argc,&argv); 
	MPI_Comm_rank(MPI_COMM_WORLD,&myid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

	printf("\n myid: %d of numprocs: %d processors has been activated \n",myid,numprocs);

	fftw_mpi_init();
	
	//get the local (for current cpu) array sizes
	alloc_local = fftw_mpi_local_size_3d(Nz, Ny, Nx/2+1, MPI_COMM_WORLD,&local_n0, &local_0_start);
}
void domainParams()
{
	//from the lattice spacing and grid spacing, compute the number of grid points
	if(myid==0) printf("dx=%f  numAtomsx=%d numAtomsy=%d numAtomsz=%d\n",dx, atomsx,atomsy,atomsz);
	
	Nx = (int)(floor(spacing/dx*atomsx));
	Ny = (int)(floor(spacing/dx*atomsy));
	Nz = (int)(floor(spacing/dx*atomsz));

	if(myid==0) printf("number of grid points in x %d\n",Nx);
	if(myid==0) printf("number of grid points in y %d\n",Ny);
	if(myid==0) printf("number of grid points in z %d\n",Nz);
	
	Nx2 = 2*(Nx/2+1);

	//set up fourier scaling factors
	facx = 2.*Pi/(Nx*dx);
	facy = 2.*Pi/(Ny*dx);
	facz = 2.*Pi/(Nz*dx);
	normFac = 1.0/(Nx*Ny*Nz);

	if(myid==0) printf("facx=%lf\n",facx);
	if(myid==0) printf("facy=%lf\n",facy);
	if(myid==0) printf("facz=%lf\n",facz);
	if(myid==0) printf("normFac=%0.10lf\n",normFac);

	/*maxnum = 8*Nx*dx*Ny*dy*Nz*dz/numprocs;
	if(myid==0) printf("maxnum=%d\n",maxnum);*/
}
void inputVariables()
{
	char *line = malloc(BUFSIZ);
	FILE *in;
	in = fopen("input3DCuNiMPI","rt");	
   
	if(myid==0) printf("reading input file\n");
	if (in == NULL)
	{
		if(myid==0) printf("Either wrong file name or file does not exist\n");
		if(myid==0) printf("Exiting simulation!\n");
	}
	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d %d", &atomsx,&atomsy,&atomsz);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&spacing,&dx,&dt);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&totalTime,&printFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d",&peakFreq);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HCu0,&wCu0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wCu1,&wCu2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMCu1,&sigMCu2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&HNi0,&wNi0);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&wNi1,&wNi2);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&sigMNi1,&sigMNi2);	
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&iSig);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf",&cavg);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&cs,&cl);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&eta,&chi);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf %lf",&alpha,&co,&Omega);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%lf %lf",&Mn,&Mc);
	line = fgets(line, BUFSIZ, in);
	sscanf (line, "%d %d",&restartFlag,&restartTime);
	fclose(in);

	if(myid==0)
	{
		printf("done reading input file\n");
	
		printf("numAtomsx=%d	numAtomsy=%d	numAtomsz=%d\n",atomsx,atomsy,atomsz);
		printf("spacing=%lf	dx=%lf	dt=%lf\n",spacing,dx,dt);
		printf("totalTime=%d	printFreq=%d	peakFreq=%d\n",totalTime,printFreq,peakFreq);
		printf("HCu0=%lf	wCu0=%lf\n",HCu0,wCu0);
		printf("wCu1=%lf	wCu2=%lf\n",wCu1,wCu2);
		printf("sigMCu1=%lf	sigMCu2=%lf\n",sigMCu1,sigMCu2);
		printf("HNi0=%lf	wNi0=%lf\n",HNi0,wNi0);
		printf("wNi1=%lf	wNi2=%lf\n",wNi1,wNi2);
		printf("sigMNi1=%lf	sigMNi2=%lf\n",sigMNi1,sigMNi2);
		printf("iSig=%lf\n",iSig);
		printf("cavg=%lf	cs=%lf	cl=%lf\n",cavg,cs,cl);
		printf("eta=%lf	chi=%lf\n",eta,chi);
		printf("alpha=%lf	co=%lf	Omega=%lf\n",alpha,co,Omega);
		printf("Mn=%lf	Mc=%lf\n",Mn,Mc);
		printf("rFlag=%d	rTime=%d\n",restartFlag,restartTime);
	}	
}
void setPolynomialCoeffs()
{
	//initialize the coefficients for the concentration dependence correlation prefactor
	//these are polynomials to third order
	//CCu				//CNi
	a0 = 1.;			b0 = 0;
	a1 = 0;				b1 = 0;
	a2 = -3.;			b2 = 3.;
	a3 = 2.;			b3 = -2.;
}
void setPeakPositions()
{
	kCu1 = sqrt(3)*2.*Pi;
	kCu2 = 4.*Pi;

	kNi1 = (62./63.)*sqrt(3)*2.*Pi;
	kNi2 = (62./63.)*4.*Pi;
}
void getpeaks(int time,char *filepre)
{
    double r0;
    float fi,fj,fk;
    int npeaks,pkover,kp,km,jp,jm,ip,im;
    int index,index1,index2,k2,iup,idn;
    int indexip,indexim;
	int npk,npkoff,npeaksd,npeaks0,npkarr[numprocs],i,j,k,kk;
	
	char filename[BUFSIZ];
    FILE *fp;
    MPI_Status status;

	double *fdn;			//getpeaks dn arr
	int *pkpos;				//getpeaks pkpos arr
	fdn = (double *) malloc( sizeof (double)*(Nx*Ny) );
	pkpos = (int *) malloc( sizeof (int)*(maxnum*3) );

	iup = myid+1;
	idn = myid-1;

	//for periodicity
	if (myid == numprocs-1) 
		iup = 0;
	if (myid == 0) 
		idn = numprocs-1;

    r0 = 0.;
    npeaks = 0;
    pkover = 0;
	for(k=1;k<local_n0-1;k++)
	{
		index1 = Nx2*k*Ny;
        k2 = k+local_0_start;
        kp = k+1;
        km = k-1;
		for(j=0;j<Ny;j++)
		{
			index2 = index1 + Nx2*j;
            jp = j+1;
            jm = j-1;
			//periodicity
            if ( j == 0 ) 
				jm = Ny-1;
            if ( j == Ny-1) 
				jp = 0;
			for(i=0;i<Nx;i++)
			{
			    index = i + index2;
                if (n[index] > r0)
			    {
              		ip = i+1;
              		im = i-1;
					//periodicity
              		if ( i == 0 ) 
						im = Nx-1;
              		if ( i == Nx-1) 
						ip = 0;

			    	indexip = ip + index2;
			    	indexim = im + index2;
                    
					if( (n[index] > n[indexip]) && (n[index] > n[indexim]) && (n[index] > n[i+Nx2*(jp+Ny*k)]) && (n[index] > n[i+Nx2*(jm+Ny*k)]) && (n[index] > n[i+Nx2*(j+Ny*kp)]) && (n[index] > n[i+Nx2*(j+Ny*km)]) && (n[index] > n[ip+Nx2*(jp+Ny*k)]) && (n[index] > n[ip+Nx2*(jm+Ny*k)]) && (n[index] > n[im+Nx2*(jp+Ny*k)]) && (n[index] > n[im+Nx2*(jm+Ny*k)]) && (n[index] > n[ip+Nx2*(j+Ny*km)]) && (n[index] > n[im+Nx2*(j+Ny*km)]) && (n[index] > n[i+Nx2*(jp+Ny*km)]) && (n[index] > n[i+Nx2*(jm+Ny*km)]) && (n[index] > n[ip+Nx2*(jp+Ny*km)]) && (n[index] > n[ip+Nx2*(jm+Ny*km)]) && (n[index] > n[im+Nx2*(jp+Ny*km)]) && (n[index] > n[im+Nx2*(jm+Ny*km)]) && (n[index] > n[ip+Nx2*(j+Ny*kp)]) && (n[index] > n[im+Nx2*(j+Ny*kp)]) && (n[index] > n[i+Nx2*(jp+Ny*kp)]) && (n[index] > n[i+Nx2*(jm+Ny*kp)]) && (n[index] > n[ip+Nx2*(jp+Ny*kp)]) && (n[index] > n[ip+Nx2*(jm+Ny*kp)]) && (n[index] > n[im+Nx2*(jp+Ny*kp)]) && (n[index] > n[im+Nx2*(jm+Ny*kp)]) )
	             	{
                        if (npeaks < maxnum)
	             		{
                            npeaks = npeaks+1;
                            pkpos[3*(npeaks-1)]   = i;
                            pkpos[3*(npeaks-1)+1] = j;
                            pkpos[3*(npeaks-1)+2] = k2;
	             		}
                        else pkover = 1;
	             	}
				}
			}
		}
	}

	index1 = Nx2*(local_n0-1)*Ny;
	for(j=0;j<Ny;j++)
	{
		index2 = index1 + Nx2*j;
		for(i=0;i<Nx;i++)
		{
			index = i + index2;
        	MPI_Send(&n[index],1,MPI_DOUBLE,iup,i+j*Nx,MPI_COMM_WORLD);
        	MPI_Recv(&fdn[i+j*Nx],1,MPI_DOUBLE,idn,i+j*Nx,MPI_COMM_WORLD,&status);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);

	k=0;
    k2 = k+local_0_start;
    kp = k+1;
	for(j=0;j<Ny;j++)
	{
		index2 = Nx2*j;
        jp = j+1;
        jm = j-1;
        if ( j == 0 ) jm = Ny-1;
        if ( j == Ny-1) jp = 0;
		for(i=0;i<Nx;i++)
		{
			index = i + index2;
            if (n[index] > r0)
			{
              	ip = i+1;
              	im = i-1;
              	if ( i == 0 ) im = Nx-1;
              	if ( i == Nx-1) ip = 0;
			    indexip = ip + index2;
			    indexim = im + index2;
                if( (n[index] > n[indexip]) && (n[index] > n[indexim]) && (n[index] > n[i+Nx2*(jp+Ny*k)]) && (n[index] > n[i+Nx2*(jm+Ny*k)]) && (n[index] > n[i+Nx2*(j+Ny*kp)]) && (n[index] > fdn[i+Nx*j]) && (n[index] > n[ip+Nx2*(jp+Ny*k)]) && (n[index] > n[ip+Nx2*(jm+Ny*k)]) && (n[index] > n[im+Nx2*(jp+Ny*k)]) && (n[index] > n[im+Nx2*(jm+Ny*k)]) && (n[index] > fdn[ip+Nx*j]) && (n[index] > fdn[im+Nx*j]) && (n[index] > fdn[i+Nx*jp]) && (n[index] > fdn[i+Nx*jm]) && (n[index] > fdn[ip+Nx*jp]) && (n[index] > fdn[ip+Nx*jm]) && (n[index] > fdn[im+Nx*jp]) && (n[index] > fdn[im+Nx*jm]) && (n[index] > n[ip+Nx2*(j+Ny*kp)]) && (n[index] > n[im+Nx2*(j+Ny*kp)]) && (n[index] > n[i+Nx2*(jp+Ny*kp)]) && (n[index] > n[i+Nx2*(jm+Ny*kp)]) && (n[index] > n[ip+Nx2*(jp+Ny*kp)]) && (n[index] > n[ip+Nx2*(jm+Ny*kp)]) && (n[index] > n[im+Nx2*(jp+Ny*kp)]) && (n[index] > n[im+Nx2*(jm+Ny*kp)]) )
	            {
                    if (npeaks < maxnum)
	             	{
                        npeaks = npeaks+1;
                        pkpos[3*(npeaks-1)]   = i;
                        pkpos[3*(npeaks-1)+1] = j;
                        pkpos[3*(npeaks-1)+2] = k2;
	             	}
                    else pkover = 1;
	            }
			}
		}
	}

	for(j=0;j<Ny;j++)
	{
		index2 = Nx2*j;
		for(i=0;i<Nx;i++)
		{
			index = i + index2;
        	MPI_Send(&n[index],1,MPI_DOUBLE,idn,i+j*Nx,MPI_COMM_WORLD);
        	MPI_Recv(&fdn[i+j*Nx],1,MPI_DOUBLE,iup,i+j*Nx,MPI_COMM_WORLD,&status);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);

	k=local_n0-1;
    k2 = k+local_0_start;
    km = k-1;
	index1 = Nx2*k*Ny;
	for(j=0;j<Ny;j++)
	{
		index2 = index1 + Nx2*j;
        jp = j+1;
        jm = j-1;
        if ( j == 0 ) 
			jm = Ny-1;
        if ( j == Ny-1) 
			jp = 0;
		for(i=0;i<Nx;i++)
		{
			index = i + index2;
            if (n[index] > r0)
			{
              	ip = i+1;
              	im = i-1;
              	if ( i == 0 ) 
					im = Nx-1;
              	if ( i == Nx-1) 
					ip = 0;
			    
				indexip = ip + index2;
			    indexim = im + index2;

                if( (n[index] > n[indexip]) && (n[index] > n[indexim]) && (n[index] > n[i+Nx2*(jp+Ny*k)]) && (n[index] > n[i+Nx2*(jm+Ny*k)]) && (n[index] > fdn[i+Nx*j]) && (n[index] > n[i+Nx2*(j+Ny*km)]) && (n[index] > n[ip+Nx2*(jp+Ny*k)]) && (n[index] > n[ip+Nx2*(jm+Ny*k)]) && (n[index] > n[im+Nx2*(jp+Ny*k)]) && (n[index] > n[im+Nx2*(jm+Ny*k)]) && (n[index] > n[ip+Nx2*(j+Ny*km)]) && (n[index] > n[im+Nx2*(j+Ny*km)]) && (n[index] > n[i+Nx2*(jp+Ny*km)]) && (n[index] > n[i+Nx2*(jm+Ny*km)]) && (n[index] > n[ip+Nx2*(jp+Ny*km)]) && (n[index] > n[ip+Nx2*(jm+Ny*km)]) && (n[index] > n[im+Nx2*(jp+Ny*km)]) && (n[index] > n[im+Nx2*(jm+Ny*km)]) && (n[index] > fdn[ip+Nx*j]) && (n[index] > fdn[im+Nx*j]) && (n[index] > fdn[i+Nx*jp]) && (n[index] > fdn[i+Nx*jm]) && (n[index] > fdn[ip+Nx*jp]) && (n[index] > fdn[ip+Nx*jm]) && (n[index] > fdn[im+Nx*jp]) && (n[index] > fdn[im+Nx*jm]) )
	            {
                    if (npeaks < maxnum)
	             	{
                        npeaks = npeaks+1;
                        pkpos[3*(npeaks-1)]   = i;
                        pkpos[3*(npeaks-1)+1] = j;
                        pkpos[3*(npeaks-1)+2] = k2;
	             	}
                    else pkover = 1;
	            }
			}
		}
	}

    if (pkover == 1) printf("%d  WARNING: found more peaks than allocated for\n",myid);

    npeaks0 = npeaks;
    if (myid == 0)
	{
        npkarr[0] = npeaks0;
 		for(i=1;i<numprocs;i++)
		{
			MPI_Recv(&npeaksd,1,MPI_INT,i,1,MPI_COMM_WORLD,&status);
            npeaks = npeaks+npeaksd;
            npkarr[i] = npeaksd;
		}
        printf("Total pks found: %d\n",npeaks);
	}
    else
	{
        MPI_Send(&npeaks,1,MPI_INT,0,1,MPI_COMM_WORLD);
	}
	MPI_Barrier(MPI_COMM_WORLD);

    if (myid == 0)
	{
 		for(i=1;i<numprocs;i++)
		{
			MPI_Send(&npkarr,numprocs,MPI_INT,i,1,MPI_COMM_WORLD);
		}
	}
    else
	{
        MPI_Recv(&npkarr,numprocs,MPI_INT,0,1,MPI_COMM_WORLD,&status);
	}
	MPI_Barrier(MPI_COMM_WORLD);

    sprintf(filename,"%s.pkpos.%d",filepre,time);
	MPI_Barrier(MPI_COMM_WORLD);
	for (kk=0;kk<numprocs;kk++)
	{
		if (myid == kk)
		{
			if (myid==0)
			{
				fp = fopen(filename,"w");
				if(fp==NULL)
				{
					printf("Unable to open file for writing\n");
					exit(1);
				}
				fprintf(fp,"ITEM: TIMESTEP\n");
				fprintf(fp,"%d\n",time);
				fprintf(fp,"ITEM: NUMBER OF ATOMS\n");
				fprintf(fp,"%d\n",npeaks);
				fprintf(fp,"ITEM: BOX BOUNDS\n");
				fprintf(fp,"%.1f %.1f\n",-0.5,Nx-.5);
				fprintf(fp,"%.1f %.1f\n",-0.5,Ny-.5);
				fprintf(fp,"%.1f %.1f\n",-0.5,Nz-.5);
				fprintf(fp,"ITEM: ATOMS id type x y z\n");
			}
			else
			{
				fp = fopen(filename,"a");
				if(fp==NULL)
				{
					printf("Unable to open file for appending\n");
					exit(1);
				}
			}
            npkoff = 0;
 	   		for(i=0;i<myid;i++)
	   		{
				npkoff = npkoff + npkarr[i];
	  		}
 	   		for(npk=1;npk<npeaks0+1;npk++)
	   		{
				fi = pkpos[3*(npk-1)];
				fj = pkpos[3*(npk-1)+1];
				fk = pkpos[3*(npk-1)+2];
				fprintf(fp,"%9d %5d %.1f %.1f %.1f\n",npk+npkoff,1,fi,fj,fk);
	  		}
	        fclose(fp);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}

	free(fdn);
	free(pkpos);
}
int main(int argc, char* argv[])
{
	int t;	

	//set the coefficients for the correlation interpolation functions
	setPolynomialCoeffs();

	//read in the parameter file
	inputVariables();

	//set up domain parameters
	domainParams();

	//setup the fftw mpi environment
	fftwMPIsetup();

	//allocate real and k-space arrays
	allocateArrays();

	MPI_Barrier(MPI_COMM_WORLD);

	//set up fftw plans
	setfftwPlans();

	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Barrier(MPI_COMM_WORLD);
    maxnum = 8*Nx*dx*Ny*dy*Nz*dz/numprocs;
	if(myid==0) printf("maxnum=%d\n",maxnum);

	srand48(time(NULL)+myid);
	//determine if this is a restart or initialization
	if (restartFlag == 0)
	{
		initialize();

		if(myid==0) printf("output 0\n");
		
		output(0,n,"dens");
		output(0,c,"conc");
		//outputSlice(0,n,"dSlice");
		//outputSlice(0,c,"cSlice");
	}
	else
	{
		if(myid==0) printf("restart flag has been triggered\n");
		if(myid==0) printf("restart from time iteration %d\n",restartTime);
		restart(restartTime,n,"dens");
		restart(restartTime,c,"conc");
	}

	//setting the prefactors for the correlation based on the widths
	setCorrPeakPrefactors();

	MPI_Barrier(MPI_COMM_WORLD);

	//setting up the initial debye waller prefactors based on the initial temperature
	setDebyeWaller(iSig);

	//set the corresponding peak positions
	setPeakPositions();
	
	MPI_Barrier(MPI_COMM_WORLD);
	/*****************************************************************************************************************************/
	/*****************************************************************************************************************************/
	if(myid==0) printf("beginning iterations\n");
	for(t=restartTime+1;t<totalTime;t++)
	{
		fftw_mpi_execute_dft_r2c(planF,n,kn);	//forward transform density
		fftw_mpi_execute_dft_r2c(planF,c,kc);	//forward transform concentration

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point	

		/****** calculate correlatiions, inverse transform and normalize *******/
		calcCorrelations();				//caluclation the convolution of the correlation and density in kspace

		fftw_mpi_execute_dft_c2r(planB,kCCun,CCun);	//back transform Cu correlation after convolution with density		
		normalize(CCun);				//normalize the transform

		fftw_mpi_execute_dft_c2r(planB,kCNin,CNin);	//back transform Ni correlation after convolution with density
		normalize(CNin);					//normalize the transform

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		/****** non-linear and effective correlations *******/
		calcCeffn();					//calculate non-linear terms and effective correlation (variation with density)
		calcCeffc();					//calculate non-linear terms and effective correlation (variation with c)
		
		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point
		
		/****Note****/ //some improved stability maybe gain by solving for density first, and then concentration
		
		/****** time step fields *******/
		timeStepn();
		timeStepc();

		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		/****** inverse transform and normalize field arrays *******/
		fftw_mpi_execute_dft_c2r(planB,kn,n);	//inverse transform density
		normalize(n);							//normalize density after inverse transform	

		fftw_mpi_execute_dft_c2r(planB,kc,c);	//inverse transform concentration A
		normalize(c);							//normalize concentration A after inverse transform
				
		MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point

		if( t%printFreq == 0 )
		{
			if(myid==0) printf("time = %d\n",t);

			output(t,n,"dens");
			output(t,c,"conc");
			//outputSlice(t,n,"dSlice");
			//outputSlice(t,c,"cSlice");
		}

		if( t%peakFreq == 0 )
		{
			getpeaks();			
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);	//mpi barrier call -- waits for all cpus to reach this point	
	freeMemory();  //free memory and destroy fftw plans and arrays

	//exit mpi environment
	MPI_Finalize();

	return 0;
}